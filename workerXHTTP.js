
// Charthouse
// https://gitlab.com/fjorden/charthouse
// Copyright 2017-2019 Fjorden

// this needs to be small because a copy exists
// for every layer (XXX look into shared web workers?)

"use strict";

var cmd;
var stats={};

var runningMutex=false;

function fetchCB(xhr)
  {
  if (xhr.readyState != 4)
    return;

  stats.xhr = {};
  stats.xhr.status = xhr.status;
  stats.total = -1;
  stats.visible = -1;
  stats.timeLoad = -1;

  if (stats.xhr.status == 200)
    {
    var data;
    var filterNameRE;

    stats.total = 0;
    stats.visible = 0;
    stats.dataType = xhr.getResponseHeader("Content-Type");
    stats.timeLoad = ((new Date()).getTime() - runningMutex) / 1000;

    if (stats.filterName.length > 0)
      filterNameRE = new RegExp(stats.filterName, "i");
    else
      filterNameRE = undefined;

    data = xhr.responseText;

    if (stats.dataType.match(/application\/vnd.google-earth\.kml\+xml/))
      {
      stats.kml = data;
      stats.geojson = undefined;
      stats.total = -1; // XXX parse KML and look for total
      stats.visible = stats.visible;
      }
    else if (stats.dataType.match(/application\/vnd\.geo\+json/) ||
             stats.dataType.match(/application\/json/))
      {
      var geojson;

      try
        {
        geojson = JSON.parse(data);
        }
      catch(err)
        {
        console.log("workerXHTTP JSON.parse(): " + stats.layerName + ": " +
                    err.message);
        };

      if (geojson != undefined && geojson.features != undefined)
        {
        var total;
        var index;

        if (cmd.debug && filterNameRE != undefined)
          {
          console.log("workerXHTTP filterNameRE: " + stats.layerName + ": " +
                      filterNameRE);
          };

        total = geojson.features.length;

        sortGeo(geojson, stats.bbox);

        for (index=geojson.features.length-1; index >= 0; --index)
          {
          var feature;
          var featureBBox;
          var featureArea;
          var good;
          var geoms;
          var coords;
          var jndex;

          feature = setFeatureDefaults(geojson.features[index]);

          if (feature.properties != undefined)
            {
            if (feature.properties.node != undefined)
              {
              if (feature.properties.node.name != undefined)
                {
                if (filterNameRE != undefined)
                  {
                  if (!feature.properties.node.name.match(filterNameRE))
                    {
                    geojson.features.splice(index, 1);
                    continue;
                    };
                  };
                };
              };
            if (feature.properties.style != undefined)
              {
              if (feature.properties.style.zoom != undefined)
                {
                // if we're zoomed out too far, don't show feature
                if (stats.zoomp < feature.properties.style.zoom)
                  {
                  geojson.features.splice(index, 1);
                  continue;
                  };
                };
              };

            // set additional properties
            feature.properties.layerUUID = stats.layerUUID;
            feature.properties.layerName = stats.layerName;
            feature.properties.layerGroup = stats.layerGroup;
            feature.properties.url = stats.url;
            };
          };

        stats.total = total;
        stats.visible = geojson.features.length;

        stats.geojson = geojson;
        stats.kml = undefined;
        };
      }
    else
      {
      // unknown data type
      stats.other = data;
      };
    }
  else if (cmd.debug && stats.xhr.status != 404)
    {
    console.log("workerXHTTP load error: " + stats.layerName + ": " +
                stats.xhr.status);
    };

  self.postMessage(JSON.stringify(stats));

  runningMutex = false;
  }

self.addEventListener("message", function(evt)
  {
  cmd = evt.data;
  stats.layerName = cmd.layerName;
  stats.layerGroup = cmd.layerGroup;

  if (runningMutex != false)
    {
    if (cmd.debug)
      console.log("workerXHTTP mutex locked: " + stats.layerName);
    return;
    };

  var found=false;
  runningMutex = (new Date()).getTime();

  stats.url = cmd.url;
  stats.bbox = [];
  stats.bbox.lonW = cmd.lonW;
  stats.bbox.latS = cmd.latS;
  stats.bbox.lonE = cmd.lonE;
  stats.bbox.latN = cmd.latN;
  stats.url += "?latN=" + stats.bbox.latN + "&lonW=" + stats.bbox.lonW +
               "&latS=" + stats.bbox.latS + "&lonE=" + stats.bbox.lonE;

  stats.zoomp = cmd.zoomp;

  stats.layerUUID = cmd.layerUUID;
  stats.visible = cmd.visible;
  stats.filterName = cmd.filterName;

  if (stats.visible)
    {
    xhttpGet(stats.url, false, false, fetchCB);
    }
  else
    {
    // if layer isn't visible, then just call the callback
    // so an emtpy statistics object can be returned
    var xhr;

    xhr = {};
    xhr.readyState = 4;
    xhr.status = 0;
    fetchCB(xhr);
    };
  });

