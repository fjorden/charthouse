
// Charthouse
// https://gitlab.com/fjorden/charthouse
// Copyright 2017-2019 Fjorden

// MQTT client

// mosquitto_sub -h localhost -t charthouse/#
// mosquitto_pub -h localhost -t charthouse/geojson -f example1.json

// see utils/mqtttest

"use strict";

var running=false;
var cmd;
var mqttWS;
var mqttTopics;
var msg={};
var bbox=[];
var features=[];

var client;

  function MQTTconnected()
    {
    var message;
    var index;

    if (cmd.debug === true)
      console.log("MQTT connected: " + mqttWS);

// XXX disconnects/reconnects happen quite often, so
// turn off user notificiations until we can figure out why
//    client.on("reconnect", MQTTreconnected);
//    client.on("close", MQTTdisconnected);

    for (index=0; index < mqttTopics.length; ++index)
      {
      client.subscribe(mqttTopics[index]);
      if (cmd.debug === true)
        console.log("MQTT subscribed: " + mqttTopics[index]);
      };
    }

  function MQTTreconnected()
    {
    if (cmd.debug === true)
      console.log("MQTT reconnected: " + mqttWS);

    // data for workerCB()
    msg.error = undefined;
    msg.notice = "MQTT server reconnected.";
    msg.warning = undefined;
    msg.topic = undefined;
    msg.message = undefined;
    msg.geojson = undefined;
    msg.layerUUID = cmd.layerUUID;
    msg.layerName = cmd.layerName;
    msg.layerGroup = cmd.layerGroup;
    msg.mqttTopics = cmd.mqttTopics;
    msg.xhr.status = 200; // fake return

    self.postMessage(JSON.stringify(msg));
    }

  function MQTTdisconnected()
    {
    if (cmd.debug === true)
      console.log("MQTT disconnected: " + mqttWS);

    // data for workerCB()
    msg.error = "MQTT server disconnected.";
    msg.notice = undefined;
    msg.warning = undefined;
    msg.topic = undefined;
    msg.message = undefined;
    msg.geojson = undefined;
    msg.layerUUID = cmd.layerUUID;
    msg.layerName = cmd.layerName;
    msg.layerGroup = cmd.layerGroup;
    msg.mqttTopics = cmd.mqttTopics;
    msg.xhr.status = 200; // fake return

    self.postMessage(JSON.stringify(msg));
    }

  function MQTTerror(message)
    {
    if (cmd.debug === true)
      console.log("MQTT error: " + mqttWS);

    // data for workerCB()
    if (message !== undefined)
      {
      msg.error = message;
      }
    else
      {
      msg.error = "MQTT Error<br>Check your network connection and/or " +
                  "browser extensions (e.g. ad blockers).";
      };
    msg.notice = undefined;
    msg.warning = undefined;
    msg.topic = undefined;
    msg.message = undefined;
    msg.geojson = undefined;
    msg.layerUUID = cmd.layerUUID;
    msg.layerName = cmd.layerName;
    msg.layerGroup = cmd.layerGroup;
    msg.xhr.status = 404; // fake return

    self.postMessage(JSON.stringify(msg));
    }

  function MQTTnotice(message)
    {
    if (cmd.debug === true)
      console.log("MQTT notice: " + mqttWS);

    // data for workerCB()
    if (message !== undefined)
      msg.notice = message
    else
      return;
    msg.error = undefined;
    msg.warning = undefined;
    msg.topic = undefined;
    msg.message = undefined;
    msg.geojson = undefined;
    msg.layerUUID = cmd.layerUUID;
    msg.layerName = cmd.layerName;
    msg.layerGroup = cmd.layerGroup;
    msg.xhr.status = 404; // fake return

    self.postMessage(JSON.stringify(msg));
    }

  function MQTTwarning(message)
    {
    if (cmd.debug === true)
      console.log("MQTT warning: " + mqttWS);

    // data for workerCB()
    if (message !== undefined)
      msg.warning = message
    else
      return;
    msg.error = undefined;
    msg.notice = undefined;
    msg.topic = undefined;
    msg.message = undefined;
    msg.geojson = undefined;
    msg.layerUUID = cmd.layerUUID;
    msg.layerName = cmd.layerName;
    msg.layerGroup = cmd.layerGroup;
    msg.xhr.status = 404; // fake return

    self.postMessage(JSON.stringify(msg));
    }

  function MQTTreceived(topic, message)
    {
    var _features=[];
    var _msg;

/*
    if (cmd.debug === true)
      console.log("MQTT: " + topic + " " + message);
*/

    if (topic.match(/\/error/))
      MQTTerror("" + message);
    if (topic.match(/\/notice/))
      MQTTnotice("" + message);
    if (topic.match(/\/warning/))
      MQTTwarning("" + message);

    try
      {
      _msg = JSON.parse(message);
      }
    catch(err)
      {
      if (cmd.debug === true)
        console.log("JSON.parse: " + err.message + ": " + message);

      return;
      };

    if (_msg.type === "Feature")
      {
      _features["type"] = "FeatureCollection";
      _features["features"] = [];
      _features["features"].push(_msg);
      }
    else if (_msg.type === "FeatureCollection")
      {
      _features = _msg;
      }
    else
      {
      // not geojson
      return;
      };

    // if platform already exists in list, overwrite it.
    // if platform doesn't already exist, add it to the list
    {
    var index;

    // XXX why do I need to do this here?
    if (features == undefined)
      features = [];

    for (index=0; index < _features["features"].length; ++index)
      {
      var jndex;
      var found=false;
      var _feature;

      _features["features"][index] =
        setFeatureDefaults(_features["features"][index]);

      for (jndex=0; jndex < features.length; ++jndex)
        {
        var properties;

        properties = features[jndex]["properties"];
        if (properties != undefined)
          {
          var node;

          node = properties["node"];
          if (node != undefined)
            {
            var uuid;

            uuid = node["uuid"];
            if (uuid != undefined)
              {
              var _properties;

              _properties = _features["features"][index]["properties"];
              if (_properties != undefined)
                {
                var _node;

                _node = _properties["node"];
                if (_node != undefined)
                  {
                  var _uuid;

                  _uuid = _node["uuid"];
                  if (uuid == _uuid)
                    {
                    // replace old feature with new feature
                    features[jndex] = _features["features"][index];
                    found = true;
                    };
                  };
                };
              };
            };
          };
        };

      if (found === false)
        {
        features.push(_features["features"][index]);
        };
      };
    };

    }

  function MQTTconnCheck()
    {
    if (cmd.debug === true)
      console.log("MQTT connection check: " + mqttWS);

    if (client.connected === false)
      {
      // data for workerCB()
      msg.error = "MQTT server not connected<br>" +
                  "Check your network connection and/or " +
                  "browser extensions (e.g. ad blockers).";
      msg.notice = undefined;
      msg.warning = undefined;
      msg.topic = undefined;
      msg.message = undefined;
      msg.geojson = undefined;
      msg.layerUUID = cmd.layerUUID;
      msg.layerName = cmd.layerName;
      msg.layerGroup = cmd.layerGroup;
      msg.xhr.status = 404; // fake return

      self.postMessage(JSON.stringify(msg));
      };
    }

  function MQTTsendMessage()
    {
    var message;

    // data for workerCB()
    msg.error = undefined;
    msg.notice = undefined;
    msg.warning = undefined;
    msg.topic = "MQTT";
    msg.geojson = undefined;
    msg.layerUUID = cmd.layerUUID;
    msg.layerName = cmd.layerName;
    msg.layerGroup = cmd.layerGroup;
    msg.xhr.status = 200; // fake return

    features = ageOff(features);

    // XXX why do I have to do this?
    if (features == undefined)
      features = [];

    msg.total = features.length;

    // XXX error check
    // turn bare features Array into feature collection
    message = '{"features":' + JSON.stringify(features) +
              ',"type":"FeatureCollection"}';
//    message = JSON.stringify(features);

    try
      {
      msg.geojson = JSON.parse(message);
      }
    catch(err)
      {
      if (cmd.debug === true)
        console.log("JSON.parse: " + err.message + ": " + message);

      return;
      };

    if (msg.geojson != undefined)
      sortGeo(msg.geojson, bbox);

    if (msg.geojson.features === undefined)
      msg.visible = -1;
    else
      msg.visible = msg.geojson.features.length;

    self.postMessage(JSON.stringify(msg));
    }

self.addEventListener("message", function(evt)
  {
  // data for workerCB()
  msg.error = undefined;
  msg.notice = undefined;
  msg.warning = undefined;
  msg.topic = undefined;
  msg.message = undefined;
  msg.geojson = undefined;
  msg.layerUUID = undefined;
  msg.layerName = undefined;
  msg.layerGroup = undefined;
  msg.xhr = {};
  msg.xhr.status = 404; // fake return
  msg.total = -1;
  msg.visible = -1;
  msg.timeLoad = -1;

  cmd = evt.data;

  switch (cmd.cmd)
    {
    case "start":
      // Older FireFoxes don't support WebSockets in WebWorkers
      if (typeof WebSocket === "undefined")
        {
        var _msg;

        _msg = "MQTT Error<br>This web browser does not support " +
               "required functionality.<br>Please update to a newer browser.";
        console.log("WebSocket in WebWorker not supported by this browser");
        MQTTerror(_msg);

        return;
        };

      var options =
        {
        clientId: cmd.clientId
        // XXX don't use hardcoded usernames/passwords!!
        // clients end up with this source code and could then
        // steal the username/password.  a better method should
        // be devised!
        //username: "charthouse",
        //password: "charthouse"
        };

      mqttWS = cmd.mqttWS;
      mqttTopics = cmd.mqttTopics;

      if (cmd.debug === true)
        console.log("MQTT connecting: " + mqttWS);

      client = mqtt.connect(mqttWS, options);

      client.on("error", MQTTerror);
      client.on("connect", MQTTconnected);
      client.on("message", function(topic, message)
        {MQTTreceived(topic, message);});

      setInterval(MQTTconnCheck, 60000);
      setInterval(MQTTsendMessage, 1000);
      break;
    case "bbox":
      bbox.latN = cmd.latN;
      bbox.latS = cmd.latS;
      bbox.lonE = cmd.lonE;
      bbox.lonW = cmd.lonW;
      break;
    };
  });

