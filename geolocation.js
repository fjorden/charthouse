
// Charthouse
// https://gitlab.com/fjorden/charthouse
// Copyright 2018-2019 Fjorden

"use strict";

// XXX use watchPosition() and clearWatch()

function locationSend(features)
  {
  if (features === undefined)
    features = [];

  if (settings["advanced"]["showDebug"])
    console.log("locationSend():", JSON.stringify(features));

  workerCBcall(features, "locationSend()",
    getLayerByName("Geolocation").uuid, "Geolocation", "Data");
  }

function getLocation()
  {
  if (settings["other"]["geolocation"] !== true)
    {
    locationSend();
    return;
    };

  navigator.geolocation.getCurrentPosition(locationFound, locationNotFound);
  }

function locationFound(position)
  {
  if (position === undefined || position.coords === undefined ||
      position.coords.latitude === undefined ||
      position.coords.longitude === undefined)
    {
	console.log("locationFound(): Invalid position");
    locationSend();
    return;
    };

  if (position.coords.latitude < -90 || position.coords.latitude > 90 ||
      position.coords.longitude < -180 || position.coords.longitude > 180)
    {
	console.log("locationFound(): Invalid latitude/longitude: " +
                position.coords.latitude + "/" + position.coords.longitude);
    locationSend();
    return;
    };

  // accuracy is in meters
  if (settings["advanced"]["showDebug"])
    {
	console.log("Location: " + position.coords.latitude + "/" +
                position.coords.longitude + " (±" +
                position.coords.accuracy + "m) " +
                position.coords.heading + "° " +
                position.coords.speed + "km/h");
    };

  var feature;
  var features=[];

  // build GeoJSON to send to workerCB()
  feature = setFeatureDefaults(feature);
  feature.geometry = {};
  feature.geometry.type = "GeometryCollection";
  feature.geometry.geometries = [];
  feature.geometry.geometries[0] = {};
  feature.geometry.geometries[0].type = "Point";
  feature.geometry.geometries[0].coordinates = [];
  feature.geometry.geometries[0].coordinates[0] = position.coords.longitude;
  feature.geometry.geometries[0].coordinates[1] = position.coords.latitude;
  feature.properties.node.uuid = "Geolocation";
  feature.properties.node.name = "Geolocation";

  feature.properties.style.iconURL = "\uF05B"; // crosshairs
  feature.properties.style.iconFontFamily = "FontAwesome";
  feature.properties.style.iconFontSize = 12;
  feature.properties.style.icon = "#0FF";
  feature.properties.style.iconOutline = "#077";
  feature.properties.style.iconOpacity = 0.9;
  feature.properties.style.iconOutlineOpacity = 0.9;
  feature.properties.style.fill = "#077";
  feature.properties.style.fillOpacity = 0;
  feature.properties.style.stroke = "#077";
  feature.properties.style.zoom = 0;
  feature.properties.layerGroup = "Data";
  feature.properties.layerName = "Geolocation";
  feature.properties.layerUUID = getLayerByName("Geolocation").uuid;

  if (position.coords.accuracy >= 0)
    {
    var circle;

    feature.properties.gps = {};
    feature.properties.gps.hdop = position.coords.accuracy;

    circle = turf.circle(
      [position.coords.longitude, position.coords.latitude],
      position.coords.accuracy/1000);
    circle = turf.polygonToLine(circle);

    feature.geometry.geometries.push(circle["geometry"]);
    };

  if (position.coords.heading >= 0 && position.coords.heading < 360)
    {
    feature.properties.node.attitude = {};
    feature.properties.node.attitude.heading = position.coords.heading;
    };

  if (position.coords.speed >= 0)
    {
    feature.properties.node.speed = {};
    feature.properties.node.speed.sog = position.coords.speed;
    };

  features.push(feature);

  locationSend(features);
  }

function locationNotFound()
  {
  console.log("locationFound(): Unable to determine location");
  }

