
// Charthouse
// https://gitlab.com/fjorden/charthouse
// Copyright 2017-2019 Fjorden

"use strict";

var layersData=[];
var layersBase=[];
var settings={}; // settings object

function settingsInit()
  {
  layersData =
    [
    {
    name: "User markers",
    group: "Data",
    OLtype: "Vector",
    dataFunction: dataFunctionMarkersUser,
    interval: 300
    }
    ];

  layersBase =
    [
    {
    name: "Simple world map",
    group: "Maps",
    OLtype: "Vector",
    url: baseURL + "maps/countries_world.kml",
    visible: 0,
    input: "slider"
    },
    {
    name: "Time zones",
    group: "Overlays",
    OLtype: "Vector",
    url: baseURL + "maps/timezones.kml",
    visible: 0,
    input: "slider"
    },
    {
    name: "Moon & Lunar Illumination",
    group: "Overlays",
    OLtype: "Vector",
    dataFunction: dataFunctionMoon,
    interval: 300,
    visible: 0,
    input: "slider"
    },
    {
    name: "Sun & Solar Illumination",
    group: "Overlays",
    OLtype: "Vector",
    dataFunction: dataFunctionSun, 
    interval: 300,
    visible: 10,
    input: "slider"
    }
    ];

  settingsErrorCheck(settings);
  };

  function setCookie(cname, cvalue)
    {
    document.cookie = cname + "=" + cvalue + "; ";
    }

  function getCookie(cname)
    {
    var name;
    var ca;
    var index;

    name = cname + "=";
    ca = document.cookie.split(";");

    for (index=0; index < ca.length; ++index)
      {
      var c = ca[index];

      while (c.charAt(0) == ' ')
        c = c.substring(1);
      if (c.indexOf(name) == 0)
        return(c.substring(name.length, c.length));
      };

    return(null);
    }

function settingsErrorCheck(sets)
  {
  if (settings["center"] == undefined)
    {
    settings["center"] = {};
    settings["center"]["latitude"] = undefined;
    settings["center"]["longitude"] = undefined;
    settings["center"]["zoom"] = undefined;
    settings["center"]["zoomp"] = undefined;
    };
  if (!(settings["center"]["latitude"] >= -85.0 &&
        settings["center"]["latitude"] <= 85.0 &&
        settings["center"]["longitude"] >= -180.0 &&
        settings["center"]["longitude"] <= 180.0))
    {
    settings["center"]["latitude"] = 0;
    settings["center"]["longitude"] = 0;
    };
  if (!(settings["center"]["zoom"] >= 0 && settings["center"]["zoom"] <= 20))
    settings["center"]["zoom"] = 2;
  if (!(settings["center"]["zoomp"] >= 0 && settings["center"]["zoom"] <= 100))
    settings["center"]["zoomp"] = 0;

  if (settings["format"] == undefined)
    {
    settings["format"] = {};
    settings["format"]["position"] = undefined;
    };
  if (!(settings["format"]["position"] == "degrees" ||
        settings["format"]["position"] == "degmin" ||
        settings["format"]["position"] == "dms" ||
        settings["format"]["position"] == "utm" ||
        settings["format"]["position"] == "mgrs"))
    {
    settings["format"]["position"] = "degrees";
    };

  if (settings["units"] == undefined)
    {
    settings["units"] = {};
    settings["units"]["distance"] = undefined;
    settings["units"]["altitude"] = undefined;
    };
  if (!(settings["units"]["distance"] == "metric" ||
        settings["units"]["distance"] == "nautical" ||
        settings["units"]["distance"] == "uscustomary"))
    {
    settings["units"]["distance"] = "metric";
    };
  if (!(settings["units"]["altitude"] == "feet" ||
        settings["units"]["altitude"] == "metric"))
    {
    settings["units"]["altitude"] = "feet";
    };

  if (settings["other"] == undefined)
    {
    settings["other"] = {};
    settings["other"]["geolocation"] = undefined;
    settings["other"]["followTrackUp"] = undefined;
    settings["other"]["showCrosshair"] = undefined;
    settings["other"]["showFeaturesCount"] = undefined;
    };
  if (!(settings["other"]["geolocation"] === true ||
        settings["other"]["geolocation"] === false))
    {
    settings["other"]["geolocation"] = false;
    };
  if (!(settings["other"]["followTrackUp"] === true ||
        settings["other"]["followTrackUp"] === false))
    {
    settings["other"]["followTrackUp"] = false;
    };
  if (!(settings["other"]["showCrosshair"] === true ||
        settings["other"]["showCrosshair"] === false))
    {
    settings["other"]["showCrosshair"] = true;
    };
  if (!(settings["other"]["showFeaturesCount"] === true ||
        settings["other"]["showFeaturesCount"] === false))
    {
    settings["other"]["showFeaturesCount"] = false;
    };

  if (settings["advanced"] == undefined)
    {
    settings["advanced"] = {};
    settings["advanced"]["showDiagnostics"] = undefined;
    settings["advanced"]["showDebug"] = undefined;
    settings["advanced"]["showFeaturesAll"] = undefined;
    };
  if (!(settings["advanced"]["showDiagnostics"] === true ||
        settings["advanced"]["showDiagnostics"] === false))
    {
    settings["advanced"]["showDiagnostics"] = false;
    };
  if (!(settings["advanced"]["showDebug"] === true ||
        settings["advanced"]["showDebug"] === false))
    {
    settings["advanced"]["showDebug"] = false;
    };
  if (!(settings["advanced"]["showFeaturesAll"] === true ||
        settings["advanced"]["showFeaturesAll"] === false))
    {
    settings["advanced"]["showFeaturesAll"] = false;
    };

  if (settings["markers"] === undefined)
    settings["markers"] = "{}";
  }

function settingsSave()
  {
  var inputs;
  var setting;
  var index;

  inputs = document.querySelectorAll("input[name='posFormat']")
  for (index=0; index < inputs.length; ++index)
    {
    if (inputs[index].checked)
      settings["format"]["position"] = inputs[index].value;
    };

  inputs = document.querySelectorAll("input[name='distUnits']")
  for (index=0; index < inputs.length; ++index)
    {
    if (inputs[index].checked)
      {
      settings["units"]["distance"] = inputs[index].value;
      };
    };

  inputs = document.querySelectorAll("input[name='altUnits']")
  for (index=0; index < inputs.length; ++index)
    {
    if (inputs[index].checked)
      settings["units"]["altitude"] = inputs[index].value;
    };

  settings["other"]["geolocation"] =
    document.querySelector('input[name="geolocation"]').checked;

  settings["other"]["followTrackUp"] =
    document.querySelector('input[name="followTrackUp"]').checked;

  settings["other"]["showCrosshair"] =
    document.querySelector('input[name="showCrosshair"]').checked;

  settings["other"]["showFeaturesCount"] =
    document.querySelector('input[name="showFeaturesCount"]').checked;

  settings["advanced"]["showDiagnostics"] =
    document.querySelector('input[name="showDiagnostics"]').checked;

  settings["advanced"]["showDebug"] =
    document.querySelector('input[name="showDebug"]').checked;

  settings["advanced"]["showFeaturesAll"] =
    document.querySelector('input[name="showFeaturesAll"]').checked;

  if (markersGeoJSON.type === "FeatureCollection")
    settings["markers"] = JSON.stringify(markersGeoJSON);
  else
    settings["markers"] = "{}";

  // error check all settings
  settingsErrorCheck(settings);

  // save settings
  if (settings["advanced"]["showDebug"])
    console.log("settingsSave():", JSON.stringify(settings));

  setCookie("settings", JSON.stringify(settings));

  // update various items based on settings
  updateCenter();
  if (settings["other"]["showCrosshair"])
    crosshair.style.display = "block";
  else
    crosshair.style.display = "none";

  if (settings["other"]["showFeaturesCount"])
    elemShowById("buttonStatistics");
  else
    elemHideById("buttonStatistics");
  }

function settingsLoad()
  {
  var value;
  var group;
  var btn;
  var index;

  value = getCookie("settings");
  try
    {
    settings = JSON.parse(value);
    }
  catch(err)
    {
    console.log("settingsLoad(): JSON.parse(): " + err.message +
                ": " + value);
    };
  if (settings == null || settings == undefined)
    settings = {};

  // error check all settings
  settingsErrorCheck(settings);

  // now use settings
  group = document.forms["formSettings"]["posFormat"];
  for (index=0; index < group.length; ++index)
    {
    if (group[index].id == settings["format"]["position"])
      group[index].checked = true;
    };
  if (settings["format"]["position"] == "nautical")
    scaleLine.setUnits("nautical");
  else if (settings["format"]["position"] == "uscustomary")
    scaleLine.setUnits("us");
  else
    scaleLine.setUnits("metric");

  group = document.forms["formSettings"]["distUnits"];
  for (index=0; index < group.length; ++index)
    if (group[index].id == settings["units"]["distance"])
      group[index].checked = true;

  group = document.forms["formSettings"]["altUnits"];
  for (index=0; index < group.length; ++index)
    if (group[index].id == settings["units"]["altitude"])
      group[index].checked = true;

  document.querySelector('input[name="geolocation"]').checked =
    settings["other"]["geolocation"];

  document.querySelector('input[name="showCrosshair"]').checked =
    settings["other"]["showCrosshair"];

  document.querySelector('input[name="showFeaturesCount"]').checked =
    settings["other"]["showFeaturesCount"];

  document.querySelector('input[name="showDiagnostics"]').checked =
    settings["advanced"]["showDiagnostics"];

  document.querySelector('input[name="showDebug"]').checked =
    settings["advanced"]["showDebug"];

  document.querySelector('input[name="showFeaturesAll"]').checked =
    settings["advanced"]["showFeaturesAll"];

  try
    {
    markersGeoJSON = JSON.parse(settings["markers"]);
    }
  catch(err)
    {
    console.log("settingsLoad(): JSON.parse(): " + err.message +
                ": " + value);
    markersGeoJSON = {};
    };

  {
  var lonlat=[];
  var coord;

  // move map to center
  lonlat[0] = settings["center"]["longitude"];
  lonlat[1] = settings["center"]["latitude"];
  coord = ol.proj.transform(lonlat, "EPSG:4326", "EPSG:3857");
  map.getView().setCenter(coord);
  map.getView().setZoom(settings["center"]["zoom"]);
  updateCenter();
  };

  // call so various things get updated
  settingsSave();
  }

