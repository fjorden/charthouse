
// Charthouse
// https://gitlab.com/fjorden/charthouse
// Copyright 2017-2019 Fjorden

"use strict";

function dropdownsClose()
  {
  var dropdowns;
  var index;

  dropdowns = document.getElementsByClassName("dropdown-content");
  for (index=0; index < dropdowns.length; ++index)
    dropdowns[index].style.display = "none";
  }

function dropdownToggle(elem)
  {
  var dropdown;
  var display =false;

  dropdown = document.getElementById(elem.dataset.dropdown);
  if (dropdown == undefined)
    return;

  display = dropdown.style.display;

  dropdownsClose();

  console.log(display);
  if (display === "block")
    dropdown.style.display = "none";
  else
    dropdown.style.display = "block";
  }

