
// Charthouse
// https://gitlab.com/fjorden/charthouse
// Copyright 2017-2019 Fjorden

"use strict";

var followName;
var followUUID;

function followOn(name, uuid)
  {
  if (settings["advanced"]["showDebug"])
    console.log("followOn:", name, uuid);
  elemShow(buttonFollow);
  // XXX this isn't working with UIkit
  //buttonFollow.title = "Following " + name + ". Click to stop following";
  badgeFollow.innerHTML = name;
  followName = name;
  followUUID = uuid;
  followUpdate();
  popupFeatureClose();
  }

function followOff()
  {
  if (settings["advanced"]["showDebug"])
    console.log("followOff:", followName, followUUID);
  elemHide(buttonFollow);
  followName = undefined;
  followUUID = undefined;
  mapRotate(0);
  }

function followUpdate()
  {
  if (followUUID == undefined)
    return;

  var feature;
  var latlon;

  feature = getFeature(followUUID);
  if (feature == undefined)
    {
    UIkit.notification({status: "warning", message:
      "<div><i class='fas fa-exclamation-circle fa-fw'></i> " +
      "Feature no longer in view:</div>" +
      "<div class='ch-center'>" + followName + "</div>" +
      "<div><b><i>Feature follow</i></b> turned off.</div>"});
    followOff();
    return;
    };

  crosshair.style.display = "none";

  latlon = getLatLonAlt(feature);
  mapCenter(latlon[0], latlon[1]);

  if (settings["advanced"]["showDebug"])
    console.log("followUpdate:", followUUID, latlon[0], ",", latlon[1]);

  if (settings["other"]["followTrackUp"] === true)
    {
    var azimuth;

    azimuth = getHeading(feature);
    if (azimuth >= 0 && azimuth < 360)
      mapRotate(azimuth);
    };
  }

