
// Charthouse
// https://gitlab.com/fjorden/charthouse
// Copyright 2017-2019 Fjorden

"use strict";

var interactionMarkerAdd;
var markerAdd=false;

var markersGeoJSON={};

function dataFunctionMarkersUser()
  {
  if (markersGeoJSON.type !== "FeatureCollection")
    return;

  var data;
  var evt;

  // build data to send to workerCB()
  data = {};
  data.url = "dataFunctionMarkersUser()";
  data.layerUUID = getLayerByName("User markers").uuid;
  data.layerName = "User markers";
  data.layerGroup = "Data";
  data.total = 1;    // total number of features
  data.visible = -1; // total number of features visible
  data.xhr = {};
  data.xhr.status = 200;
  data.geojson = JSON.stringify(markersGeoJSON);

  // build a fake message to send to workerCB()
  evt = {};
  evt.target = {};
  evt.target.running = false;
  evt.data = JSON.stringify(data);

  workerCB(evt);
  }

function markerAddToggle()
  {
  if (markerAdd == false)
    markerAddOn();
  else
    markerAddOff();
  }

function markerAddOn()
  {
  buttonMarkerAddBackground = buttonMarkerAdd.style.background;
  buttonMarkerAddColor = buttonMarkerAdd.style.color;
  buttonMarkerAdd.style.background = "#C00";
  buttonMarkerAdd.style.color = "#EEE";
/*
  var html;

  html = buttonMarkerAdd.innerHTML;
  html = html.replace(/<br><span.*span>/, "");
  html += "<br><span class='tiny badge-tiny green'>On</span>";
  buttonMarkerAdd.innerHTML = html;
*/

  markerAdd = true;
  map.addInteraction(interactionMarkerAdd);
  }

function markerAddSet(latitude, longitude)
  {
  var feature;

  feature = {};
  setFeatureDefaults(feature);

  feature.type = "Feature";
  feature.geometry = {};
  feature.geometry.type = "GeometryCollection";
  feature.geometry.geometries = [];
  feature.geometry.geometries[0] = {};
  feature.geometry.geometries[0].type = "Point";
  feature.geometry.geometries[0].name = "position";
  feature.geometry.geometries[0].coordinates = [];
  feature.geometry.geometries[0].coordinates[0] = longitude;
  feature.geometry.geometries[0].coordinates[1] = latitude;
  feature.properties.deletable = "true";
  feature.properties.node.name = "User point";
  feature.properties.style.icon = "#F50"; // Int'l Orange
  feature.properties.style.iconFontFamily = "FontAwesome";
  feature.properties.style.iconFontSize = 16;
  feature.properties.style.iconOpacity = 1;
  feature.properties.style.iconOutline = "#FFF";
  feature.properties.style.iconOutlineOpacity = 1;
  feature.properties.style.iconURL = "\uF041"; // map-marker
  feature.properties.style.textOpacity = 0;
  feature.properties.style.zoom = 0;

  markersGeoJSON.type = "FeatureCollection";
  if (markersGeoJSON.features == undefined)
    markersGeoJSON.features = [];

  markersGeoJSON.features.push(feature);

  dataFunctionMarkersUser();
  }

function markerAddOff()
  {
  buttonMarkerAdd.style.background = buttonMarkerAddBackground;
  buttonMarkerAdd.style.color = buttonMarkerAddColor;

/*
  var html;

  html = buttonMarkerAdd.innerHTML;
  html = html.replace(/<br><span.*span>/, "");
  buttonMarkerAdd.innerHTML = html;
*/

  map.removeInteraction(interactionMarkerAdd);
  markerAdd = false;
  }

function markerDelete(uuid)
  {
  if (markersGeoJSON.type !== "FeatureCollection" ||
      markersGeoJSON.features == undefined)
    {
    return;
    };

  popupFeatureClose();
 
  var index;

  for (index=markersGeoJSON.features.length-1; index >= 0; --index)
    {
    var node;

    node = markersGeoJSON.features[index].properties.node;
    if (node != "undefined" && node["uuid"] == uuid)
        markersGeoJSON.features.splice(index, 1);
    };

  dataFunctionMarkersUser();
  }

