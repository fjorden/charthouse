
// Charthouse
// https://gitlab.com/fjorden/charthouse
// Copyright 2017-2019 Fjorden

"use strict";

function getFeature(uuid)
  {
  var index;

  for (index=0; index < layersData.length; ++index)
    {
    var features;
    var jndex;

    features = layersData[index].layer.getSource().getFeatures();

    for (jndex=0; jndex < features.length; ++jndex)
      {
      if (getUUID(features[jndex]) == uuid)
        return(features[jndex]);
      };
    };

  return(undefined);
  }

function getRFM(feature)
  {
  var rfm;

  rfm = feature.get("rfm");
  if (rfm != undefined)
    {
    var text="";
    var value;
    var value1;

    value = rfm["snr"];
    if (value != undefined)
      text += " SNR:" + value + "dB";

    value = rfm["rssi"];
    if (value != undefined)
      text += " RSSI:" + value + "dBm";

    value = rfm["rxBad"];
    value1 = rfm["rxGood"];
    if (value != undefined && value1 != undefined)
      {
      var tot;

      tot = value + value1;
      text += " RX:" + value1 + "/" + tot;
      if (tot > 0)
        {
        var perc;

        perc = roundf((value1 / tot) * 100, 0);
        text += " (" + perc + "%)";
        };
      };

    if (text.length > 0)
      text = "RFM " + text;

    return(text);
    };

  return("");
  }

function getGPS(feature)
  {
  var gps;

  gps = feature.get("gps");
  if (gps != undefined)
    {
    var text="";
    var value;

    value = gps["sats"];
    if (value != undefined)
      text += " SVs:" + value;

    value = gps["pdop"];
    if (value != undefined)
      text += " PDOP:" + value + "m";

    value = gps["hdop"];
    if (value != undefined)
      text += " HDOP:" + value + "m";

    value = gps["vdop"];
    if (value != undefined)
      text += " VDOP:" + value + "m";

    if (text.length > 0)
      text = "GPS " + text;

    return(text);
    };

  return("")
  }

function getHeading(feature)
  {
  var attitude;
  var cmg;
  var heading;

  // XXX how do I use .getRotation instead?!

  attitude = feature.get("attitude");
  if (attitude != undefined) // libmote
    {
    cmg = attitude["cmg"];
    heading = attitude["heading"];
    }
  else // other (e.g. kml)
    {
    cmg = feature.get("cmg");
    heading = feature.get("heading");
    };

  if (heading != undefined && heading >= 0 && heading < 360)
    return(heading);

  // if we don't have heading, then try course made good
  if (cmg != undefined && cmg >= 0 && cmg < 360)
    return(cmg);

  return(-1);
  }

function getSOG(feature)
  {
  var speed;
  var sog;

  speed = feature.get("speed");
  if (speed != undefined) // libmote
    sog = speed["sog"];
  else // other (e.g. kml)
    sog = feature.get("sog");

  if (sog != undefined && sog >= 0)
    return(sog);

  return(-1);
  }

function getName(feature, truncate)
  {
  var node;
  var name;

  if (truncate != true)
    truncate = false;

  node = feature.get("node");
  if (node != undefined) // libmote
    name = node["name"];
  else // other (e.g. kml)
    name = feature.get("name");

  // no name?  try UUID
  if (name === undefined || name.length == 0)
    name = getUUID(feature);

  name = name.trim(); // strip leading/trailing white space

  if (truncate && name.length > 15)
    name = name.substring(0, 10).trim() + "…";

  return(name);
  }

function getInfo(feature)
  {
  var node;
  var info;

  node = feature.get("node");
  if (node != undefined) // libmote
    info = node["info"];
  else // other (e.g. kml)
    info = feature.get("info");

  if (info != undefined && info.length > 0)
    {
    info = info.trim(); // strip leading/trailing white space
    return(info);
    };

  return("");
  }

function getUUID(feature)
  {
  var node;
  var uuid;

  node = feature.get("node");
  if (node != undefined)
    uuid = node["uuid"];
  else
    uuid = feature.get("uuid");

  if (uuid != undefined && uuid.length > 0)
    return(uuid);

  return("");
  }

function getType(feature)
  {
  var node;
  var type;

  node = feature.get("node");
  if (node != undefined)
    type = node["type"];
  else
    type = feature.get("type");

  if (type != undefined && type.length > 0)
    return(type);

  return("");
  }

function getLayerName(feature)
  {
  var layerName;

  layerName = feature.get("layerName");
  if (layerName != undefined && layerName.length > 0)
    return(layerName);

  return("");
  }

function getLayerGroup(feature)
  {
  var layerGroup;

  layerGroup = feature.get("layerGroup");
  if (layerGroup != undefined && layerGroup.length > 0)
    return(layerGroup);

  return("");
  }

function getURL(feature)
  {
  var url;

  url = feature.get("url");
  if (url != undefined && url.length > 0)
    return(url);

  return("");
  }

function getText(feature)
  {
  var node;
  var text;

  node = feature.get("node");
  if (node != undefined)
    text = node["text"];
  else
    text = feature.get("description");

  if (text != undefined && text.length > 0)
    return(text);

  return("");
  }

function getAdmin(feature)
  {
  var node;
  var admin;

  node = feature.get("node");
  if (node != undefined)
    admin = node["admin"];
  else
    admin = feature.get("admin");

  if (admin != undefined && admin.length > 0)
    return(admin);

  return("");
  }

// return latitude, longitude, altitude
function getLatLonAlt(feature)
  {
/*
  var coord;
  var lonlat;

  // XXX this doesn't seem to work, is there a better way?!
  coord = ol.extent.getCenter(feature.getGeometry().getExtent());
  lonlat = ol.proj.transform(coord, "EPSG:3857", "EPSG:4326");
  console.dir(coord, lonlat)

  return([lonlat[1], lonlat[0]]);
*/

  var position;
  var latitude=-256;
  var longitude=-256;
  var altitude=-256;

  position = feature.get("position");
  if (position != undefined)
    {
    latitude = position["latitude"];
    longitude = position["longitude"];
    altitude = position["altitude"];
    }
  else
    {
    var coord;
    var lonlat;

    // try other (e.g. KML)
    if (typeof feature.getGeometry().getCoordinates == "function")
      {
      coord = feature.getGeometry().getCoordinates()
      }
    else if (typeof feature.getGeometry().getGeometries == "function")
      {
      var geometries;
      var index;

      geometries = feature.getGeometry().getGeometries();
      for (index=0; index < geometries.length; ++index)
        {
        if (geometries[index].getType() == "Point")
          {
          coord = geometries[index].getCoordinates();
          };
        };
      };

    if (coord != undefined)
      {
      lonlat = ol.proj.transform(coord, "EPSG:3857", "EPSG:4326");
      latitude = lonlat[1];
      longitude = lonlat[0];
      altitude = lonlat[2];
      };
    };

  return([latitude, longitude, altitude]);
  }

function getPosition(feature)
  {
  var text="";
  var latlonalt;
  var latitude;
  var longitude;
  var altitude;

  latlonalt = getLatLonAlt(feature);
  latitude = latlonalt[0];
  longitude = latlonalt[1];
  altitude = latlonalt[2];

  if (latitude != undefined && longitude != undefined &&
      latitude >= -90 && latitude <= 90 &&
      longitude >= -180 && longitude <= 180)
    {
    text = latlonString(roundf(latitude, 5), roundf(longitude, 5));
    };
  if (altitude != undefined)
    {
    text += " " + altitudeString(altitude);
    };

  if (text != undefined && text.length > 0)
    return(text);

  return("");
  }

function getError(feature)
  {
  var position;
  var text="";
  var errorSemiMajor;
  var errorSemiMinor;
  var errorAzimuth;

  position = feature.get("position");
  if (position != undefined)
    {
    errorSemiMajor = position["errorSemiMajor"];
    errorSemiMinor = position["errorSemiMinor"];
    errorAzimuth = position["errorAzimuth"];
    }
  else
    {
    // try other (e.g. KML)
    // XXX
    };

  if (errorSemiMajor != undefined && errorSemiMinor != undefined &&
      errorAzimuth != undefined && errorSemiMajor > 0 &&
      errorSemiMinor > 0 && errorAzimuth >= 0 && errorAzimuth < 360)
    {
    text = "Error: ";
    text += distanceString(errorSemiMajor) + " &times; ";
    text += distanceString(errorSemiMinor);
    text += " @" + errorAzimuth + "&deg;";
    };

  if (text != undefined && text.length > 0)
    return(text);

  return("");
  }

function getAimpoint(feature)
  {
  var position;
  var text="";
  var latitude;
  var longitude;
  var latitudeAim;
  var longitudeAim;
  var fovh;

  position = feature.get("position");
  if (position != undefined)
    {
    var aimpoint;

    latitude = position["latitude"];
    longitude = position["longitude"];

    aimpoint = position["aimpoint"];
    if (aimpoint != undefined)
      {
      latitudeAim = aimpoint["latitudeDesired"];
      longitudeAim = aimpoint["longitudeDesired"];
      fovh = aimpoint["fovh"];
      };
    }
  else
    {
    // try other (e.g. KML)
    // XXX
    };

  if (latitudeAim != undefined && latitudeAim != undefined &&
      longitudeAim != undefined && latitudeAim >= -90 && latitudeAim <= 90 &&
      longitudeAim >= -180 && longitudeAim <= 180)
    {
    text = "Aimpoint: " + latlonString(latitudeAim, longitudeAim);
    if (fovh != undefined && fovh > 0 && fovh <= 360)
      text += " FOV: " + fovh + "&deg;";

    // also output distance and azimuth to aimpoint
    if (latitude != undefined && longitude != undefined &&
        latitude >= -90 && latitude <= 90 &&
        longitude >= -180 && longitude <= 180)
      {
      var distance;
      var azimuth;
      var pos;
      var posAim;

      pos = new LatLon(latitude, longitude);
      posAim = new LatLon(latitudeAim, longitudeAim);
      distance = pos.distanceTo(posAim);
      azimuth = pos.initialBearingTo(posAim);

      if (distance != undefined && azimuth != undefined &&
          distance >= 0 && azimuth >= 0 && azimuth < 360)
        {
        text += " " + distanceString(distance) + " " + azimuthString(azimuth);
        };
      };
    };

  if (text != undefined && text.length > 0)
    return(text);

  return("");
  }

function getDatetime(feature)
  {
  var datetime;
  var text="";
  var epoch;

  datetime = feature.get("datetime");
  if (datetime != undefined)
    {
    epoch = datetime["epoch"];
    // XXX what about other date/time fields?
    }
  else
    {
    // try other (e.g. KML)
    // XXX
    };

  if (epoch != undefined)
    {
    var dt;

    dt = new Date(epoch*1000);
    text = "<font class='x-small uk-text-muted'>Last update: " +
      "<font class='italic'>" + datetimeString(dt) + "</font></font>";
    // XXX output "xxx hours mins secs ago"
    };

  if (text != undefined && text.length > 0)
    return(text);

  return("");
  }

function getIP(feature)
  {
  var network;
  var ip;

  network = feature.get("network");
  if (network != undefined)
    ip = network["ip"];
  else
    ip = feature.get("ip");

  if (ip != undefined && ip.length > 0)
    return(ip);

  return("");
  }

function getFeatureText(feature, html)
  {
  var text="";
  var value;

  value = getName(feature);
  if (value.length == 0) // if no name, don't bother with rest
    return("");

  if (html)
    text += "<b>" + value + "</b>";
  else
    text += value;

  /*
  {
  // if no name, then output all the keys
  var index;
  var keys;

  keys = feature.getKeys();
  for (index=0; index < keys.length; ++index)
    {
    var value;

    value = feature.get(keys[index]);
    // ignore some KML fields
    if (value != undefined && value.length > 0 &&
        value != "styleUrl")
      textExtra += keys[index] + ": " + value + newline;
    };
  };
  */

  value = getPosition(feature);
  if (value.length > 0)
    {
    if (html)
      text += "<br>" + value;
    else
      text += " " + value;
    };

  value = getHeading(feature);
  if (value >= 0 && value < 360)
    text += " " + azimuthString(value);

  value = getSOG(feature);
  if (value >= 0)
    text += " " + speedString(value);

  value = getError(feature);
  if (value.length > 0)
    {
    if (html)
      text += "<br>" + value;
    else
      text += " " + value;
    };

  value = getAimpoint(feature);
  if (value.length > 0)
    {
    if (html)
      text += "<br>" + value;
    else
      text += " " + value;
    };

  value = getAdmin(feature);
  if (value.length > 0)
    if (html)
      text += "<br>" + value;
    else
      text += " " + value;

  value = getText(feature);
  if (value.length > 0)
    if (html)
      text += "<br>" + value;
    else
      text += " " + value;

  value = getInfo(feature);
  if (value.length > 0)
    if (html)
      text += "<br>" + value;
    else
      text += " " + value;

  value = getGPS(feature)
  if (value.length > 0)
    if (html)
      text += "<br>" + value;
    else
      text += " " + value;

  value = getRFM(feature)
  if (value.length > 0)
    if (html)
      text += "<br>" + value;
    else
      text += " " + value;

  value = getDatetime(feature);
  if (value.length > 0)
    {
    if (html)
      text += "<br>" + value;
    else
      text += " " + value;
    };

  value = getLayerName(feature);
  if (value.length > 0)
    {
    var group;

    group = getLayerGroup(feature);
    if (group == undefined || group.length == 0)
      group = "";

    if (html)
      {
      text += "<br><font class='x-small uk-text-muted'>Data source: " +
        "<font class='italic'>" + group + " / " + value + "</font></font>";
      }
    else
      {
      text += " " + value;
      };
    };

  if (settings["advanced"]["showDiagnostics"])
    {
    var uuid;
    var type;
    var url;

    uuid = getUUID(feature);
    if (uuid != undefined && uuid.length > 0)
      {
      if (html)
        {
        text += "<br><font class='small uk-text-muted'>UUID: " +
          "<font class='italic'>" + uuid + "</font></font>";
        }
      else
        {
        text += " " + uuid;
        };
      };

    type = getType(feature);
    if (type != undefined && type.length > 0)
      {
      if (html)
        {
        text += "<br><font class='small uk-text-muted'>Type: " +
          "<font class='italic'>" + type + "</font></font>";
        }
      else
        {
        text += " " + type;
        };
      };

    url = getURL(feature);
    if (url != undefined && url.length > 0)
      {
      if (html)
        {
        text += "<br><font class='small uk-text-muted'>URL: " +
          "<font class='italic'>" + url + "</font></font>";
        }
      else
        {
        text += " " + url;
        };
      };
    };

  text = text.trim(); // strip leading/trailing white space

  return(text);
  }

// this should be fast.  it's called A LOT
// if feature already has a style, then bail (unless override === true)
function styleGenerate(feature, override)
  {
  if (override === false && feature.getStyle() != null)
    return(undefined);

  var style;

  style = feature.get("style");
  if (style === undefined)
    return(undefined); // it's supposed to exist due to setFeatureDefaults()

  // if we're zoomed out too far, don't show feature
  // unless, of course, showFeaturesAll setting is set
  if (settings["center"]["zoomp"] < style.zoom &&
      settings["advanced"]["showFeaturesAll"] === false)
    {
    return(false);
    };

  var font;
  var styleTextIconFont;
  var styleIcon;
  var styleFill;
  var styleStroke;
  var styles=[];

  style.fillRGBA = ol.color.asArray(style.fill);
  style.fillRGBA = style.fillRGBA.slice();
  style.fillRGBA[3] = style.fillOpacity;

  style.iconRGBA = ol.color.asArray(style.icon);
  style.iconRGBA = style.iconRGBA.slice();
  style.iconRGBA[3] = style.iconOpacity;

  style.iconOutlineRGBA = ol.color.asArray(style.iconOutline);
  style.iconOutlineRGBA = style.iconOutlineRGBA.slice();
  style.iconOutlineRGBA[3] = style.iconOutlineOpacity;

  style.iconOutlineRGBA = ol.color.asArray(style.iconOutline);
  style.iconOutlineRGBA = style.iconOutlineRGBA.slice();
  style.iconOutlineRGBA[3] = style.iconOutlineOpacity;

  style.strokeRGBA = ol.color.asArray(style.stroke);
  style.strokeRGBA = style.strokeRGBA.slice();
  style.strokeRGBA[3] = style.strokeOpacity;

  style.textRGBA = ol.color.asArray(style.text);
  style.textRGBA = style.textRGBA.slice();
  style.textRGBA[3] = style.textOpacity;

  style.textBackgroundRGBA = ol.color.asArray(style.textBackground);
  style.textBackgroundRGBA = style.textBackgroundRGBA.slice();
  style.textBackgroundRGBA[3] = style.textBackgroundOpacity;

  style.textOutlineRGBA = ol.color.asArray(style.textOutline);
  style.textOutlineRGBA = style.textOutlineRGBA.slice();
  style.textOutlineRGBA[3] = style.textOutlineOpacity;

  styleFill = new ol.style.Fill({color: style.fillRGBA});
  styleStroke = new ol.style.Stroke(
    {color: style.strokeRGBA, width: style.strokeWidth});

  font = "Normal " + style.iconFontSize + "pt '" + style.iconFontFamily + "'";

  // in case it's not set
  if (style.rotation === undefined)
    style.rotation = 0;

  styleTextIconFont = new ol.style.Text(
    {
    font: font,
    text: style.iconURL,
    textAlign: "center",
    textBaseline: "middle",
    rotation: style.rotation * Math.PI / 180.0,
    rotateWithView: true,
    fill: new ol.style.Fill({color: style.iconRGBA}),
    stroke: new ol.style.Stroke({color: style.iconOutlineRGBA, width: 2}),
    offsetX: 0,
    offsetY: 0
    });

/*
  if (style.iconURL !== undefined &&
      style.iconURL.indexOf("/") !== -1)
    {
    styleIcon = new ol.style.Icon(
      {
      //src: "/users/charthouse/support/weather-icons-2.0.10/_docs/gh-pages/favicons/favicon-32x32.png"
      anchor: [0, 0],
      src: style.iconURL,
      offset: [10, 10],
      size: [30, 30]
      });
    };
*/

  // set styles based on geometry type
  // this is so that, for example, a LineString doesn't also have
  // an icon on it
  var geometries;
  var index;
  var point;

  geometries = feature.getGeometry().getGeometries();
  for (index=0; index < geometries.length; ++index)
    {
    if (geometries[index].getType() === "Point")
      {
      var _style;

      if (styleIcon === undefined)
        {
        _style = new ol.style.Style(
          {
          geometry: geometries[index],
          text: styleTextIconFont
          });
        }
      else
        {
        _style = new ol.style.Style(
          {
          geometry: geometries[index],
          image: styleIcon
          });
        };
      styles.push(_style);
      point = geometries[index];
      }
    else if (geometries[index].getType() === "LineString")
      {
      var _style;

      _style = new ol.style.Style(
        {
        geometry: geometries[index],
        stroke: styleStroke
        });
      styles.push(_style);
      }
    else if (geometries[index].getType() === "Polygon")
      {
      var _style;

      _style = new ol.style.Style(
        {
        geometry: geometries[index],
        stroke: styleStroke,
        fill: styleFill
        });
      styles.push(_style);
      }
    else
      {
      console.log("styleGenerate(): " + geometries[index].getType() +
                  ": Unknown geometry type");
      };
    };

  if (point !== undefined && style.textOpacity > 0)
    {
    var styleText;

    styleText = new ol.style.Text(
      {
      text: getName(feature, true),
      textAlign: "left",
      textBaseline: "top",
      font: "Normal " + style.fontSize + "pt OpenSans",
      rotation: 0,
      rotateWithView: false,
      backgroundFill: new ol.style.Fill({color: style.textBackgroundRGBA}),
      fill: new ol.style.Fill({color: style.textRGBA}),
      stroke: new ol.style.Stroke({color: style.textOutlineRGBA, width: 2}),
      offsetX: 8,
      offsetY: 8,
      padding: [0, 0, 0, 0]
      });

    geometries.push(point);
    _style = new ol.style.Style(
      {
      geometry: point,
      text: styleText
      });
    styles.push(_style);
    };

  return(styles);
  }

function setStyles(features)
  {
  if (features == undefined || features.length <= 0)
    return;

  var index;

  // create style for each feature
  // DO NOT use a style function because it causes OpenLayers to
  // flicker the features badly.  set style directly instead.
  for (index=features.length-1; index >= 0; --index)
    {
    var styles;

    styles = styleGenerate(features[index]);

    // delete feature if styleGenerate() thinks it shouldn't be shown
    if (styles === false)
      features.splice(index, 1);
    else if (styles != undefined)
      features[index].setStyle(styles);
    };
  }

