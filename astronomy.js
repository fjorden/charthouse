
// Charthouse
// https://gitlab.com/fjorden/charthouse
// Copyright 2017-2019 Fjorden

// This was adapted from Fourmilab "Earthview" and
// http://www.jgiesen.de/astro/astroJS/siderealClock/sidClock.js
//  JavaScript Sun Table Calculator
// © 2001 Juergen Giesen
// http://www.jgiesen.de
// and
// http://www.lunar-occultations.com/rlo/ephemeris.htm

// Other good sources include
// http://aa.usno.navy.mil/faq/docs/GAST.php
// https://en.wikipedia.org/wiki/Position_of_the_Sun
// http://www.stargazing.net/kepler/sun.html
// http://www.geoastro.de/elevazmoon/basics/
// http://stjarnhimlen.se/comp/ppcomp.html

"use strict";

// Julian Day
Date.prototype.getJulianDay = function()
  {
  return(this.valueOf() / (1000*60*60*24) - 0.5 + 2440588);
  }

// Greenwich Mean Sidereal Time
Date.prototype.getGMST = function()
  {
  var MJD;
  var MJD0;
  var UT;
  var t_eph;
  var gmst;

  MJD = this.getJulianDay() - 2400000.5;
  MJD0 = Math.floor(MJD);
  UT = (MJD - MJD0) * 24.0;
  t_eph = (MJD0 - 51544.5) / 36525.0;
  gmst = 6.697374558 + 1.0027379093*UT + (8640184.812866 +
         (0.093104 - 0.0000062*t_eph) * t_eph) *t_eph / 3600.0;

  return(gmst%24);
  }

// https://stackoverflow.com/questions/26370688/convert-a-julian-date-to-regular-date-in-javascript
function JulianDayToDate(jd)
  {
  var millis;

  millis = (jd - 2440587.5) * 86400000;

  return(new Date(millis));
  }

// return Local Solar Time based on longitude
// this is approximate and can drift up to +/- 16m
// https://stackoverflow.com/questions/13314626/local-solar-time-function-from-utc-and-longitude
Date.prototype.getLST = function(longitude)
  {
  var utc;
  var lst;

  utc = this.getUTCHours() + (this.getUTCMinutes()/60);
  lst = (utc + longitude / 360 * 24) % 24;
  if (lst < 0)
    lst = 24 + lst;

  return(lst);
  }

function Body()
  {
  var latitude;
  var longitude;
  var posSet;

  // returns an array of latitude/longitue points of body's terminator
  // aa.quae.nl/en/antwoorden/zonpositie.html
  this.terminator = function(latitude, longitude)
    {
    var lat;
    var lon;
    var points=[];
    var point=[];
    var index;

    lat = latitude * Math.PI / 180; // radians
    lon = longitude * Math.PI / 180; // radians

    for (index=0; index < 360; index+=5)
      {
      var dist;
      var B;
      var x;
      var y;
      var L;
      var _point=[];

      dist = index * Math.PI / 180; // radians

      B = Math.asin(Math.cos(lat) * (Math.sin(dist)));
      B *= 180 / Math.PI; // latitude degrees
      B = Math.round(1000*B)/1000;

      x = (-1 * Math.cos(lon) * Math.sin(lat) * Math.sin(dist)) -
          (Math.sin(lon) * Math.cos(dist));
      y = (-1 * Math.sin(lon) * Math.sin(lat) * Math.sin(dist)) +
          (Math.cos(lon) * Math.cos(dist));
      L = Math.atan2(y, x);
      L *= 180 / Math.PI; // longitude degrees
      L = Math.round(1000*L)/1000;

      _point[0] = B;
      _point[1] = L;
      points.push(_point);
      };
    points.sort(function(a, b){return(a[1]-b[1]);});

    // add points to draw the northern or southern edges of the polygon
    point = [points[points.length-1][0], -179.999999];
    points.unshift(point);

    if (latitude > 0)
      point = [-89.999999, -179.999999];
    else
      point = [89.999999, -179.999999];
    points.unshift(point);

    point = [points[points.length-1][0], 179.999999];
    points.push(point);

    if (latitude > 0)
      {
      point = [-89.999999, 179.999999];
      points.push(point);
      point = [-89.999999, -179.999999];
      points.push(point);
      }
    else
      {
      point = [89.999999, 179.999999];
      points.push(point);
      point = [89.999999, -179.999999];
      points.push(point);
      };

    return(points);
    };

  this.terminatorGeoJSON = function(latitude, longitude, fillOpacity,
                                    uuid, name)
    {
    var terminator;
    var feature;
    var index;

    terminator = this.terminator(latitude, longitude);

    feature = setFeatureDefaults(feature);
    feature.properties.datetime.timeStale = 3600;
    feature.properties.datetime.timeExpire = 3600;
    feature.geometry = {};
    feature.geometry.type = "GeometryCollection";
    feature.geometry.geometries = [];
    feature.geometry.geometries[0] = {};
    feature.geometry.geometries[0].type = "Point";
    feature.geometry.geometries[0].coordinates = [];
    feature.geometry.geometries[0].coordinates[0] = longitude;
    feature.geometry.geometries[0].coordinates[1] = latitude;

    feature.geometry.geometries[1] = {};

/*
    feature.geometry.geometries[1].type = "LineString";
    feature.geometry.geometries[1].coordinates = [];

    for (index=0; index < terminator.length; ++index)
      {
      feature.geometry.geometries[1].coordinates[index] = [];
      feature.geometry.geometries[1].coordinates[index][0] =
        terminator[index][1];
      feature.geometry.geometries[1].coordinates[index][1] =
        terminator[index][0];
      };
*/


    feature.geometry.geometries[1].type = "Polygon";
    feature.geometry.geometries[1].coordinates = [];
    feature.geometry.geometries[1].coordinates[0] = [];

    for (index=0; index < terminator.length; ++index)
      {
      feature.geometry.geometries[1].coordinates[0][index] = [];
      feature.geometry.geometries[1].coordinates[0][index][0] =
        terminator[index][1];
      feature.geometry.geometries[1].coordinates[0][index][1] =
        terminator[index][0];
      };


    feature.properties.node.uuid = uuid;
    feature.properties.node.name = name;
    feature.properties.style.fill = "#000";
    feature.properties.style.fillOpacity = fillOpacity;
    feature.properties.style.textOpacity = 0;
    feature.properties.style.iconOpacity = 0;
    feature.properties.style.strokeOpacity = 0;
    feature.properties.style.zoom = 0;

    return(feature);
    };

  this.GeoJSON = function(latitude, longitude, uuid, name, icon,
                          fontfamily, fontsize, color, outline)
    {
    var feature;

    feature = setFeatureDefaults(feature);
    feature.geometry = {};
    feature.geometry.type = "GeometryCollection";
    feature.geometry.geometries = [];
    feature.geometry.geometries[0] = {};
    feature.geometry.geometries[0].type = "Point";
    feature.geometry.geometries[0].name = "position";
    feature.geometry.geometries[0].coordinates = [];
    feature.geometry.geometries[0].coordinates[0] = longitude;
    feature.geometry.geometries[0].coordinates[1] = latitude;
    feature.properties.node.uuid = uuid;
    feature.properties.node.name = name;
    feature.properties.position = {};
    feature.properties.position.latitude = latitude;
    feature.properties.position.longitude = longitude;

    feature.properties.style.iconURL = icon;
    feature.properties.style.iconFontFamily = fontfamily;
    feature.properties.style.iconFontSize = fontsize;
    feature.properties.style.icon = color;
    feature.properties.style.iconOutline = outline;
    feature.properties.style.textOpacity = 0;
    feature.properties.style.zoom = 0;

    return(feature);
    };
  }

// Sun Object
function Sun()
  {
  var body;
  // XXX set latitude/longitude and time so we only call subSolar() every
  // so often

  body = new Body();

  // returns declination and right ascension in degrees
  this.declinationRA = function(julianDay)
    {
    var T;
    var L0;
    var M;
    var C;
    var theta;
    var omega;
    var lambda;
    var eps0;
    var eps;
    var decl;
    var RA;

    T = (julianDay - 2451545.0) / 36525.0;
    L0 = 280.46645 + (36000.76983 + 0.0003032 * T) * T;
    M = 357.52910 + (35999.05030 - (0.0001559 * T + 0.00000048 * T) * T) * T;
    M *= Math.PI / 180.0; // radians
    C = (1.914600 - 0.004817 * T - 0.000014 * T * T) * Math.sin(M) +
        (0.019993 - 0.000101 * T) * Math.sin(2*M) + 0.000290 * Math.sin(3*M);
    theta = L0 + C;
    omega = 125.04 - 1934.136 * T;
    omega *= Math.PI / 180.0; // radians
    lambda = theta - 0.00569 - 0.00478 * Math.sin(omega);
    lambda *= Math.PI / 180.0; // radians
    eps0 =  23.0 + 26.0/60.0 + 21.448/3600.0 -
            (46.8150*T + 0.00059*T*T - 0.001813*T*T*T)/3600;
    eps = eps0 + 0.00256*Math.cos(omega);
    eps *= Math.PI / 180.0; // radians
    decl = Math.sin(eps) * Math.sin(lambda);
    decl = Math.asin(decl);
    decl *= 180.0 / Math.PI; // degrees
    RA = Math.atan2(Math.cos(eps)*Math.sin(lambda), Math.cos(lambda));
    RA *= 180.0 / Math.PI; // degrees
    if (RA < 0)
      RA = RA + 360;

    decl = Math.round(10000*decl)/10000;
    RA = Math.round(10000*RA)/10000;

    return([decl, RA]);
    };

  // return latitude/longitude in degrees
  this.subSolar = function()
    {
    var dt;
    var decl;
    var ra;
    var latitude;
    var longitude;
    var declRA;
    var GMST;
    var point=[];

    dt = new Date();

    declRA = this.declinationRA(dt.getJulianDay());
    GMST = dt.getGMST();

    //console.log("declination=" + declRA[0] + " RA=" + declRA[1]);
    //console.log("gmst=" + GMST);

    latitude = declRA[0]; // declination
    latitude = Math.round(10000*latitude)/10000;
    longitude = declRA[1] - (GMST * 15);
    if (longitude < -180)
      longitude += 360;
    if (longitude > 180)
      longitude -= 360;
    longitude = Math.round(10000*longitude)/10000;

    point.push(latitude);
    point.push(longitude);

    return(point);
    };

  // https://en.wikipedia.org/wiki/Sunrise_equation
  this.getNextSunRise = function(latitude, longitude)
    {
    var n;
    var JM;  // mean solar noon
    var M;   // solar mean anomaly (degrees)
    var Mr;  // solar mean anomaly (radians)
    var C;   // equation of the center
    var lambda; // ecliptic longitude
    var Jtransit; // solar transit
    var sinDecl; // sin(declination)
    var cosDecl; // cos(declination)
    var latitudeR; // latitude (radians)
    var longitudeR; // longitude (radians)

    latitudeR = latitude * Math.PI / 180; // radians
    longitudeR = longitude * Math.PI / 180; // radians

    n = (new Date()).getJulianDay() - 2451545 + 0.0008;
    JM = n - (longitude / 360);
    M = (357.5291 + 0.98560028 * JM) % 360;
    Mr = M * Math.PI / 180.0; // radians
    C = 1.9148 * Math.sin(Mr) + 0.02 * Math.sin(2*Mr) +
        0.0003 * Math.sin(3*Mr);
    lambda = (M + C + 180 + 102.9372) % 360;

    Jtransit = 2451545.5 + JM + 0.0053 * Math.sin(Mr)

    sinDecl = Math.sin(longitudeR) * Math.sin(23.44*Math.PI/180);
    cosDecl = Math.cos(Math.asin(sinDecl));

    cosOmega = (Math.sin(-0.83*Math.PI/180) - Math.sin(latitudeR) * sinDecl) /
               (Math.cos(latitudeR) * cosDecl);
    omega = Math.acos(cosOmega) * 180 / Math.PI; // degrees
    Jset = Jtransit + (omega / 360);
    Jrise = Jtransit - (omega / 360);

//console.log(JulianDayToDate(Jset), JulianDayToDate(Jrise));
//console.log(JulianDayToDate(Jset));
    };

  this.GeoJSON = function()
    {
    var latlon;

    latlon = this.subSolar();

    // wi-day-sunny icon
    return(body.GeoJSON(latlon[0], latlon[1], "Sun", "Sun", "\uF00D",
                        "weathericons", 20, "#FA0", "#FFF"));
    };

  this.terminatorGeoJSON = function()
    {
    var latlon;
    var feature;

    latlon = this.subSolar();

    feature = body.terminatorGeoJSON(latlon[0], latlon[1], 0.1,
      "Solar Terminator", "Solar Terminator");

    return(feature);
    };
  }

// Moon Object
function Moon()
  {
  var body;

  body = new Body();

  function datan(x) { return(180/ Math.PI * Math.atan(x)); };

  function datan2(y, x)
    {
    var a;

    if ((x == 0) && (y == 0))
      {
      return(0);
      }
    else
      {
      a = datan(y / x);
      if (x < 0)
        a = a + 180;
      if (y < 0 && x > 0)
        a = a + 360;
      return(a);
      };
    }

  function dsin(x) { return(Math.sin(Math.PI / 180 * x)); }

  function dasin(x) { return(180/ Math.PI * Math.asin(x)); }

  function dcos(x) { return(Math.cos(Math.PI / 180 * x)); }

  function dtan(x) { return(Math.tan(Math.PI / 180 * x)); }

  function range(x)
    {
    var a
    var b;

    b = x / 360;
    a = 360 * (b - ipart(b));
    if (a  < 0)
      a = a + 360;

    return(a);
    }

  function ipart(x)
    {
    var a;

    if (x> 0)
      a = Math.floor(x);
    else
      a = Math.ceil(x);

    return(a);
    }

  // return latitude/longitude in degrees
  // http://stjarnhimlen.se/comp/ppcomp.html
  this.subLunar = function()
    {
    var days;
    var t;
    var F;
    var L1;
    var M1;
    var C1;
    var V1;
    var Obl;
    var Ec1;
    var R1;
    var Th1;
    var Om1;
    var Lam1;
    var L2;
    var Om2;
    var M2;
    var D;
    var D2;
    var R2;
    var R3;
    var Bm;
    var Lm;
    var HLm;
    var HBm;
    var Ra2;
    var Dec2;
    var GMST;
    var latitude;
    var longitude;
    var point=[];

    days = (new Date).getJulianDay() - 2451545;
    t = days / 36525;

    // Sun formulas
    L1 = range(280.466 + 36000.8 * t);
    M1 = range(357.529+35999*t - 0.0001536* t*t + t*t*t/24490000);
    C1 = (1.915 - 0.004817* t - 0.000014* t * t)* dsin(M1);
    C1 = C1 + (0.01999 - 0.000101 * t)* dsin(2*M1);
    C1 = C1 + 0.00029 * dsin(3*M1);
    V1 = M1 + C1;
    Ec1 = 0.01671 - 0.00004204 * t - 0.0000001236 * t*t;
    R1 = 0.99972 / (1 + Ec1 * dcos(V1));
    Th1 = L1 + C1;
    Om1 = range(125.04 - 1934.1 * t);
    Lam1 = Th1 - 0.00569 - 0.00478 * dsin(Om1);
    Obl = (84381.448 - 46.815 * t)/3600;

    //  Moon formulas
    //  F   - Argument of latitude (F)
    //  L2  - Mean longitude (L')
    //  Om2 - Long. Asc. Node (Om')
    //  M2  - Mean anomaly (M')
    //  D   - Mean elongation (D)
    //  D2  - 2 * D
    //  R2  - Lunar distance (Earth - Moon distance)
    //  R3  - Distance ratio (Sun / Moon)
    //  Bm  - Geocentric Latitude of Moon
    //  Lm  - Geocentric Longitude of Moon
    //  HLm - Heliocentric longitude
    //  HBm - Heliocentric latitude
    //  Ra2 - Lunar Right Ascension
    //  Dec2- Declination

    F = range(93.2721 + 483202 * t - 0.003403 * t* t - t * t * t/3526000);
    L2 = range(218.316 + 481268 * t);
    Om2 = range(125.045 - 1934.14 * t + 0.002071 * t * t + t * t * t/450000);
    M2 = range(134.963 + 477199 * t + 0.008997 * t * t + t * t * t/69700);
    D = range(297.85 + 445267 * t - 0.00163 * t * t + t * t * t/545900);
    D2 = 2*D;
    R2 = 1 + (-20954 * dcos(M2) - 3699 * dcos(D2 - M2) - 2956 *
         dcos(D2)) / 385000;
    R3 = (R2 / R1) / 379.168831168831;
    Bm = 5.128 * dsin(F) + 0.2806 * dsin(M2 + F);
    Bm = Bm + 0.2777 * dsin(M2 - F) + 0.1732 * dsin(D2 - F);
    Lm = 6.289 * dsin(M2) + 1.274 * dsin(D2 -M2) + 0.6583 * dsin(D2);
    Lm = Lm + 0.2136 * dsin(2*M2) - 0.1851 * dsin(M1) - 0.1143 * dsin(2 * F);
    Lm = Lm +0.0588 * dsin(D2 - 2*M2);
    Lm = Lm + 0.0572* dsin(D2 - M1 - M2) + 0.0533* dsin(D2 + M2);
    Lm = Lm + L2;
    Ra2 = datan2(dsin(Lm) * dcos(Obl) - dtan(Bm)* dsin(Obl), dcos(Lm));
    Dec2 = dasin(dsin(Bm)* dcos(Obl) + dcos(Bm)*dsin(Obl)*dsin(Lm));
    HLm = range(Lam1 + 180 + (180/Math.PI) * R3 * dcos(Bm) * dsin(Lam1 - Lm));
    HBm = R3 * Bm;

    GMST = (new Date()).getGMST();
    latitude = Dec2;
    latitude = Math.round(10000*latitude)/10000;
    longitude = Ra2 - (GMST * 15);
    if (longitude < -180)
      longitude += 360;
    if (longitude > 180)
      longitude -= 360;
    longitude = Math.round(10000*longitude)/10000;

    point.push(latitude);
    point.push(longitude);

    return(point);
    };

  this.GeoJSON = function()
    {
    var latlon;
    var icon;

    latlon = this.subLunar();

    icon = this.phaseIcon(true);

    // wi-day-sunny icon
    return(body.GeoJSON(latlon[0], latlon[1], "Moon", "Moon", icon,
                        "weathericons", 20, "#FFF", "#555"));
    };

  this.terminatorGeoJSON = function()
    {
    var latlon;
    var feature;

    latlon = this.subLunar();

    feature = body.terminatorGeoJSON(latlon[0], latlon[1], 0.1,
      "Lunar Terminator", "Lunar Terminator");

    return(feature);
    };

  this.illumination = function(dt)
    {
    if (dt === undefined)
      dt = new Date();

    return(Math.round(SunCalc.getMoonIllumination(dt).fraction*100));
    };

  this.phase = function()
    {
    return(SunCalc.getMoonIllumination(new Date()).phase);
    };

  // if unicode == true, then return the "weathericons" unicode for the
  // phase icon
  this.phaseIcon = function(unicode)
    {
    var phase;
    var moonIcon;

    phase = this.phase();

    /*
    0     new
    0.0625
    0.125 waxing crescent
    0.1875
    0.25  first quarter
    0.3125
    0.375 waxing gibbous
    0.4375
    0.5   full moon
    0.5625
    0.625 waning gibbous
    0.6875
    0.75  last quarter
    0.8125
    0.875 waning crescent
    0.9375
    1     new
    */

    if (unicode === true)
      {
      if (phase <= 0.0625)
        moonIcon = "\uF095";    // wi-moon-new
      else if (phase <= 0.1875)
        moonIcon = "\uF097";    // wi-moon-waxing-crescent-2
      else if (phase <= 0.3125)
        moonIcon = "\uF09C";    // wi-moon-first-quarter
      else if (phase <= 0.4375)
        moonIcon = "\uF09E";    // wi-moon-waxing-gibbous-2
      else if (phase <= 0.5625)
        moonIcon = "\uF0A3";    // wi-moon-full
      else if (phase <= 0.6875)
        moonIcon = "\uF0A5";    // wi-moon-waning-gibbous-2
      else if (phase <= 0.8125)
        moonIcon = "\uF0AA";    // wi-moon-third-quarter
      else if (phase <= 0.9375)
        moonIcon = "\uF0AC";    // wi-moon-waning-crescent-2
      else if (phase <= 1)
        moonIcon = "\uF095";    // wi-moon-new
      }
    else
      {
      if (phase <= 0.0625)
        moonIcon = "New <i class='wi wi-moon-alt-new fa-2x wi-fw'></i>";
      else if (phase <= 0.1875)
        moonIcon = "Waxing crescent " +
          "<i class='wi wi-moon-alt-waxing-crescent-2 fa-2x wi-fw'></i>";
      else if (phase <= 0.3125)
        moonIcon = "First quarter " +
          "<i class='wi wi-moon-alt-first-quarter fa-2x wi-fw'></i>";
      else if (phase <= 0.4375)
        moonIcon = "Waxing gibbous" +
          "<i class='wi wi-moon-alt-waxing-gibbous-2 fa-2x wi-fw'></i>";
      else if (phase <= 0.5625)
        moonIcon = "Full <i class='wi wi-moon-alt-full fa-2x wi-fw'></i>";
      else if (phase <= 0.6875)
        moonIcon = "Waning gibbous " +
          "<i class='wi wi-moon-alt-waning-gibbous-2 fa-2x wi-fw'></i>";
      else if (phase <= 0.8125)
        moonIcon = "Last quarter " +
          "<i class='wi wi-moon-alt-third-quarter fa-2x wi-fw'></i>";
      else if (phase <= 0.9375)
        moonIcon = "Waning crescent " +
          "<i class='wi wi-moon-alt-waning-crescent-2 fa-2x wi-fw'></i>";
      else if (phase <= 1)
        moonIcon = "New <i class='wi wi-moon-alt-new fa-2x wi-fw'></i>";
      };

    return(moonIcon);
    };

  this.nextFull = function(dt)
    {
    var index;

    if (dt === undefined)
      dt = new Date();

    for (index=0; index < 40; ++index)
      {
      var illum;

      illum = this.illumination(dt);
      if (illum == 100)
        return(dt);

      dt.setUTCDate(dt.getUTCDate()+1);
      };

    return(undefined);
    };

  this.nextNew = function(dt)
    {
    var index;

    if (dt === undefined)
      dt = new Date();

    for (index=0; index < 40; ++index)
      {
      var illum;

      illum = this.illumination(dt);
      if (illum == 0)
        return(dt);

      dt.setUTCDate(dt.getUTCDate()+1);
      };

    return(undefined);
    };
  };

function dataFunctionSun(obj)
  {
  var sun;
  var featureSun;
  var featureTerminator;
  var features=[];

  sun = new Sun();

  featureSun = sun.GeoJSON();
  featureTerminator = sun.terminatorGeoJSON();

  features.push(featureSun);
  features.push(featureTerminator);

  workerCBcall(features, "dataFunctionSun()",
    getLayerByName("Sun & Solar Illumination").uuid, "Sun", "Overlays");
  }

function dataFunctionMoon(obj)
  {
  var moon;
  var featureMoon;
  var featureTerminator;
  var feature;
  var features=[];

  moon = new Moon();

  featureMoon = moon.GeoJSON();
  featureTerminator = moon.terminatorGeoJSON();

  features.push(featureMoon);
  features.push(featureTerminator);

  workerCBcall(features, "dataFunctionMoon()",
    getLayerByName("Moon & Lunar Illumination").uuid, "Moon", "Overlays");
  }

