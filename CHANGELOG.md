
# Charthouse

## 1.2.19 (2020-02-25)
- Upgraded to OpenLayers 5.3.0.
- Upgraded to UIkit 3.0.2.
- Upgraded to Font Awesome 5.6.3.
- Added some new icons.
- Changed follow icon to pushpin.
- Fixed Python2 GeoJSON "id" output bug.
- Fixed Python features pathSize truncation.
- Fixed adsb MQTT packets and ported to Python2.
- Fixed adsb feature timeouts.
- Fixed feature following.
- Decreased feature following interval.
- Fixed stale feature fading.
- Decreased default opacity of OpenStreetMap.
- Modals are no offcanvases.
- Added error checking to utils/adsb.
- Added error checking to layerClear().
- Removed normalize.css.

## 1.2.18 (2018-11-01)
- Upgraded to UIkit 3.0.0-rc.20.
- Upgraded to OpenLayers 5.2.0.
- Upgraded to Gov Icons 1.4.1.
- Upgraded to Font Awesome 5.4.2.
- Removed Material Design Icons.
- Fixed output of adsb utility.
- Added Python bottle, bullseye, campground, crosshairs,
  hammer, hiking, network, running, and tractor icons.
- Improved Python fan, mountain, and poision icons.
- Added dms2dd() and dd2dms() to JavaScript.
- Tweaked some icon sizes.
- Added pathSize option to Python features add() function.

## 1.2.17 (2018-09-16)
- Added Flask test application.
- Added framebuster.
- Improved help contents.
- Improved look of measure popup.
- Improved name trucation.
- Switched measure tool to show range ring.
- Improved how debug JavaScript file is built.
- Improved checking for sassc or pscss.
- Upgraded to UIkit 3.0.0-rc16.
- Upgraded to Font Awesome Free 5.3.1 web.
- Added "trackShow" style property to features.
- Added setStyleIconFontSize() and setStyleTrackShow() to Python code.
- mqttjsond now removes file if number of features is zero.
- Fixed follow button in toolbar.
- Fixed aim icon.

## 1.2.16 (2018-07-23)
- Notifications' z-index above everything.
- Makefile checks for different SCSS preprocessors.
- Added slight hover delay to feature popup.
- Feature popup closes after a few seconds.
- Improved look of statistics charts.
- Upgraded to Font Awesome Free 5.1.1 web.
- Switched many fonts to Font Awesome.
- Rotated map stays rotated.
- Upgraded to OpenLayers 5.1.3.
- Python icons switching to mostly Font Awesome 5.
- Python generates minimalist style, map fills in the rest.
- Decreased some decimals points in Python, super accuracy not necessary.

## 1.2.15 (2018-06-26)
- Added "Email link to map view" button.
- Improved button colors.
- Fixed z-index of modals when a header is present.
- Added "Mouse" section to help.
- Added better error correction to Python module.
- In Python features module, only add points to existing tracks.
- Improved efficiency of Python features add().
- OpenSky util now runs in a loop.

## 1.2.14 (2018-06-20)
- Generate JavaScript code based on Python feature icons and related info.
- Added "icon" and "color" parameters to setFeatureDefaults().
- Added more icons to Python module.
- Improved Python module icons.
- Fixed typo in index.html.
- Miscellaneous code cleanup.
- Python module works with Python 2.x.
- Added names to GeoJSON LineStrings and Polygons.
- Updated UIkit to v3.0.0-rc.5.
- Added Chart.js v2.7.2.
- Added rudimentary statistics charts (but currently disabled).
- Improved look of "keys" in help table.
- Names on map are now truncated to 10 characters.
- Ported Python module to 2.x.

## 1.2.13 (2018-06-04)
- Truncate displayed icon name if too long.
- Improved error checking in Python module functions.
- Moved from github.com to gitlab.com.

## 1.2.12 (2018-06-03)
- Added more icons to Python module.
- Added dataFunction() helper workerCBcall().
- Consolidated all CSS to one file.
- Improved various Python and HTML icons.
- Made sections non-selectable.
- Map can now handle points, ellipses, and aimpoints in a
  GeometryCollection; Python module outputs these in a GeometryCollection.
- Improved Python feature colors.
- Fixed a bug in MQTT Web Worker.

## 1.2.11 (2018-05-30)
- Added "use strict" (although uglifyjs strips it (for now)).
- Fixed "data functions" (see dataFunctionTest()).
- Improved feature defaults in setFeatureDefaults().
- Improved workerCB() error messages.
- Added a couple more icons to Python module.
- Fixed Sun and Moon "data functions".
- Fixed solar and lunar terminators.
- Terminators do not show up in feature popup menus.
- Fixed browser geolocation.

## 1.2.10 (2018-05-21)
- Added new "features" object in Python library.
- Improved error checking of feature object functions.
- Fixed opensky utility to work properly with feature object.
- Fixed altitude setting in feature object.
- adsb utility now uses feature Python object.
- Improved how feature styles are set
- Feature styles now work nicely with LineStrings in GeomeetryCollections.
- Turned off Fitler and User Markers because they're not working, need to
  fix.
- Python Feature object now draws line to desired aimpoint in addition to
  actual aimpoint.
- Fixed header with new navbar across top.
- Added Open Sans font.
- Added TopoJSON layer support.
- Removed Material Design Iconic Font.
- Improved reliability of mqttjsond.

## 1.2.9 (2018-05-03)
- Improved look by using navbar across top.
- Fixed an error where getElementById() is called before element exists.
- Improved output of adsb and mqttjsond utilities.
- Added featureCompare() and featureAdd() to Python library.
- Fixed how background is reset.
- Added help icon and modal window.
- Added Data Layers "Select all" button (to go with "Deselect all").
- Added more icons to Python featureStyle().
- Added "fake" utility.
- Added debug messages to mqttjsond.
- Added new "feature" object in Python library.
- Fixed orientation of satellite icon.
- sortGeo() now works properly with all geometry types.

## 1.2.8 (2018-04-18)
- Switched more icons from Material Design Iconic Font.
- Fixed some error messages.
- Added a polyfill for old Chrome browsers.
- Added beginnings of MQTT password support.
- Added featureStyle() to Python library.
- Upgraded to Font Awesome Free 5.0.10.

## 1.2.7 (2018-04-16)
- Better measure icon.
- Improved setting permissions during install.
- Improved latitude/longitude error checking for adsb utility.
- Updated Material Design Icons.
- Material Design Icons now works on iOS 9 browsers.

## 1.2.6 (2018-04-12)
- Upgraded to Govicons 1.4.0.
- Improved icons.
- Fixed rotation (heading) of a couple of Govicons.
- Fixed "name" output in opensky utility.
- Improved GeoJSON error checking from WebWorkers.
- Added APRS utility.
- Added featureIsExpired(), featureIsStale(), and featuresAgeOff() to
  Python library.
- Added mqttjsond utility.
- Added adsb utility.
- Added Chartbundle Aviation Charts.

## 1.2.5 (2018-03-27)
- Fixed checking for feature course made good (cmg).
- Added some more layers to config.
- Fixed Material Design Icons for older browsers.
- Improved placement of Material Design Icons (CSS).
- Slight improvement to OpenSky utility.
- Upgraded to OpenLayers 4.6.5.
- Added background image to splash screen.
- Fixed layer loading so "visibility" is properly set.
- Added Noto Emoji.
- Fixed Python library.

## 1.2.4 (2018-03-07)
- Removed moted stats file checking.
- Added a "Deselect all" button to "Data sources" menu.
- Made toolbar buttons a tad smaller.
- Fixed user markers.
- Fixed focus problem with search and filter fields.
- Added Material Design Icons (Material Design Iconic Font hasn't had any
  updates in a long time).
- Converted a lot of icons to Material Design Icons.
- Added "spin" and "rotate-90" classes to CSS.

## 1.2.3 (2018-03-04)
- Added ability to send "error", "notice", and "warning" messages via MQTT.
- Added Polyfills for Turf on old Chrome browser versions.
- Added ellipse function to Python module.
- Added mqttall utility.
- Removed setting of feature properties in Python module.
- Set name and UUID in feature defaults.
- Added browser geolocation capability.
- Can now receive FeatureCollections via MQTT.
- Python module outputs FeatureCollections.
- Fixed mouse clicking/hovering problem near toolbar.
- Fixed uuid setting for feature defaults.
- Decreased amount of time "Too many features..." message is shown.
- Upgraded to UIkit 3.0.0-beta40.
- Upgraded to Font Awesome Free 5.0.7.
- Worker JavaScript now goes through Uglify.
- Added "withCredentials" option to xhttpGet().
- Added adsbexchange utility.
- Fixed timeStale and timeExpire feature defaults.

## 1.2.2 (2018-01-28)
- Improved "make clean".
- Fixed layer table generation.
- Error check if CharthouseVersion element id exists.
- Fixed error checking MQTT JSON parsing.
- Improved look/consistency of button icons.
- Fixed ageOff() for when feature is missing datetime.
- Fixed footer placement.
- Fixed other elements' placement in relation to header and footer.
- Fixed long URLs in notifications.
- Improved feature defaults.
- Added polyfill for Element.prototype.matches for UIKit on older Firefox
  versions.
- Reverted to Turfjs 5.0.4 for older Firefox versions.
- Upgraded to MQTT.js 2.15.1.
- Fixed index.html text centering.
- Centered text in notifications.
- Check for WebSocket support in MQTT WebWorker (older Firefox versions).
- Improved error messaging for MQTT WebWorker.
- Added ADS-B Exchange test utility.
- Added CoT test utility.

## 1.2.1 (2018-01-15)
- Added OpenSky test utility.
- Removed Pyephem (no longer in use).
- Added moon rise/set and next new/full to illumination popup.
- Fixed bug if local solar time (LST) is less than zero.
- Added ability to subscribe to multiple MQTT topics.
- Added Node-RED Mosquitto MQTT Status Flow.
- Added text background, background opacity, and outline opacity
  style settings.
- Improved style defaults.
- Fixed for sun rise/set for locations that have midnight sun.
- Added version to About modal.
- Greatly improved Python library build.

## 1.2.0 (2018-01-09)
- Now using UIkit for widgets/layout.
- Added MQTT client support (experimental).
- Improved compass rose.
- Improved error checking of features in onMouseMove().
- Fixed error in displaying rxGood/rxBad percentage.
- Fixed bug where feature popup doesn't disappear.
- Added "noscript" tag to HTML in case JavaScript is off.
- Upgraded to OpenLayers 4.6.4.
- Upgraded to Turfjs 5.1.6.
- Upgraded to Geodesy 1.1.2.
- Upgraded to Normalize 7.0.0.
- Upgraded to python-geojson 2.3.0.
- Upgraded to Font Awesome 5.0.3.

## 1.1.5 (2017-11-06)
- Updated to OpenLayers 4.4.1.
- Fixed follow button.
- Fixed button badge location.
- Added GPS and RFM data to feature popup.
- Added "Near-real-time" data set to sample cofiguration.
- Web worker removes features that don't have a geometry.
- XYZ map types now work well (should give better performance).
- Now sets feature rotation based on JSON attitude heading.
- Added cachebuster option to xhttpGet().
- Fixed bug in dateString() output.

## 1.1.4 (2017-07-04)

- Added preliminary support for dropdown menus in toolbar.
- Various UI improvements.
- Cursor no longer changes back to default when dragging.
- Selected features are no longer erroneously left on the map.
- Clear layer if there's an error (e.g. 404).

## 1.1.3 (2017-06-18)

- Added setHeader() and setFooter() functions.
- Sun/moon positions are calculated in JavaScript (PHP no longer needed).
- Various UI improvements.
- Upgraded to OpenLayers v4.2.0.

## 1.1.2 (2017-06-11)

- Added various utilities.
- Added ability for users to add/delete markers.
- No longer using php/python for Sun/Moon information,
  now done in JavaScript.
- Added "cache buster" back so GeoJSON isn't cached.
- Run JavaScript through UglifyJS2.
- Fixed various variable declarations.
- Improved how/when Web Workers are called.
- Improved settings error checking.
- Various bug fixes and UI improvements.

## 1.1.1 (2017-05-29)

- Improved many UI elements' look/feel.
- Improved timing of layer reloads.
- Created a "web worker" pool so too many are not spawned.
- Added "data function" capability to layers.
- Added tooltips to fields and buttons.
- Fixed show/hide center crosshair.
- Improved settings error checking.

## 1.1.0 (2017-05-21)

- Build using UglifyJS2.
- Improved loading performance.
- Lots of big and little UI improvements.
- Improved error checking of settings.
- Added some polyfills for older browsers.
- Upgraded to OpenLayers 4.1.1.
- Fixed measure, search, filter, and follow.
- Fixed heatmaps.
- Removed a bunch of dependencies (e.g. JQuery).

## 1.0.0 (2017-05-02)

- First official release.

## 0.9.0 (2017-02-06)

- Getting close to a release.

## 0.0.0 (2017-02-06)

- Initial commit.

# Copyright

Copyright 2017 Fjorden
https://gitlab.com/fjorden/charthouse

