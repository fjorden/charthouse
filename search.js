
// Charthouse
// https://gitlab.com/fjorden/charthouse
// Copyright 2017-2019 Fjorden

"use strict";

var searchRE;
var searchString;

function toggleSearchUpdate(count)
  {
  var html;

  html = buttonSearch.innerHTML;
  html = html.replace(/<br><span.*span>/, "");
  if (count >= 0)
    html += "<br><span class='tiny badge-tiny green'>" + count + "</span>";

  buttonSearch.innerHTML = html;
  }

function featuresCircle(centers)
  {
  if (centers == undefined || centers.length == 0 ||
      searchAnimateCount == -1 || searchAnimateCount >= 5)
    {
    toggleSearchUpdate(-1);
    searchAnimateCount = 0;
    return;
    };

  var rgba;
  var style;
  var index;

  rgba = ol.color.asArray("#F00");
  rgba = rgba.slice();
  rgba[3] = 0.5;

  style = new ol.style.Style(
    {
    image: new ol.style.Circle(
      {
      radius: 20,
      points: 1,
      fill: new ol.style.Fill(
        {
        color: rgba
        })
      })
    });

  for (index=0; index < centers.length; ++index)
    {
    var feature;

    feature = new ol.Feature(new ol.geom.Point(centers[index]));
    flash(feature);
    };

  toggleSearchUpdate(centers.length);

  ++searchAnimateCount;
  setTimeout(function() { featuresCircle(centers); }, 2000);
  }

function featuresSearch(inputId)
  {
  if (inputId == null || inputId == undefined)
    return(-1);

  var index;
  var centers=[];

  searchAnimateCount = 0;

  searchString = document.getElementById(inputId).value;
  if (searchString.length == 0)
    {
    searchString = undefined;
    return(-1);
    };

  searchRE = new RegExp(searchString, "i");

  if (settings["advanced"]["showDebug"])
    console.log("featureSearch(" + inputId + "):", searchRE);

  for (index=0; index < layersData.length; ++index)
    {
    var features;
    var jndex;

    features = layersData[index].layer.getSource().getFeatures();
    for (jndex=0; jndex < features.length; ++jndex)
      {
      var center;
      var name;
      var uuid;
      var text;

      center = centerOfExtent(features[jndex].getGeometry().getExtent());

      name = getName(features[jndex]);
      if (name != undefined && name.length > 0 && name.match(searchRE))
        centers.push(center);

      uuid = getUUID(features[jndex]);
      if (uuid != undefined && uuid.length > 0 && uuid.match(searchRE))
        centers.push(center);

      text = getText(features[jndex], false);
      if (text != undefined && text.length > 0 && text.match(searchRE))
        centers.push(center);
      };
    };

  // sort and uniq
  // stackoverflow.com/questions/4833651/javascript-array-sort-and-unique
  //centers = centers.reduce((a, x) => a.includes(x) ? a : [...a, x], []).sort()
  centers = sort_unique(centers);

  if (centers.length == 0)
    {
    UIkit.notification({status: "warning", message:
      "<div class='ch-center'>" +
      "<i class='fas fa-exclamation-circle fa-fw'></i> " +
      "No feature names match:</div>" +
      "<div class='ch-center ch-wordbreak'>" + searchString + "</div>" +
      "<div>Modify the name and search again.</div>"});

    return(0);
    }
  else if (centers.length > 100)
    {
    UIkit.notification({status: "warning", message:
      "<div class='ch-center'>" +
      "<i class='fas fa-exclamation-circle fa-fw'></i> " +
      "Too many feature names match:</div>" +
      "<div class='ch-center ch-wordbreak'>" + searchString + "</div>" +
      "<div>Make the name more specific or zoom in to decrease " +
      "the search area.</div>"});

    return(centers.length);
    };

  featuresCircle(centers);

  return(centers.length);
  }

/* http://openlayers.org/en/latest/examples/feature-animation.html */
function flash(feature)
  {
  var start = new Date().getTime();
  var listenerKey;

  function animate(event)
    {
    var duration=2000;
    var vectorContext = event.vectorContext;
    var frameState = event.frameState;
    var flashGeom = feature.getGeometry().clone();
    var elapsed = frameState.time - start;
    var elapsedRatio = elapsed / duration;
    // radius will be 5 at start and 30 at end.
    var radius = ol.easing.easeOut(elapsedRatio) * 25 + 5;
    var opacity = ol.easing.easeOut(1 - elapsedRatio);

    var style = new ol.style.Style(
      {
      image: new ol.style.Circle(
        {
        radius: radius,
        snapToPixel: false,
        stroke: new ol.style.Stroke(
          {
          color: "rgba(255,80,0," + opacity + ")", // #F50 Int'l Orange
          width: 3 + opacity
          })
        })
      });

    vectorContext.setStyle(style);
    vectorContext.drawGeometry(flashGeom);
    if (elapsed > duration)
      {
      ol.Observable.unByKey(listenerKey);
      return;
      };
    // tell OpenLayers to continue postcompose animation
    map.render();
    };

  listenerKey = map.on('postcompose', animate);
  }

