# -*- coding: utf-8 -*-

# Charthouse
# https://gitlab.com/fjorden/charthouse
# Copyright 2018-2019 Fjorden

import json
import geojson
import uuid
import time
from copy import deepcopy
from geographiclib.geodesic import Geodesic
import math

class feature:

  # minimize the use of Unicode characters because different OSes and/or
  # browsers may draw them differently.
  iconlist = \
    [
    # name           font              character size SVG
    ["A",            "OpenSans",       "A",        9, ""],
    ["airplane",     "FontAwesome",    u"\uF072",  8, ""], # plane
    ["airport",      "FontAwesome",    u"\uF5B0",  8, ""], # plane-departure
    ["ambulance",    "FontAwesome",    u"\uF0F9", 10, ""], # ambulance
    ["ammo",         "Govicons",       u"\uE910", 10, ""], # ammo
    ["anchor",       "FontAwesome",    u"\uF13D", 10, ""], # anchor
    ["antenna",      "FontAwesome",    u"\uF519",  8, ""], # broadcast-tower
    ["arrow",        "FontAwesome",    u"\uF124",  6, ""], # location-arrow
    ["balance",      "FontAwesome",    u"\uF24E", 10, ""], # balance-scale
    ["bank",         "FontAwesome",    u"\uF19C", 10, ""], # university
    ["bicycle",      "FontAwesome",    u"\uF206", 12, ""], # bicycle
    ["biohazard",    "FontAwesome",    u"\uF780", 14, ""], # biohazard
    ["binoculars",   "FontAwesome",    u"\uF1E5", 10, ""], # binoculars
    ["bomb",         "FontAwesome",    u"\uF1E2", 10, ""], # bomb
    ["bottle",       "FontAwesome",    u"\uF72F", 10, ""], # wine-bottle
    ["bridge",       "OpenSans",       u"\u224D", 20, ""], # Unicode EQUIVALENT TO
    ["building",     "FontAwesome",    u"\uF1AD", 10, ""], # building
    ["bullseye",     "FontAwesome",    u"\uF140", 10, ""], # bullseye
    ["bus",          "Noto Emoji",     u"🚎",     12, ""], # TROLLYBUS
    ["car",          "FontAwesome",    u"\uF5E4", 10, ""], # car-side
    ["campground",   "FontAwesome",    u"\uF6BB", 10, ""], # campground
    ["coffee",       "FontAwesome",    u"\uF0F4",  8, ""], # coffee
    ["construction", "Govicons",       u"\uE960", 10, ""], # construction
    ["crescent",     "FontAwesome",    u"\uF699", 13, ""], # star and crescent
    ["cross",        "FontAwesome",    u"\uF654", 13, ""], # cross
    ["crosshairs",   "FontAwesome",    u"\uF05B", 10, ""], # crosshairs
    ["dot",          "FontAwesome",    u"\uF111",  5, ""], # circle
    ["drone",        "Govicons",       u"\uE953", 12, ""], # drone
    ["explosion",    "FontAwesome",    u"\uF666", 10, ""], # haykal
    ["factory",      "FontAwesome",    u"\uF275", 10, ""], # industry
    ["fan",          "FontAwesome",    u"\uF069", 10, ""], # asterisk
    ["fighter",      "FontAwesome",    u"\uF0FB", 10, ""], # fighter-jet
    ["fire",         "FontAwesome",    u"\uF7E4", 10, ""], # fire-alt
    ["firetruck",    "Noto Emoji",     u"🚒",     12, ""], # FIRE ENGINE
    ["fish",         "FontAwesome",    u"\uF578", 10, ""], # fish
    ["flag",         "FontAwesome",    u"\uF024",  8, ""], # flag
    ["flash",        "FontAwesome",    u"\uF0E7", 10, ""], # bolt
    ["fuel",         "FontAwesome",    u"\uF52F", 10, ""], # gas-pump
    ["gavel",        "FontAwesome",    u"\uF0E3", 11, ""], # gavel
    ["gun",          "Govicons",       u"\uE915", 14, ""], # gun
    ["hammer",       "FontAwesome",    u"\uF6E3", 10, ""], # hammer
    ["helicopter",   "Govicons",       u"\uE918", 12, ""], # helicopter
    ["hiking",       "FontAwesome",    u"\uF6EC", 10, ""], # hiking
    ["hospital",     "FontAwesome",    u"\uF47E", 10, ""], # hospital-symbol
    ["hotel",        "FontAwesome",    u"\uF594",  8, ""], # hotel
    ["house",        "FontAwesome",    u"\uF015", 10, ""], # home
    ["info",         "FontAwesome",    u"\uF05A", 10, ""], # info-circle
    ["important",    "FontAwesome",    u"\uF06A", 10, ""], # exlamation-circle
    ["lightbulb",    "FontAwesome",    u"\uF0EB", 12, ""], # lightbulb
    ["mail",         "FontAwesome",    u"\uF0E0", 10, ""], # envelope
    ["missile",      "Govicons",       u"\uE954", 12, ""], # missile
    ["monument",     "FontAwesome",    u"\uF5A6",  8, ""], # motorcycle
    ["motorcycle",   "FontAwesome",    u"\uF21C", 12, ""], # motorcycle
    ["mountain",     "FontAwesome",    u"\uF6FC", 10, ""], # mountain
    ["museum",       "FontAwesome",    u"\uF66F", 10, ""], # landmark
    ["network",      "FontAwesome",    u"\uF6FF", 10, ""], # network-wired
    ["park",         "FontAwesome",    u"\uF1BB", 10, ""], # tree
    ["parking",      "FontAwesome",    u"\uF540", 10, ""], # parking
    ["people",       "FontAwesome",    u"\uF0C0", 10, ""], # users
    ["person",       "FontAwesome",    u"\uF007",  8, ""], # user
    ["phone",        "FontAwesome",    u"\uF095",  8, ""], # phone
    ["picture",      "FontAwesome",    u"\uF030", 10, ""], # camera
    ["pin",          "FontAwesome",    u"\uF041",  8, ""], # map-marker
    ["poison",       "FontAwesome",    u"\uF714", 12, ""], # skull-crossbones
    ["prescription", "FontAwesome",    u"\uF5B1", 10, ""], # prescription
    ["radar",        "Govicons",       u"\uE96B", 10, ""], # radar
    ["radio",        "FontAwesome",    u"\uF519",  8, ""], # broadcast-tower
    ["radioactive",  "FontAwesome",    u"\uF7B9", 14, ""], # radioactive
    ["restaurant",   "FontAwesome",    u"\uF2E7", 10, ""], # utensils
    ["road",         "FontAwesome",    u"\uF018", 10, ""], # road
    ["running",      "FontAwesome",    u"\uF70C", 10, ""], # running
    ["satellite",    "FontAwesome",    u"\uF7BF", 14, ""], # satellite
    ["satdish",      "FontAwesome",    u"\uF7C0", 12, ""], # satellite-dish
    ["school",       "FontAwesome",    u"\uF549", 10, ""], # school
    ["security",     "FontAwesome",    u"\uF3ED", 10, ""], # shield-alt
    ["ship",         "FontAwesome",    u"\uF21A", 10, ""], # ship
    ["shopping",     "FontAwesome",    u"\uF07A", 10, ""], # shopping-cart
    ["soccer",       "FontAwesome",    u"\uF1E3", 12, ""], # futbol
    ["solarpanel",   "FontAwesome",    u"\uF5BA", 10, ""], # solar-panel
    ["star",         "FontAwesome",    u"\uF005", 10, ""], # star
    ["starofdavid",  "FontAwesome",    u"\uF69A", 14, ""], # star of david
    ["submarine",    "Govicons",       u"\uE96A", 13, ""], # submarine
    ["sun",          "FontAwesome",    u"\uF185", 10, ""], # sun
    ["T",            "OpenSans",       "T",        8, ""], # T
    ["tank",         "Govicons",       u"\uE91B", 13, ""], # tank
    ["tools",        "FontAwesome",    u"\uF7D9", 12, ""], # tools
    ["train",        "FontAwesome",    u"\uF238", 10, ""], # train
    ["tractor",      "FontAwesome",    u"\uF722",  8, ""], # tractor
    ["truck",        "FontAwesome",    u"\uF0D1",  8, ""], # truck
    ["tower",        "OpenSans",       u"\u22CF", 14, ""], # CURLY LOGICAL AND
    ["tunnel",       "FontAwesome",    u"\uF557",  8, ""], # archway
    ["van",          "FontAwesome",    u"\uF5B6", 10, ""], # shuttle-van
    ["warning",      "FontAwesome",    u"\uF071", 10, ""], # exclamation-triangle
    ["waves",        "FontAwesome",    u"\uF773",  8, ""], # water
    ["wrench",       "FontAwesome",    u"\uF0AD", 10, ""], # wrench
    ["wifi",         "FontAwesome",    u"\uF1EB", 10, ""], # wifi
    ["WX",           "OpenSans",       "WX",       7, ""]
    ]

  colorlist = \
    [
    # name     icon    outline stroke  text    tBack   tOut
    ["cyan",   "#0FF", "#077", "#0CC", "#077", "#FFF", "#FFF"],
    ["green",  "#0F0", "#070", "#0C0", "#070", "#FFF", "#FFF"],
    ["yellow", "#FF0", "#770", "#CC0", "#770", "#FFF", "#FFF"],
    ["red",    "#F88", "#700", "#C00", "#F00", "#FFF", "#FFF"]
    ]

  def __init__(self, icon, name, latitude, longitude, altitude=None,
               aimpoint=None, ellipse=None, feature=None):
    """Feature object constructor.

    Keyword arguments:
    name -- Name of feature
    latitude -- Latitude of feature (degrees WGS84)
    longitude -- Longitude of feature (degrees WGS84)
    altitude -- Altitude of feature (m MSL)
    aimpoint -- Aimpoint of feature (draw a sector), [aimLat, aimLon, fovh]
    ellipse -- Ellipse of feature (draw an ellipse), [semiMaj, semiMin, az]
    feature -- GeoJSON object whose "properties" are deepcopied into the
               new feature object created by this constructor
    """

    self.haveAimpoint = False
    self.haveEllipse = False

    self.latitude = round(latitude, 6)
    self.longitude = round(longitude, 6)
    if altitude == None:
      self.altitude = None
    else:
      self.altitude = round(altitude, 0)

    if aimpoint != None and len(aimpoint) == 5:
      self.aimLatitude = round(aimpoint[0], 6)
      self.aimLongitude = round(aimpoint[1], 6)
      self.aimFOVh = round(aimpoint[2], 0)
      self.aimLatitudeDesired = round(aimpoint[3], 6)
      self.aimLongitudeDesired = round(aimpoint[4], 6)
      if self.aimLatitude >= -90 and self.aimLatitude <= 90 and \
         self.aimLongitude >= -180 and self.aimLongitude <= 180 and \
         self.aimFOVh > 0 and self.aimFOVh <= 360:
        self.haveAimpoint = True

    if ellipse != None and len(ellipse) == 3:
      self.semiMajor = round(ellipse[0], 0)
      self.semiMinor = round(ellipse[1], 0)
      self.azimuth = round(ellipse[2], 0)
      if self.semiMajor > 0 and self.semiMinor > 0 and \
         self.semiMinor <= self.semiMajor and \
         self.azimuth >= 0 and self.azimuth < 360:
        self.haveEllipse = True

    shapes = []
    if self.altitude == None:
      point = geojson.Point([self.longitude, self.latitude])
    else:
      point = geojson.Point([self.longitude, self.latitude, self.altitude])
    shapes.append(point)

    if self.haveAimpoint == True:
      polygon = self.__aimpoint(aimpoint[0], aimpoint[1], aimpoint[2])
      if self.aimLatitudeDesired >= -90 and self.aimLatitudeDesired <= 90 and \
         self.aimLongitudeDesired >= -180 and self.aimLongitudeDesired <= 180:
        line = geojson.LineString([[self.longitude, self.latitude], \
          [self.aimLongitudeDesired, self.aimLatitudeDesired]])
        shapes.append(line)
      shapes.append(polygon)

    if self.haveEllipse == True:
      polygon = self.__ellipse(self.latitude, self.longitude, \
                               ellipse[0], ellipse[1], ellipse[2])
      shapes.append(polygon)

    self.gj = geojson.Feature(geometry=geojson.GeometryCollection(shapes))

    if feature != None:
      self.gj["properties"] = deepcopy(feature["properties"])

    if "node" not in self.gj["properties"]:
      self.gj["properties"]["node"] = {}
    self.gj["properties"]["node"]["name"] = name
    self.gj["properties"]["node"]["uuid"] = str(uuid.uuid4()).upper()
    # Python2 GeoJSON outputs "id: null" for every feature
    # So set it to uuid
    # https://github.com/frewsxcv/python-geojson/pull/53
    self.gj["id"] = self.gj["properties"]["node"]["uuid"]

    if "position" not in self.gj["properties"]:
      self.gj["properties"]["position"] = {}
    self.gj["properties"]["position"]["latitude"] = self.latitude
    self.gj["properties"]["position"]["longitude"] = self.longitude
    if self.altitude != None:
      self.gj["properties"]["position"]["altitude"] = self.altitude

    if self.haveAimpoint == True:
      self.gj["properties"]["position"]["aimpoint"] = {}
      self.gj["properties"]["position"]["aimpoint"]["latitudeDesired"] = \
        self.aimLatitudeDesired
      self.gj["properties"]["position"]["aimpoint"]["longitudeDesired"] = \
        self.aimLongitudeDesired

    if self.haveEllipse == True:
      self.gj["properties"]["position"]["errorSemiMajor"] = self.semiMajor
      self.gj["properties"]["position"]["errorSemiMinor"] = self.semiMinor
      self.gj["properties"]["position"]["errorAzimuth"] = self.azimuth

    if self.haveEllipse == True:
      self.gj["properties"]["position"]["errorSemiMajor"] = self.semiMajor
      self.gj["properties"]["position"]["errorSemiMinor"] = self.semiMinor
      self.gj["properties"]["position"]["errorAzimuth"] = self.azimuth

    self.setDatetimeEpoch(int(time.time()))
    self.setDatetimeExpire(300)
    self.setDatetimeStale(120)

  def __getIconInfo(self, icon):
    for line in self.iconlist:
      if line[0] == icon:
        return(line)
    return(self.iconlist["dot"])

  def __getColorInfo(self, color):
    for line in self.colorlist:
      if line[0] == color:
        return(line)
    return(self.colorlist["cyan"])

  # draw an arc
  # points are returned as (longitude, latitude) for GeoJSON
  def __arc(self, latitude, longitude, radius, azStart, azStop, interval):
    if azStart == azStop:
      return([])
    if azStop < azStart:
      azStop += 360
    positions = []
    azimuth = azStart
    while azimuth <= azStop:
      geod = Geodesic.WGS84.Direct(latitude, longitude, azimuth, radius)
      positions.append((round(geod["lon2"], 5), round(geod["lat2"], 5)))
      azimuth += interval
    return(positions)

  # draw an ellipse
  # points are returned as (longitude, latitude) for GeoJSON
  def __ellipse(self, latitude, longitude, semiMajor, semiMinor, azimuth):
    if semiMajor < semiMinor or semiMajor <= 0 or semiMinor <= 0:
      return([])
    if azimuth < 0 or azimuth >= 360:
      return([])

    step = math.ceil((semiMinor/semiMajor)*10)
    points = []
    theta = 0
    while theta <= 360:
      tht = (theta % 360) * 0.017453 # convert to radians
      ctht = math.cos(tht)
      stht = math.sin(tht)
      radius = math.sqrt((semiMajor*semiMajor*semiMinor*semiMinor) /
                         (semiMinor*semiMinor*ctht*ctht +
                          semiMajor*semiMajor*stht*stht))
      geod = Geodesic.WGS84.Direct(latitude, longitude, \
                                   (theta+azimuth)%360, radius)
      points.append((round(geod["lon2"], 5), round(geod["lat2"], 5)))
      theta += step
    polygon = geojson.LineString(points)
    polygon.name = "ellipse"
    return(polygon)

  # draw a pie wedge (arc with radial)
  # points are returned as (longitude, latitude) for GeoJSON
  def __pie(self, latitude, longitude, radius, azStart, azStop, interval):
    positions = []
    positions.append((longitude, latitude))
    positions += self.__arc(latitude, longitude, radius, azStart, azStop, \
                            interval)
    positions.append((longitude, latitude))
    return(positions)

  # draw a pie wedge (arc with radial)
  # points are returned as (longitude, latitude) for GeoJSON
  def __sector(self, latitude, longitude, distance, azimuth, fovh):
    azHalf = fovh / 2
    azStart = azimuth - azHalf
    azStop = azimuth + azHalf
    return(self.__pie(latitude, longitude, distance, azStart, azStop, 5))

  # Draw an aimpoint
  def __aimpoint(self, aimLat, aimLon, aimFOVh):
    if aimLat < -90 or aimLat > 90 or aimLon < -180 or aimLon > 180 or \
       aimFOVh <= 0 or aimFOVh > 360:
      return
    geod = Geodesic.WGS84.Inverse(self.latitude, self.longitude, \
                                  aimLat, aimLon)
    dist = geod["s12"]
    azF = geod["azi1"]
    if dist < 10:
      dist = 10
    points = self.__sector(self.latitude, self.longitude, dist, azF, aimFOVh)
    polygon = geojson.LineString(points)
    polygon.name = "aimpoint"
    return(polygon)

  # Course Made Good
  def setCMG(self, cmg):
    if cmg == None:
      return
    if cmg >= 0 and cmg < 360:
      if not "attitude" in self.gj["properties"]:
        self.gj["properties"]["attitude"] = {}
      self.gj["properties"]["attitude"] = {}
      self.gj["properties"]["attitude"]["cmg"] = cmg

  def setDatetimeEpoch(self, epoch):
    if epoch == None:
      return
    if not "datetime" in self.gj["properties"]:
      self.gj["properties"]["datetime"] = {}
    self.gj["properties"]["datetime"]["epoch"] = epoch

  def setDatetimeExpire(self, expire):
    if expire == None:
      return
    if not "datetime" in self.gj["properties"]:
      self.gj["properties"]["datetime"] = {}
    self.gj["properties"]["datetime"]["timeExpire"] = expire

  def setDatetimeStale(self, stale):
    if stale == None:
      return
    if not "datetime" in self.gj["properties"]:
      self.gj["properties"]["datetime"] = {}
    self.gj["properties"]["datetime"]["timeStale"] = stale

  def setName(self, name):
    if name == None:
      return
    self.gj["properties"]["node"]["name"] = name

  # Speed Over Ground (km/h)
  def setSOG(self, sog):
    if sog == None:
      return
    if sog == None:
      return
    if sog >= 0:
      if not "speed" in self.gj["properties"]:
        self.gj["properties"]["speed"] = {}
      self.gj["properties"]["speed"]["sog"] = sog

  def setStyle(self, name, value):
    if value == None:
      return
    if not "style" in self.gj["properties"]:
      self.gj["properties"]["style"] = {}
    self.gj["properties"]["style"][name] = value

  def setStyleIconColor(self, color):
    self.setStyle("iconColor", color)

  def setStyleIconFontSize(self, size):
    if size <= 0:
      return
    self.setStyle("iconFontSize", size)

  def setStyleIconName(self, name):
    self.setStyle("iconName", name)

  def setStyleIconOpacity(self, opacity):
    if opacity == None:
      return
    if opacity >= 0 and opacity <= 1:
      if not "style" in self.gj["properties"]:
        self.gj["properties"]["style"] = {}
      self.gj["properties"]["style"]["iconOpacity"] = opacity

  def setStyleIconOutlineOpacity(self, opacity):
    if opacity == None:
      return
    if opacity >= 0 and opacity <= 1:
      if not "style" in self.gj["properties"]:
        self.gj["properties"]["style"] = {}
      self.gj["properties"]["style"]["iconOutlineOpacity"] = opacity

  def setStyleTextOpacity(self, opacity):
    if opacity == None:
      return
    if opacity >= 0 and opacity <= 1:
      if not "style" in self.gj["properties"]:
        self.gj["properties"]["style"] = {}
      self.gj["properties"]["style"]["textOpacity"] = opacity

  def setStyleTextOutlineOpacity(self, opacity):
    if opacity == None:
      return
    if opacity >= 0 and opacity <= 1:
      if not "style" in self.gj["properties"]:
        self.gj["properties"]["style"] = {}
      self.gj["properties"]["style"]["textOutlineOpacity"] = opacity

  def setStyleTextBackgroundOpacity(self, opacity):
    if opacity == None:
      return
    if opacity >= 0 and opacity <= 1:
      if not "style" in self.gj["properties"]:
        self.gj["properties"]["style"] = {}
      self.gj["properties"]["style"]["textBackgroundOpacity"] = opacity

  def setStyleTrackShow(self, show):
    if show == True:
      self.setStyle("trackShow", "true")
    if show == False:
      self.setStyle("trackShow", "false")

  def setStyleStrokeOpacity(self, opacity):
    if opacity == None:
      return
    if opacity >= 0 and opacity <= 1:
      if not "style" in self.gj["properties"]:
        self.gj["properties"]["style"] = {}
      self.gj["properties"]["style"]["strokeOpacity"] = opacity

  def setStyleZoom(self, zoom):
    if zoom == None:
      return
    if zoom >= 0 and zoom <= 100:
      if not "style" in self.gj["properties"]:
        self.gj["properties"]["style"] = {}
      self.gj["properties"]["style"]["zoom"] = zoom

  def setAdmin(self, admin):
    if admin == None:
      return
    if not "node" in self.gj["properties"]:
      self.gj["properties"]["node"] = {}
    self.gj["properties"]["node"]["admin"] = admin

  def setText(self, text):
    if text == None:
      return
    if not "node" in self.gj["properties"]:
      self.gj["properties"]["node"] = {}
    self.gj["properties"]["node"]["text"] = text

  def setUUID(self, uuid):
    if uuid == None:
      return
    self.gj["properties"]["node"]["uuid"] = uuid

  def initStyle(self, icon, color):
    if icon == None or color == None:
      return
    if "style" not in self.gj["properties"]:
      self.gj["properties"]["style"] = {}

    # icon
    iconInfo = self.__getIconInfo(icon)
    self.gj["properties"]["style"]["iconName"] = iconInfo[0]
#    self.gj["properties"]["style"]["iconFontFamily"] = iconInfo[1]
#    self.gj["properties"]["style"]["iconURL"] = iconInfo[2]
#    self.gj["properties"]["style"]["iconFontSize"] = iconInfo[3]

    # color
    colorInfo = self.__getColorInfo(color)
    self.setStyleIconColor(colorInfo[0])
#    self.gj["properties"]["style"]["icon"] = colorInfo[1]
#    self.gj["properties"]["style"]["iconOutline"] = colorInfo[2]
#    self.gj["properties"]["style"]["stroke"] = colorInfo[3]
#    self.gj["properties"]["style"]["text"] = colorInfo[4]
#    self.gj["properties"]["style"]["textBackground"] = colorInfo[5]
#    self.gj["properties"]["style"]["textOutline"] = colorInfo[6]

    # don't set textOpacity or textBackgroundOpacity because we typically
    # don't want to show names, it clutters the screen too much
#    self.gj["properties"]["style"]["fillOpacity"] = 0
#    self.gj["properties"]["style"]["strokeOpacity"] = 0.7
#    self.setStyleIconOpacity(0.8)
#    self.setStyleIconOutlineOpacity(0.8)

  def getName(self):
    return(self.gj["properties"]["node"]["name"])

  def getGeoJSON(self):
    return(json.dumps(self.gj))

  def getGeoJSONobj(self):
    return(self.gj)

  def getUUID(self):
    return(self.gj["properties"]["node"]["uuid"])

  def isExpired(self):
    if "properties" in self.gj and "datetime" in self.gj["properties"] and \
       "epoch" in self.gj["properties"]["datetime"] and \
       "timeExpire" in self.gj["properties"]["datetime"]:
      expire = self.gj["properties"]["datetime"]["epoch"] + \
               self.gj["properties"]["datetime"]["timeExpire"]
      if int(time.time()) < expire:
        return(False)
    return(True)

  def isInBBox(self, latN, lonW, latS, lonE):
    if latN >= -90 and latN <= 90 and lonW >= -180 and lonW <= -180 and \
       latS >= -90 and latS <= 90 and lonE >= -180 and lonW <= -180:
      if self.latitude >= latS and self.latitude <= latN and \
         self.longitude >= lonW and self.longitude <= lonE:
        return(True)
    return(False)

  def isStale(self):
    if "properties" in self.gj and "datetime" in self.gj["properties"] and \
       "epoch" in self.gj["properties"]["datetime"] and \
       "timeStale" in self.gj["properties"]["datetime"]:
      stale = self.gj["properties"]["datetime"]["epoch"] + \
              self.gj["properties"]["datetime"]["timeStale"]
      if stale < int(time.time()):
        return(False)
    return(True)

