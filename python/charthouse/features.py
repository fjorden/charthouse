# -*- coding: utf-8 -*-

# Charthouse
# https://gitlab.com/fjorden/charthouse
# Copyright 2018-2019 Fjorden

import sys
import json
import geojson
import time
import threading
import uuid

class features:

  def __init__(self):
    self.feats = {}
    self.mutex = threading.Lock()

  def __isExpired(self, feat):
    if "properties" in feat and "datetime" in feat["properties"] and \
       "epoch" in feat["properties"]["datetime"] and \
       "timeExpire" in feat["properties"]["datetime"]:
      expire = feat["properties"]["datetime"]["epoch"] + \
               feat["properties"]["datetime"]["timeExpire"]
      if int(time.time()) < expire:
        return(False)
    return(True)

  def __isInBBox(self, feat, latN, lonW, latS, lonE):
    if latN >= -90 and latN <= 90 and lonW >= -180 and lonW <= 180 and \
       latS >= -90 and latS <= 90 and lonE >= -180 and lonW <= 180:
      if feat["properties"]["position"]["latitude"] >= latS and \
         feat["properties"]["position"]["latitude"] <= latN and \
         feat["properties"]["position"]["longitude"] >= lonW and \
         feat["properties"]["position"]["longitude"] <= lonE:
        return(True)
    return(False)

  # https://stackoverflow.com/questions/5447494/best-way-to-remove-an-item-from-a-python-dictionary
  def ageOff(self):
    self.mutex.acquire()
    self.feats = { key: feat for key, feat in self.feats.items() \
                   if not self.__isExpired(feat) }
    self.mutex.release()

  # only keep features that are in this bounding box
  def siftBBox(self, latN, lonW, latS, lonE):
    self.mutex.acquire()
    self.feats = { key: feat for key, feat in self.feats.items() \
                   if not self.__isInBBox(feat, latN, lonW, latS, lonE) }
    self.mutex.release()

  # If feature in list, replace it.  If not, add it
  # this must be AS FAST AS POSSIBLE!
  # optional value to limit number of points in path
  def add(self, newfeature, pathSize=100):

    uuid = newfeature["properties"]["node"]["uuid"]

    self.mutex.acquire()

    # if feature already exists...
    if uuid in self.feats:
      # find points and linestrings in existing feature
      pointOld = None
      linestringOld = None
      geoms = self.feats[uuid]["geometry"]["geometries"]
      for geom in geoms:
        if "type" in geom:
          if geom["type"] == "Point":
            pointOld = geom
          elif geom["type"] == "LineString":
            # only get linestrings that are "track"
            # ignore "ellipse", "aimpoint", etc
            if "name" in geom and geom["name"] == "track":
              linestringOld = geom

      # find points and linestrings in new feature
      pointNew = None
      linestringNewIdx = None
      geoms = newfeature["geometry"]["geometries"]
      index = 0
      for geom in geoms:
        if geom["type"] == "Point":
          pointNew = geom
        elif geom["type"] == "LineString":
          # only get linestrings that are "track"
          # ignore "ellipse", "aimpoint", etc
          if "name" in geom and geom["name"] == "track":
            linestringNewIdx = index
        index += 1

      trackShow = True
      if "properties" in newfeature:
        if "style" in newfeature["properties"]:
          if "trackShow" in newfeature["properties"]["style"]:
            if newfeature["properties"]["style"]["trackShow"] == "false":
              trackShow = False

      if trackShow == True and pointOld != None and pointNew != None:
        # if linestring doesn't exist in old feature, create it
        # if linestring does exist, then append new point to it
        if linestringOld == None:
          linestringOld = geojson.LineString([pointNew["coordinates"], \
                                              pointOld["coordinates"]])
        else:
          linestringOld["coordinates"].append(pointNew["coordinates"])

        # remove any track points beyond the first X
        # so list doesn't grow without bounds
        del(linestringOld["coordinates"][pathSize:])

        # if linestring doesn't exist in new feature, add it
        # if linestring does exist in new feature, overwrite it
        if linestringNewIdx == None:
          linestringOld.name = "track"
          newfeature["geometry"]["geometries"].append(linestringOld)
        else:
          newfeature["geometry"]["geometries"][linestringNewIdx] = \
            linestringOld
          newfeature["geometry"]["geometries"][linestringNewIdx].name = "track"

    # add new feature to list
    self.feats[uuid] = newfeature

    self.mutex.release()

  def getCount(self):
    return(len(self.feats))

  def getFeatures(self):
    return(self.feats)

  def getGeoJSON(self):
    jsonstr = '{"type":"FeatureCollection","features":['
    self.mutex.acquire()
    if len(self.feats) > 0:
      for key, feat in self.feats.items():
        jsonstr += json.dumps(feat) + ","
      jsonstr = jsonstr[:-1] # strip last ","
    self.mutex.release()
    jsonstr += "]}"
    return(jsonstr)

