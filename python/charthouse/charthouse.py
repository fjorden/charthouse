# -*- coding: utf-8 -*-

# Charthouse
# https://gitlab.com/fjorden/charthouse
# Copyright 2017-2019 Fjorden

# Note that all these functions use (longitude, latitude)

import time
import re
import math
import sys
import json
import geojson
from copy import deepcopy

def dms2dd(degrees, minutes, seconds):
  dd = abs(float(degrees)) + float(minutes)/60 + float(seconds)/3600
  if degrees < 0:
    dd *= -1
  return dd

def dd2dms(deg):
  d = int(deg)
  md = abs(deg - d) * 60
  m = int(md)
  sd = (md - m) * 60
  return [d, m, sd]

# ^(\-?\d+(\.\d+)?),\s*(\-?\d+(\.\d+)?)$
# ^[-+]?([1-8]?\d(\.\d+)?|90(\.0+)?),\s*[-+]?(180(\.0+)?|((1[0-7]\d)|([1-9]?\d))(\.\d+)?)$

def coordParse(coord):
  lat = -255
  lon = -255
  match = re.match(r"([0-9][0-9])([0-5][0-9])([0-5][0-9])([N|S]) ([0-1][0-8][0-9])([0-5][0-9])([0-5][0-9])([E|W])", coord)
  if match != None:
    latDeg = float(match.group(1))
    latMin = float(match.group(2))
    latSec = float(match.group(3))
    latHem = str(match.group(4))
    lonDeg = float(match.group(5))
    lonMin = float(match.group(6))
    lonSec = float(match.group(7))
    lonHem = str(match.group(8))
    if latHem == "S":
      latDeg *= -1
    lat = dms2dd(latDeg, latMin, latSec)
    if lonHem == "W":
      lonDeg *= -1
    lon = dms2dd(lonDeg, lonMin, lonSec)
  match = re.match(r"(-?[0-9]?[0-9]\.[0-9]+) (-?[0-1]?[0-8]?[0-9]\.[0-9]+)",
                   coord)
  if match != None:
    lat = float(match.group(1))
    lon = float(match.group(2))
  if lat < -90 or lat > 90 or lon < -180 or lon > 180:
    return(-255, -255)
  return(lon, lat)

def featureIsExpired(feature):
  if "properties" in feature and "datetime" in feature["properties"] and \
     "epoch" in feature["properties"]["datetime"] and \
     "timeExpire" in feature["properties"]["datetime"]:
    expire = feature["properties"]["datetime"]["epoch"] + \
             feature["properties"]["datetime"]["timeExpire"]
    if expire < int(time.time()):
      return(False)
  return(True)

def featureIsStale(feature):
  if "properties" in feature and "datetime" in feature["properties"] and \
     "epoch" in feature["properties"]["datetime"] and \
     "timeStale" in feature["properties"]["datetime"]:
    stale = feature["properties"]["datetime"]["epoch"] + \
            feature["properties"]["datetime"]["timeStale"]
    if stale < int(time.time()):
      return(False)
  return(True)

