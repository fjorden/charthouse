# -*- coding: utf-8 -*-

# Charthouse
# https://gitlab.com/fjorden/charthouse
# Copyright 2017-2019 Fjorden

import sys

if sys.version_info >= (3, 0):
  from charthouse.charthouse import *
  from charthouse.feature import feature
  from charthouse.features import features
elif sys.version_info >= (2, 0):
  from charthouse import *
  from feature import feature
  from features import features

