
// Charthouse
// https://gitlab.com/fjorden/charthouse
// Copyright 2017-2019 Fjorden

"use strict";

function xhttpGet(url, cachebuster, withCreds, callback, params)
  {
  var xhttp;
  var _url;

  if (typeof XMLHttpRequest != "undefined")
    {
    xhttp = new XMLHttpRequest();
    }
  else
    {
    var index;
    var versions = ["MSXML2.XmlHttp.5.0",
                    "MSXML2.XmlHttp.4.0",
                    "MSXML2.XmlHttp.3.0",
                    "MSXML2.XmlHttp.2.0",
                    "Microsoft.XmlHttp"]
    var len;

    len = versions.length;

    for (index=0; index < len; ++index)
      {
      try
        {
        xhttp = new ActiveXObject(versions[index]);
        break;
        }
      catch(err)
        {
        };
      };
    };

  if (withCreds)
    xhttp.withCredentials = true;

  if (callback != undefined)
    {
    xhttp.onreadystatechange = function()
      {
      callback(xhttp, params);
      };
    };

  // add cache buster
  _url = url;
  if (cachebuster === true)
    {
    if (url.indexOf('?') > -1)
      _url += '&';
    else
      _url += '?';
    _url += "cb=" + Math.round(new Date().getTime()/1000);
    };

  xhttp.open("GET", _url, true);
  xhttp.send();
  }

