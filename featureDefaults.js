
// Charthouse
// https://gitlab.com/fjorden/charthouse
// Copyright 2017-2019 Fjorden

"use strict";

var fontAwesome = "/charthouse/support/fontawesome/svgs/";

function getFeatureIcon(name)
  {
  var index;

  for (index=0; index < iconlist.length; ++index)
    {
    if (iconlist[index][0] === name)
      return(iconlist[index]);
    };
  }

function getFeatureColor(color)
  {
  var index;

  for (index=0; index < colorlist.length; ++index)
    {
    if (colorlist[index][0] === color)
      return(colorlist[index]);
    };
  }

function uuid()
  {
  return('xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g,
         function(c)
           {
           var r = Math.random()*16|0, v = c == 'x' ? r : (r&0x3|0x8);
           return(v.toString(16));
           }
        ));
  }

function genRGBA(color, opacity)
  {
  var rgba=[]; // [red, green, blue, alpha]
  }

function setFeatureDefaults(feature, icon, color)
  {
/*
  var defaults =
    {
    "datetime":
      {
      "epoch": (new Date).getTime() / 1000,
      "timeExpire": 300,
      "timeStale": 120
      },
    "node":
      {
      "name": "",
      "uuid": uuid(),
      },
    "style":
      {
      "fill": "#FFF",
      "fillOpacity": 0.5,
      "fontFamily": "Helvetica",
      "fontSize": 8,
      "icon": "#FFF",
      "iconFontFamily": "FontAwesome",
      "iconOpacity": 0.75,
      "iconOutline": "#000"
      "rotation": 0,
      "stroke": "#FFF",
      "strokeOpacity": 0.5,
      "strokeWidth": 2,
      "text": "#FFF",
      "textBackground": "#FFF",
      "textBackgroundOpacity": 0,
      "textOpacity": 0,
      "textOutline": "#000",
      "textOutlineOpacity": 0,
      "zoom": 20
      },
    };
*/

  if (feature === undefined)
    feature = {};

  feature.type = "Feature";

  if (feature.properties == undefined)
    feature.properties = {};

  if (feature.properties.attitude === undefined)
    feature.properties.attitude = {};
  if (feature.properties.attitude.heading === undefined)
    feature.properties.attitude.heading = undefined;

  if (feature.properties.datetime === undefined)
    feature.properties.datetime = {};
  if (feature.properties.datetime.epoch === undefined)
    feature.properties.datetime.epoch = (new Date).getTime() / 1000;
  if (feature.properties.datetime.timeExpire === undefined)
    feature.properties.datetime.timeExpire = 300;
  if (feature.properties.datetime.timeStale === undefined)
    feature.properties.datetime.timeStale = 120;

  if (feature.properties.node === undefined)
    feature.properties.node = {};
  if (feature.properties.node.uuid === undefined)
    feature.properties.node.uuid = uuid();
  if (feature.properties.node.name === undefined)
    feature.properties.node.name = "";

  if (feature.properties.style === undefined)
    feature.properties.style = {};
  if (feature.properties.style.fill === undefined)
    feature.properties.style.fill = "#FFF";
  if (feature.properties.style.fillOpacity === undefined)
    feature.properties.style.fillOpacity = 0.5;
  if (feature.properties.style.fontFamily === undefined)
    feature.properties.style.fontFamily = "OpenSans";
  if (feature.properties.style.fontSize === undefined)
    feature.properties.style.fontSize = 8;
  if (feature.properties.style.icon === undefined)
    feature.properties.style.icon = "#FFF";
  if (feature.properties.style.iconColor === undefined)
    feature.properties.style.iconColor = color;
  if (feature.properties.style.iconFontFamily === undefined)
    feature.properties.style.iconFontFamily = "FontAwesome";
  if (feature.properties.style.iconFontSize === undefined)
    feature.properties.style.iconFontSize = 8;
  if (feature.properties.style.iconName === undefined)
    feature.properties.style.iconName = icon;
  if (feature.properties.style.iconOpacity === undefined)
    feature.properties.style.iconOpacity = 0.75;
  if (feature.properties.style.iconOutline === undefined)
    feature.properties.style.iconOutline = "#FFF";
  if (feature.properties.style.iconOutlineOpacity === undefined)
    feature.properties.style.iconOutlineOpacity = 0.75;

  // we don't want a default for iconURL because the given feature
  // might be just a line or polygon without an icon

  if (feature.properties.style.rotation === undefined)
    feature.properties.style.rotation = 0;
  if (feature.properties.style.stroke === undefined)
    feature.properties.style.stroke = "#FFF";
  if (feature.properties.style.strokeOpacity === undefined)
    feature.properties.style.strokeOpacity = 0.4;
  if (feature.properties.style.strokeWidth === undefined)
    feature.properties.style.strokeWidth = 2.0;
  if (feature.properties.style.text === undefined)
    feature.properties.style.text = "#FFF";
  if (feature.properties.style.textBackground === undefined)
    feature.properties.style.textBackground = "#FFF";
  if (feature.properties.style.textBackgroundOpacity === undefined)
    feature.properties.style.textBackgroundOpacity = 0;
  if (feature.properties.style.textOpacity === undefined)
    feature.properties.style.textOpacity = 0;
  if (feature.properties.style.textOutline === undefined)
    feature.properties.style.textOutline = "#FFF";
  if (feature.properties.style.textOutlineOpacity === undefined)
    feature.properties.style.textOutlineOpacity = 0;
  if (feature.properties.style.zoom === undefined)
    feature.properties.style.zoom = 20;

  {
  var heading;
  var cmg;

  heading = feature.properties.attitude.heading;
  cmg = feature.properties.attitude.cmg;
  if (heading >= 0 && heading < 360)
    feature.properties.style.rotation = heading;
  else if (cmg >= 0 && cmg < 360)
    feature.properties.style.rotation = cmg;
  };

  var info;

  info = getFeatureIcon(feature.properties.style.iconName);
  if (info !== undefined)
    {
    if (info[4].length > 0)
      {
      feature.properties.style.iconFontFamily = info[1];
      feature.properties.style.iconFontSize = info[3];
      feature.properties.style.iconURL = fontAwesome + info[4];
      }
    else
      {
      feature.properties.style.iconFontFamily = info[1];
      feature.properties.style.iconURL = info[2];
      feature.properties.style.iconFontSize = info[3];
      };
    };

  info = getFeatureColor(feature.properties.style.iconColor);
  if (info !== undefined)
    {
    feature.properties.style.icon = info[1];
    feature.properties.style.iconOutline = info[2];
    feature.properties.style.stroke = info[3];
    feature.properties.style.text = info[4];
    feature.properties.style.textBackground = info[5];
    feature.properties.style.textOutline = info[6];
    };

  // XXX this should probably go in Python iconinfo list
  // some icons aren't pointing straight up, so we must adjust them
  // some icons should be bigger or smaller by default, so set iconFontSize
  if (feature.properties.style.iconURL !== undefined)
    {
    if (feature.properties.style.iconFontFamily == "FontAwesome")
      {
      if (feature.properties.style.iconURL == "\uF0FB" || // fighter-jet
          feature.properties.style.iconURL == "\uF072")   // plane
        feature.properties.style.rotation -= 90;
      if (feature.properties.style.iconURL == "\uF124" ||  // location-arrow
          feature.properties.style.iconURL == "\uF7BF" ||  // satellite
          feature.properties.style.iconURL == "\uF7C0")    // satellite-dish
        feature.properties.style.rotation -= 45;
      }
    else if (feature.properties.style.iconFontFamily === "Govicons")
      {
      if (feature.properties.style.iconURL == "\uE953") // drone
        feature.properties.style.rotation -= 45;
      if (feature.properties.style.iconURL == "\uE918") // helicopter
        feature.properties.style.rotation -= 45;
      if (feature.properties.style.iconURL == "\uE91A") // jet
        feature.properties.style.rotation -= 45;
      if (feature.properties.style.iconURL == "\uE954") // missile
        feature.properties.style.rotation -= 45;
      else if (feature.properties.style.iconURL == "\uE955") // satellite
        feature.properties.style.rotation -= 45;
      };

    feature.properties.style.rotation =
      feature.properties.style.rotation % 360;
    };

  return(feature);
  }

