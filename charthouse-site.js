
// Charthouse
// https://gitlab.com/fjorden/charthouse
// Copyright 2017-2019 Fjorden

// Site-specific configuration

"use strict";

// Base URL for Charthouse site installation
//var baseURL = "/charthouse";
var baseURL = "./";

// About file
var aboutURL = baseURL + "/about.html";

// Helpf file
var helpURL = baseURL + "/help.html";

// Tile that appears in browser title bar
var siteTitle = "Charthouse";

// Title that appears in map HUD (explore icon)
var siteHUDtitle = "<i class='fas fa-compass fa-fw'></i> Charthouse";

// MQTT server and port
//var mqttWS = "ws://localhost/mqtt";
//var mqttWS = "wss://localhost:443/ws";
var mqttWS;
// if topic contains "#/error", "#/notice", or "#/warning",
// then a popup is displayed
var mqttTopics =
  [
  "charthouse/error",
  "charthouse/notice",
  "charthouse/warning",
  "charthouse/geojson"
  ];

// this is a sample init function that gets called at the very end
// this is a "initFunction" for testing purposes only
// see reference to dataFunctionTest below
// this would ideally be placed in a javascript file that gets
// loaded before charthouse-site.js in index.html
function _initFunction()
  {
  console.log("Site init function");
  }

var initFunction=_initFunction();

// this is a "dataFunction" for testing purposes only
// see reference to dataFunctionTest below
// this would ideally be placed in a javascript file that gets
// loaded before charthouse-site.js in index.html
function dataFunctionTest(obj)
  {
  //console.log("dataFunctionTest:", obj);
  var feature={};
  var features=[];

  feature = setFeatureDefaults(feature, "tower", "cyan");
  feature.geometry = {};
  feature.geometry.type = "GeometryCollection";
  feature.geometry.geometries = [];
  feature.geometry.geometries[0] = {};
  feature.geometry.geometries[0].type = "Point";
  feature.geometry.geometries[0].coordinates = [];
  feature.geometry.geometries[0].coordinates[0] = 2.2945;
  feature.geometry.geometries[0].coordinates[1] = 48.858222;
  feature.properties.node.uuid = "la Tour Eiffel";
  feature.properties.node.name = "la Tour Eiffel";
  feature.properties.style.textOpacity = 1;
  feature.properties.style.textOutlineOpacity = 0;
  feature.properties.style.textBackgroundOpacity = 1;
  feature.properties.layerGroup = "Data";
  feature.properties.layerName = "Data function";
  feature.properties.layerUUID = getLayerByName("Data function").uuid;
  feature.properties.url = "dataFunctionTest()";

  features.push(feature);

  workerCBcall(features, "dataFunctionTest()",
    getLayerByName("Data function").uuid, "Data function", "Data");
  }

// Data source layers
var siteLayersData = 
  [
/*
  {
  name: "Charthouse example GeoJSON Heatmap",
  group: "Data",
  OLtype: "Heatmap",
  url: "example.json",
  interval: 30,
  visible: 0
  },
*/
/*
  {
  name: "Near-real-time",
  group: "Data",
  OLtype: "Vector",
  url: "/geojson/NRT/NRT.json",
  interval: 1
  },
*/
  {
  name: "Example GeoJSON",
  group: "Data",
  OLtype: "Vector",
  url: "/geojson/example.json",
  interval: 1
  },
  {
  name: "MQTT",
  group: "Data",
  OLtype: "Vector",
  interval: 0
  },
  {
  name: "Geolocation",
  group: "Data",
  OLtype: "Vector",
  dataFunction: function(){getLocation();},
  interval: 5
  }
/*
  {
  name: "Data function",
  group: "Data",
  OLtype: "Vector",
  dataFunction: dataFunctionTest,
  interval: 30
  }
*/
/*
  {
  name: "utils/test GeoJSON",
  group: "Data",
  OLtype: "Vector",
  url: "utils/test.json",
  interval: 30
  }
*/
  ];

// Base layers
var siteLayersBase =
  [
/*
  {
  name: "ahocevar geospatial",
  group: "Maps",
  OLtype: "TileWMS",
  url: "https://ahocevar.com/geoserver/wms",
  params: {"LAYERS": "ne:ne", "TILED": true},
  visible: 0,
  input: "slider"
  },
*/
/*
  {
  name: "USGS Shaded Relief",
  url: "https://basemap.nationalmap.gov/arcgis/services/USGSShadedReliefOnly/MapServer/WmsServer",
  params: {"LAYERS": "0", "TILED": true},
  group: "Maps",
  OLtype: "TileWMS",
  visible: 0,
  input: "slider"
  },
*/
/*
  {
  name: "GEBCO 08 Hillshade",
  url:
"https://maps.ngdc.noaa.gov/arcgis/services/gebco08_hillshade/MapServer/WmsServer",
  params: {"LAYERS": "GEBCO_08 Hillshade", "TILED": true},
  group: "Maps",
  OLtype: "TileWMS",
  visible: 0,
  input: "slider"
  },
*/
  {
  name: "USGS Imagery",
  group: "Maps",
  OLtype: "TileWMS",
  url:
"https://basemap.nationalmap.gov/arcgis/services/USGSImageryOnly/MapServer/WMSServer",
  params: {"LAYERS": "0", "TILED": true},
  visible: 0,
  input: "slider"
  },
  {
  name: "ESRI Imagery World 2D",
  group: "Maps",
  OLtype: "XYZ",
  url: "https://services.arcgisonline.com/arcgis/rest/services/ESRI_Imagery_World_2D/MapServer/tile/{z}/{y}/{x}",
  visible: 0,
  input: "slider"
  },
  {
  name: "Chartbundle Aviation Charts",
  group: "Maps",
  OLtype: "TileWMS",
  url: "https://wms.chartbundle.com/wms/",
  params: {"LAYERS": "sec", "TILED": true},
  visible: 0,
  input: "slider"
  },
  {
  name: "Open Street Map",
  group: "Maps",
  OLtype: "OSM",
  visible: 1,
  input: "slider"
  },
  /* https://idpgis.ncep.noaa.gov/arcgis/rest/services */
  {
  name: "NOAA Radar 1x1 km Base Reflectivity",
  group: "Maps",
  OLtype: "TileWMS",
  url:
"https://idpgis.ncep.noaa.gov/arcgis/services/NWS_Observations/radar_base_reflectivity/MapServer/WMSServer",
  params: {"LAYERS": "1", "TILED": true},
  visible: 0.7,
  input: "slider"
  },
  {
  name: "NOAA Global Precipitation Analysis",
  group: "Maps",
  OLtype: "TileWMS",
  url:
"https://idpgis.ncep.noaa.gov/arcgis/services/NWS_Climate_Outlooks/cpc_cmorph_dly_025deg/MapServer/WMSServer",
  params: {"LAYERS": "1", "TILED": true},
  visible: 0,
  input: "slider"
  },
  {
  name: "NOAA Weekly Global Sea Surface Temperature",
  group: "Maps",
  OLtype: "TileWMS",
  url:
"https://idpgis.ncep.noaa.gov/arcgis/services/NWS_Climate_Outlooks/cpc_wkly_sst/MapServer/WMSServer",
  params: {"LAYERS": "1", "TILED": true},
  visible: 0,
  input: "slider"
  },
  {
  name: "NOAA National Forecast Chart",
  group: "Maps",
  OLtype: "TileWMS",
  url:
"https://idpgis.ncep.noaa.gov/arcgis/services/NWS_Forecasts_Guidance_Warnings/natl_fcst_wx_chart/MapServer/WMSServer",
  params: {"LAYERS": "1", "TILED": true},
  visible: 0,
  input: "slider"
  },
  {
  name: "NOAA Forecast",
  group: "Maps",
  OLtype: "TileWMS",
  url:
"https://idpgis.ncep.noaa.gov/arcgis/services/NWS_Forecasts_Guidance_Warnings/natl_fcst_wx_chart/MapServer/WMSServer",
  params: {"LAYERS": "1", "TILED": true},
  visible: 0,
  input: "slider"
  },
/*
  {
  name: "Open Sea Map",
  group: "Overlays",
  OLtype: "XYZ",
  url: "http://t1.openseamap.org/seamark/{z}/{x}/{y}.png"
  },
*/
/*
  {
  name: "mapbox.com Elevation",
  group: "Overlays",
  OLtype: "XYZ",
  url: "https://{a-d}.tiles.mapbox.com/v3/aj.sf-dem/{z}/{x}/{y}.png",
  input: "slider"
  },
*/
  {
  name: "1843 Monaldini Map, Roma, IT",
  group: "Overlays",
  OLtype: "Image",
  url: "maps/1843_Monaldini_Case_Map_of_Rome,_Italy_-_Geographicus_-_Rome-monaldini-1843.jpeg",
  extent: [12.4423, 41.8719, 12.5225, 41.9143],
  input: "slider",
  opacity: 0.6
  }
  ];

