
// Charthouse
// https://gitlab.com/fjorden/charthouse
// Copyright 2017-2019 Fjorden

"use strict";

function dd2dms(dd)
  {
  var dms;
  var minf;

  dms.degrees = Math.floor(deg);
  minfs = Math.abs(dms.degrees - deg) * 60;
  dms.minutes = Math.floor(minfs);
  dms.seconds = (minfs - dms.minutes) * 60;

  return(dms);
  }

function dms2dd(deg, min, sec)
  {
  var dd;

  dd = Math.abs(Math.floor(degrees)) + (minutes / 60) + (seconds/3600);
  if (degrees < 0)
    dd *= -1;
  }

function layerClear(layer)
  {
  // if we don't remove selected features, it gets left on canvas
  interactionSelect.getFeatures().clear();
  if (layer !== undefined)
    layer.getSource().clear();
  }

function kmh2kn(kmh)
  {
  return(kmh * 0.539957);
  }

function kmh2mph(kmh)
  {
  return(kmh * 0.621371417857264);
  }

function m2nm(meters)
  {
  return(meters/1852);
  }

function m2mi(meters)
  {
  return(meters*0.00062137119);
  };

function m2ft(meters)
  {
  return(meters*3.2808399);
  }

function roundf(value, precision)
  {
  var prec;

  prec = Math.pow(10, precision);
  return(Math.round(value * prec) / prec);
  }

function centerOfExtent(extent)
  {
  var x;
  var y;

  x = extent[0] + (extent[2] - extent[0]) / 2;
  y = extent[1] + (extent[3] - extent[1]) / 2;

  return([x, y]);
  }

function timeString(dt, hideSeconds)
  {
  var hours;
  var minutes;
  var seconds;
  var str;

  hours = dt.getUTCHours();
  if (hours < 10)
    hours = "0" + hours;
  minutes = dt.getUTCMinutes();
  if (minutes < 10)
    minutes = "0" + minutes;
  seconds = dt.getUTCSeconds();
  if (seconds < 10)
    seconds = "0" + seconds;

  str = hours + ":" + minutes;

  if (hideSeconds != true)
    str += ":" + seconds;

  str += "Z";

  return(str);
  }

function dateString(dt)
  {
  if (dt === undefined)
    return("");

  var str;

  switch (dt.getUTCMonth())
    {
    case 0:
      str = "Jan ";
      break;
    case 1:
      str = "Feb ";
      break;
    case 2:
      str = "Mar ";
      break;
    case 3:
      str = "Apr ";
      break;
    case 4:
      str = "May ";
      break;
    case 5:
      str = "Jun ";
      break;
    case 6:
      str = "Jul ";
      break;
    case 7:
      str = "Aug ";
      break;
    case 8:
      str = "Sep ";
      break;
    case 9:
      str = "Oct ";
      break;
    case 10:
      str = "Nov ";
      break;
    case 11:
      str = "Dec ";
      break;
    };

  str += dt.getUTCDate() + " " + dt.getUTCFullYear();

  return(str);
  }

function datetimeString(dt)
  {
  var str;

  return(dateString(dt) + " " + timeString(dt));
  }

function latlonString(lat, lon)
  {
  if (lat >= -90 && lat <= 90 && lon >= -180 && lon <= 180)
    {
    if (settings["format"]["position"] == "degmin")
      return(Dms.toLat(lat, "dm") + " " + Dms.toLon(lon, "dm"));
    if (settings["format"]["position"] == "dms")
      return(Dms.toLat(lat, "dms") + " " + Dms.toLon(lon, "dms"));
    if (settings["format"]["position"] == "utm")
      {
      var latlon;

      latlon = new LatLon(lat, lon);
      return(latlon.toUtm().toString());
      };
    if (settings["format"]["position"] == "mgrs")
      {
      var latlon;

      latlon = new LatLon(lat, lon);
      return(latlon.toUtm().toMgrs().toString());
      };

    return(Dms.toLat(lat, "d") + " " + Dms.toLon(lon, "d"));
    };

  return("");
  }

function distanceString(meters)
  {
  if (meters < 0)
    return("");

  if (settings["units"]["distance"] == "nautical")
    {
    var nm;

    nm = m2nm(meters);
    if (nm < 1)
      return(roundf(m2ft(meters), 0) + "ft");
    if (nm < 10)
      return(roundf(nm, 1) + "nm");

    return(roundf(nm, 0) + "nm");
    };

  if (settings["units"]["distance"] == "uscustomary")
    {
    var mi;

    mi = m2mi(meters);
    if (mi < 1)
      return(roundf(m2ft(meters), 0) + "ft");
    if (nm < 10)
      return(roundf(mi, 1) + "mi");

    return(roundf(mi, 0) + "mi");
    };

  if (meters < 1000)
    {
    return(meters + 'm')
    };
  if (meters < 10000)
    {
    return(roundf(meters/1000, 1) + "km")
    };

  return(roundf(meters/1000, 0) + "km")
  }

function azimuthString(degrees)
  {
  var str="";

  if (degrees >= 0 && degrees < 360)
    {
    var deg;

    deg = roundf(degrees, 0);
    if (deg == 360)
      deg = 0;

    if (deg < 10)
      str += "00";
    else if (deg < 100)
      str += "0";

    str += deg + "°T";
    };

  return(str);
  }

function altitudeString(meters)
  {
  if (meters == undefined || isNaN(meters))
    return("");

  // XXX error check meters
  if (settings["units"]["altitude"] == "feet")
    return(roundf(m2ft(meters), 0) + "'");

  return(roundf(meters, 0) + "m");
  }

function speedString(kmh)
  {
  if (settings["units"]["distance"] == "nautical")
    {
    return(roundf(kmh2kn(kmh), 0) + "kn");
    };

  if (settings["units"]["distance"] == "uscustomary")
    {
    return(roundf(kmh2mph(kmh), 0) + "mph");
    };

  // XXX if less than one km/h, then output m/s
  return(roundf(kmh, 0) + "km/h");
  }

// RFC4122 version 4 UUID
// https://stackoverflow.com/questions/105034/create-guid-uuid-in-javascript
function uuid()
  {
  return('xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g,
         function(c)
           {
           var r = Math.random()*16|0, v = c == 'x' ? r : (r&0x3|0x8);
           return(v.toString(16));
           }
        ));
  }

function getLayerByName(name)
  {
  var index;

  for (index=0; index < layersData.length; ++index)
    {
    if (layersData[index].name == name)
      return(layersData[index]);
    }
  for (index=0; index < layersBase.length; ++index)
    {
    if (layersBase[index].name == name)
      return(layersBase[index]);
    }

  return(undefined);
  }

function getLayerByUUID(uuid)
  {
  var layers;
  var index;

  for (index=0; index < layersData.length; ++index)
    {
    if (layersData[index].uuid == uuid)
      return(layersData[index]);
    }
  for (index=0; index < layersBase.length; ++index)
    {
    if (layersBase[index].uuid == uuid)
      return(layersBase[index]);
    }

  return(undefined);
  }

function updateSunMoon()
  {
  var dt;
  var lst;
  var hour;
  var minutes;
  var str;
  var sun;
  var obj;
  var moon;

  document.getElementById("obsLocation").innerHTML =
    latlonString(settings["center"]["latitude"],
    settings["center"]["longitude"]);

  dt = new Date();
  lst = dt.getLST(settings["center"]["longitude"]);
  hour = Math.floor(lst);
  minutes = Math.round((lst - hour) * 60);

  str = "";
  if (hour < 10)
    str += "0";
  str += hour;
  str += ":";
  if (minutes < 10)
    str += "0";
  str += minutes;

  document.getElementById("sunLocalSolarTime").innerHTML = str;

  obj = SunCalc.getTimes(dt, settings["center"]["latitude"],
                         settings["center"]["longitude"]);

  // XXX getTime() returns NaN if always above or below the horizon...
  // how can we figure out if it's always up or always down?
  if (isNaN(obj.sunrise.getTime()))
    {
    document.getElementById("sunRise").innerHTML = "";
    }
  else
    {
    document.getElementById("sunRise").innerHTML =
      timeString(obj.sunrise, true);
    };
  if (isNaN(obj.sunset.getTime()))
    {
    document.getElementById("sunSet").innerHTML = "";
    }
  else
    {
    document.getElementById("sunSet").innerHTML =
      timeString(obj.sunset, true);
    };

  moon = new Moon();

  document.getElementById("moonIllum").innerHTML = moon.illumination() + "%";
  document.getElementById("moonPhase").innerHTML = moon.phaseIcon();
  document.getElementById("moonNextNew").innerHTML =
    dateString(moon.nextNew());
  document.getElementById("moonNextFull").innerHTML =
    dateString(moon.nextFull());

  obj = SunCalc.getMoonTimes(dt, settings["center"]["latitude"],
                             settings["center"]["longitude"]);

  if (obj.alwaysUp === true)
    {
    document.getElementById("moonRise").innerHTML = "Above horizon all day";
    document.getElementById("moonSet").innerHTML = "";
    }
  else if (obj.alwaysDown === true)
    {
    document.getElementById("moonRise").innerHTML = "Below horizon all day";
    document.getElementById("moonSet").innerHTML = "";
    }
  else
    {
    // XXX why are obj.rise and obj.set sometimes undefined?
    if (obj.rise === undefined || isNaN(obj.rise.getTime()))
      {
      document.getElementById("moonRise").innerHTML = "";
      }
    else
      {
      document.getElementById("moonRise").innerHTML =
        timeString(obj.rise, true);
      };

    if (obj.set === undefined || isNaN(obj.set.getTime()))
      {
      document.getElementById("moonSet").innerHTML = "";
      }
    else
      {
      document.getElementById("moonSet").innerHTML =
        timeString(obj.set, true);
      };
    };

  }

function elementPosition(element, coord)
  {
  var lonlat;
  var xoffset;
  var yoffset;
  var positioning;

  lonlat = ol.proj.transform(coord, "EPSG:3857", "EPSG:4326");

  // chage x/y offset based on quadrant of coord
  if (lonlat[1] < settings["center"]["latitude"])
    {
    yoffset = -10;
    positioning = "bottom-";
    }
  else
    {
    yoffset = 10;
    positioning = "top-";
    };
  if (lonlat[0] > settings["center"]["longitude"])
    {
    xoffset = -10;
    positioning += "right";
    }
  else
    {
    xoffset = 10;
    positioning += "left";
    };

  element.setOffset([xoffset, yoffset]);
  element.setPositioning(positioning);
  element.setPosition(coord);
  }

function mapCenter(latitude, longitude)
  {
/*
  map.getView().setCenter(ol.proj.transform([longitude, latitude],
    "EPSG:4326", "EPSG:3857"));
*/
  map.getView().animate({center: ol.proj.transform([longitude, latitude],
    "EPSG:4326", "EPSG:3857"), duration: 1000});
  }

function mapRotate(azimuth)
  {
  var _az;

  _az = (360 - azimuth) % 360;
  _az = _az * Math.PI / 180; // degrees to radians

  map.getView().animate(
    {
    rotation: _az
    });
  }

function popupOpen(id)
  {
  var elems;
  var index;
  var elem;

  elems = document.getElementsByClassName("popup");
  for (index=0; index < elems.length; ++index)
    elemHide(elems[index]); // XXX use popupClose()

  elem = document.getElementById(id);
  if (elem != undefined)
    elemShow(elem);
  }

function popupClose(id)
  {
  var elem;

  // XXX I don't like checking for + "Header" need better way
  elem = document.getElementById(id + "Header");
  if (elem != undefined)
    elemFadeOut(elem);

  elem = document.getElementById(id);
  if (elem != undefined)
    elemFadeOut(elem);
  }

function elemShow(elem)
  {
  elem.style.opacity = 1;
  elem.style.display = "block";
  }

function elemHide(elem)
  {
  elem.style.opacity = 0;
  elem.style.display = "none";
  }

function elemShowById(id)
  {
  var elem;

  elem = document.getElementById(id);
  if (elem !== undefined)
    elemShow(elem);
  }

function elemHideById(id)
  {
  var elem;

  elem = document.getElementById(id);
  if (elem !== undefined)
    elemHide(elem);
  }

// https://stackoverflow.com/questions/23244338/pure-javascript-fade-in-function
function elemFadeToggle(elem, fast)
  {
  if (elem.style.opacity > 0)
    elemFadeOut(elem, fast);
  else
    elemFadeIn(elem, fast);
  };

function elemFadeIn(elem, fast)
  {
/*
  elem.classList.add("fadeIn");
  setTimeout(function()
    {
    elem.classList.remove("fadeIn");
    elemShow(elem);
    }, 1000);
*/
  }

function fadeIn(id, fast)
  {
  var elem;

  elem = document.getElementById(id);
  if (elem != undefined)
    elemFadeIn(elem, fast);
  }

function fadeOut(id, fast)
  {
  var elem;

  elem = document.getElementById(id);
  if (elem != undefined)
    elemFadeOut(elem, fast);
  }

function elemFadeOut(elem, fast)
  {
  elem.classList.add("fadeOut");
  setTimeout(function()
    {
    elemHide(elem);
    elem.classList.remove("fadeOut");
    }, 1000);
  }

// https://stackoverflow.com/questions/4833651/javascript-array-sort-and-unique
function sort_unique(arr) {
    if (arr.length === 0) return arr;
    arr = arr.sort(function (a, b) { return a*1 - b*1; });
    var ret = [arr[0]];
    for (var i = 1; i < arr.length; i++) { // start loop at 1 as element 0 can never be a duplicate
        if (arr[i-1] !== arr[i]) {
            ret.push(arr[i]);
        }
    }
    return ret;
}

/*
function drawCircle(latitude, longitude, radius, azimuth)
  {
  function animate(event)
    {
    var style;
    var geom;

    geom = new ol.geom.Point(ol.proj.transform([longitude, latitude],
      "EPSG:4326", "EPSG:3857"));

    style = new ol.style.Style(
      {
      image: new ol.style.Circle(
        {
        radius: radius,
        snapToPixel: false,
        stroke: new ol.style.Stroke(
          {
          color: "rgba(255, 80, 0, 0.8)", // #F50 Int'l Orange
          width: 3
          })
        })
      });

    event.vectorContext.setStyle(style);
    event.vectorContext.drawGeometry(geom);

    map.render();
    };

  map.once("postcompose", animate);
  }
*/

