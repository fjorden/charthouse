
// Charthouse
// https://gitlab.com/fjorden/charthouse
// Copyright 2017-2019 Fjorden

"use strict";

// NOTE!  turf*.js must be loaded before calling this function!
function ageOff(features)
  {
  if (features == undefined || features.length == 0)
    {
    return;
    };

  var timeCurr;
  var _features;
  var index;

  timeCurr = (new Date).getTime() / 1000;

  _features = features;
  for (index=_features.length-1; index >= 0; --index)
    {
    var timeExpire;
    var timeStale;

    // fade out based on timeStale
    timeStale = _features[index].properties.datetime.epoch +
                _features[index].properties.datetime.timeStale;
    if (timeStale < timeCurr)
      {
      _features[index].properties.style.iconOpacity -= 0.25;
      if (_features[index].properties.style.iconOpacity < 0)
        _features[index].properties.style.iconOpacity = 0.1;

      if (_features[index].properties.style.textOpacity > 0)
        {
        _features[index].properties.style.textOpacity -= 0.25;
        if (_features[index].properties.style.textOpacity < 0)
          _features[index].properties.style.textOpacity = 0.1;
        };
      if (_features[index].properties.style.textBackgroundOpacity > 0)
        {
        _features[index].properties.style.textBackgroundOpacity -= 0.25;
        if (_features[index].properties.style.textBackgroundOpacity < 0)
          _features[index].properties.style.textBackgroundOpacity = 0.1;
        };
      };

    timeExpire = _features[index].properties.datetime.epoch +
                 _features[index].properties.datetime.timeExpire;

    if (timeExpire < timeCurr)
      _features.splice(index, 1);
    };

  return(_features);
  }

function sortGeo(geojson, _bbox)
  {
  if (geojson == undefined || geojson.features == undefined ||
      geojson.features.length == 0)
    {
    return;
    };

  var index;
  var bbox;

  bbox = turf.bboxPolygon([_bbox.lonW, _bbox.latS, _bbox.lonE, _bbox.latN]);

  for (index=geojson.features.length-1; index >= 0; --index)
    {
    var feature;
    var featureEnvelope;

    feature = geojson.features[index];

    // no geometry?  delete it
    if (feature == undefined || feature["geometry"] == undefined)
      {
      geojson.features.splice(index, 1);
      continue;
      }

    featureEnvelope = turf.envelope(feature);

    if (turf.booleanContains(bbox, featureEnvelope) == true ||
        turf.booleanOverlap(featureEnvelope, bbox) == true ||
        turf.booleanWithin(featureEnvelope, bbox) == true)
      {
      }
    else
      {
      geojson.features.splice(index, 1);
      };
    };

  return(geojson);
  };

