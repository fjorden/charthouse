
[
    {
        "id": "805dcd09.fdafd8",
        "type": "comment",
        "z": "4a7421ca.ad0218",
        "name": "Node-RED Mosquitto MQTT Status Flow",
        "info": "Charthouse\nhttps://gitlab.com/fjorden/charthouse\nCopyright 2018-2019 Fjorden\n",
        "x": 196,
        "y": 46,
        "wires": []
    },
    {
        "id": "4a7421ca.ad0218",
        "type": "tab",
        "label": "Mosquitto MQTT Broker",
        "disabled": false,
        "info": ""
    },
    {
        "id": "28fd8325.396324",
        "type": "mqtt in",
        "z": "4a7421ca.ad0218",
        "name": "",
        "topic": "$SYS/broker/clients/connected",
        "qos": "2",
        "broker": "337e2603.14f4ea",
        "x": 170,
        "y": 140,
        "wires": [
            [
                "7c3d929c.62526c",
                "fd906a6f.15cbe8"
            ]
        ]
    },
    {
        "id": "7c3d929c.62526c",
        "type": "ui_text",
        "z": "4a7421ca.ad0218",
        "group": "43e6d29e.7f0f6c",
        "order": 2,
        "width": 0,
        "height": 0,
        "name": "Clients connected",
        "label": "<i class=\"fa fa-exchange\"></i> <i class=\"fa fa-check\"></i> Clients connected",
        "format": "{{msg.payload}}",
        "layout": "row-spread",
        "x": 750,
        "y": 140,
        "wires": []
    },
    {
        "id": "cd245217.ac4a",
        "type": "mqtt in",
        "z": "4a7421ca.ad0218",
        "name": "",
        "topic": "$SYS/broker/clients/disconnected",
        "qos": "2",
        "broker": "337e2603.14f4ea",
        "x": 180,
        "y": 200,
        "wires": [
            [
                "af4d8e62.bd0e5"
            ]
        ]
    },
    {
        "id": "af4d8e62.bd0e5",
        "type": "ui_text",
        "z": "4a7421ca.ad0218",
        "group": "43e6d29e.7f0f6c",
        "order": 3,
        "width": 0,
        "height": 0,
        "name": "Clients disconnected",
        "label": "<i class=\"fa fa-exchange\"></i> <i class=\"fa fa-times\"></i> Clients disconnected",
        "format": "{{msg.payload}}",
        "layout": "row-spread",
        "x": 760,
        "y": 200,
        "wires": []
    },
    {
        "id": "e34e4648.c3e0e",
        "type": "mqtt in",
        "z": "4a7421ca.ad0218",
        "name": "",
        "topic": "$SYS/broker/messages/received",
        "qos": "2",
        "broker": "337e2603.14f4ea",
        "x": 170,
        "y": 320,
        "wires": [
            [
                "59723e38.ea4df"
            ]
        ]
    },
    {
        "id": "59723e38.ea4df",
        "type": "ui_text",
        "z": "4a7421ca.ad0218",
        "group": "43e6d29e.7f0f6c",
        "order": 6,
        "width": 0,
        "height": 0,
        "name": "Messages received",
        "label": "<i class=\"fa fa-envelope-o\"></i> <i class=\"fa fa-arrow-left\"></i> Messages received",
        "format": "{{msg.payload}}",
        "layout": "row-spread",
        "x": 750,
        "y": 320,
        "wires": []
    },
    {
        "id": "30649cb5.c558ec",
        "type": "mqtt in",
        "z": "4a7421ca.ad0218",
        "name": "",
        "topic": "$SYS/broker/messages/sent",
        "qos": "2",
        "broker": "337e2603.14f4ea",
        "x": 160,
        "y": 260,
        "wires": [
            [
                "d10e5736.c7d"
            ]
        ]
    },
    {
        "id": "d10e5736.c7d",
        "type": "ui_text",
        "z": "4a7421ca.ad0218",
        "group": "43e6d29e.7f0f6c",
        "order": 5,
        "width": 0,
        "height": 0,
        "name": "Messages sent",
        "label": "<i class=\"fa fa-envelope-o\"></i> <i class=\"fa fa-arrow-right\"></i>  Messages sent",
        "format": "{{msg.payload}}",
        "layout": "row-spread",
        "x": 740,
        "y": 260,
        "wires": []
    },
    {
        "id": "4516bb61.e78d24",
        "type": "mqtt in",
        "z": "4a7421ca.ad0218",
        "name": "",
        "topic": "$SYS/broker/publish/messages/dropped",
        "qos": "2",
        "broker": "337e2603.14f4ea",
        "x": 200,
        "y": 380,
        "wires": [
            [
                "93d609d.d9c5e78"
            ]
        ]
    },
    {
        "id": "93d609d.d9c5e78",
        "type": "ui_text",
        "z": "4a7421ca.ad0218",
        "group": "43e6d29e.7f0f6c",
        "order": 7,
        "width": 0,
        "height": 0,
        "name": "Messages dropped",
        "label": "<i class=\"fa fa-envelope-o\"></i> <i class=\"fa fa-times\"></i> Messages dropped",
        "format": "{{msg.payload}}",
        "layout": "row-spread",
        "x": 750,
        "y": 380,
        "wires": []
    },
    {
        "id": "7154cab4.d9f20c",
        "type": "mqtt in",
        "z": "4a7421ca.ad0218",
        "name": "",
        "topic": "$SYS/broker/retained messages/count",
        "qos": "2",
        "broker": "337e2603.14f4ea",
        "x": 190,
        "y": 440,
        "wires": [
            [
                "56494f87.0098a8"
            ]
        ]
    },
    {
        "id": "56494f87.0098a8",
        "type": "ui_text",
        "z": "4a7421ca.ad0218",
        "group": "43e6d29e.7f0f6c",
        "order": 8,
        "width": 0,
        "height": 0,
        "name": "Messages retained",
        "label": "<i class=\"fa fa-envelope-o\"></i> <i class=\"fa fa-arrow-down\"></i> Messages retained",
        "format": "{{msg.payload}}",
        "layout": "row-spread",
        "x": 750,
        "y": 440,
        "wires": []
    },
    {
        "id": "d9304529.4152b",
        "type": "mqtt in",
        "z": "4a7421ca.ad0218",
        "name": "",
        "topic": "$SYS/broker/subscriptions/count",
        "qos": "2",
        "broker": "337e2603.14f4ea",
        "x": 170,
        "y": 500,
        "wires": [
            [
                "9c5d5cbb.110668"
            ]
        ]
    },
    {
        "id": "9c5d5cbb.110668",
        "type": "ui_text",
        "z": "4a7421ca.ad0218",
        "group": "43e6d29e.7f0f6c",
        "order": 4,
        "width": 0,
        "height": 0,
        "name": "Subscriptions",
        "label": "<i class=\"fa fa-envelope-o\"></i> <i class=\"fa fa-check\"></i> Subscriptions",
        "format": "{{msg.payload}}",
        "layout": "row-spread",
        "x": 740,
        "y": 500,
        "wires": []
    },
    {
        "id": "2b75d75d.ec6a5",
        "type": "ui_text",
        "z": "4a7421ca.ad0218",
        "group": "43e6d29e.7f0f6c",
        "order": 1,
        "width": 0,
        "height": 0,
        "name": "Status",
        "label": "Status",
        "format": "{{msg.payload}}",
        "layout": "row-spread",
        "x": 710,
        "y": 100,
        "wires": []
    },
    {
        "id": "fd906a6f.15cbe8",
        "type": "trigger",
        "z": "4a7421ca.ad0218",
        "op1": "<font color=\"green\"><i class=\"fa fa-check fa-fw\"></i> Operational</font>",
        "op2": "<font color=\"red\"><i class=\"fa fa-times fa-fw\"></i> No data received</font>",
        "op1type": "str",
        "op2type": "str",
        "duration": "5",
        "extend": true,
        "units": "min",
        "reset": "",
        "name": "MQTT trigger",
        "x": 520,
        "y": 100,
        "wires": [
            [
                "2b75d75d.ec6a5"
            ]
        ]
    },
    {
        "id": "7f35a998.c207e",
        "type": "mqtt in",
        "z": "4a7421ca.ad0218",
        "name": "",
        "topic": "$SYS/broker/load/messages/sent/15min",
        "qos": "2",
        "broker": "337e2603.14f4ea",
        "x": 200,
        "y": 560,
        "wires": [
            []
        ]
    },
    {
        "id": "f7f43d68.243868",
        "type": "mqtt in",
        "z": "4a7421ca.ad0218",
        "name": "",
        "topic": "$SYS/broker/load/messages/received/15min",
        "qos": "2",
        "broker": "337e2603.14f4ea",
        "x": 210,
        "y": 620,
        "wires": [
            []
        ]
    },
    {
        "id": "337e2603.14f4ea",
        "type": "mqtt-broker",
        "z": "",
        "broker": "mqtt.local",
        "port": "1883",
        "clientid": "",
        "usetls": false,
        "compatmode": false,
        "keepalive": "60",
        "cleansession": true,
        "willTopic": "",
        "willQos": "0",
        "willPayload": "",
        "birthTopic": "",
        "birthQos": "0",
        "birthPayload": ""
    },
    {
        "id": "43e6d29e.7f0f6c",
        "type": "ui_group",
        "z": "",
        "name": "Mosquitto MQTT Broker",
        "tab": "5b7854c3.75ee4c",
        "order": 2,
        "disp": true,
        "width": "6"
    },
    {
        "id": "5b7854c3.75ee4c",
        "type": "ui_tab",
        "z": "",
        "name": "MQTT",
        "icon": "dashboard",
        "order": 1
    }
]

