
// Charthouse
// https://gitlab.com/fjorden/charthouse
// Copyright 2018-2019 Fjorden

"use strict";

var chartMain;

function chartsUpdate()
  {
  var index;
  var colors = [
        "rgb(255, 99, 132)",  // red
        "rgb(255, 159, 64)",  // orange
        "rgb(255, 205, 86)",  // yellow
        "rgb(75, 192, 192)",  // green
        "rgb(54, 162, 235)",  // blue
        "rgb(153, 102, 255)"  // purple
        ];

  for (index=chartMain.data.datasets.length-1; index >= 0; --index)
    {
    chartMain.data.datasets.pop();
    };

  for (index=0; index < layersData.length; ++index)
    {
    if (layersData[index].total >= 0)
      {
      var data;
      var color;

      //console.log(layersData[index].name, layersData[index].total);

      color = colors[index % colors.length];

      data =
        {
        label: layersData[index].name,
        backgroundColor: Chart.helpers.color(color).alpha(0.5).rgbString(),
        borderColor: color,
        borderWidth: 1,
        data: [ layersData[index].total ],
        };

      chartMain.data.datasets.push(data);
      };
    };

  chartMain.update();
  };

function chartsCreate()
  {
  var data;
  var options;
  var ctx;

  data =  [ 0, 30, 10, 20, 40];

  options = { responsive: true };

  ctx = document.getElementById("statisticsCanvas").getContext("2d");
  chartMain = new Chart(ctx,
    {
    type: "horizontalBar",
    options: options
    }
    );

  chartsUpdate();
  };

