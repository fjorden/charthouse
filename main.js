
// Charthouse
// https://gitlab.com/fjorden/charthouse
// Copyright 2017-2019 Fjorden

"use strict";

var map;

var mapExtent;

var scaleLine;

var interactionSelect;

var interactionMeasure;
var posMeasureStart;

var interactionRangeRing;

var interactionAim;
var aimName;
var aimUUID;
var aimIP;


var posCursor;

var crosshair;

var overlayPopupFeature;
var popupFeature;
var popupFeatureHeader;
var popupFeatureContent;

var buttonMeasure;
var buttonMeasureBackground;
var buttonMeasureColor;
var overlayPopupSmall;
var popupSmall;
var popupSmallContent;
var tooltipPopupSmall;

var buttonMarkerAdd;
var buttonMarkerAddBackground;
var buttonMarkerAddColor;

var hudPositionCenter;
var hudPositionCursor;
var hudPositionDistAz;
var hudDiagnostics;

var featuresCount;

var formatGeoJSON;
var formatKML;

var searchAnimateCount=-1;

var buttonSearch;

var filterToggle=false;
var buttonFilter;
var buttonFilterBackground;
var buttonFilterColor;

var buttonFollow;
var badgeFollow;

var compassRose;
var compassRoseCard;
var compassRoseAzimuth;

var layersLoadMutex=false;

var workers=[];

var workerMQTT;

var mouseMoveTimeout;
var mousePixelCurrent;
var mouseCoordinateCurrent;

// keys that are pressed or not
var keysStatus={};

  function measureModeToggle()
    {
    if (posMeasureStart == undefined)
      {
      buttonMeasureBackground = buttonMeasure.style.background;
      buttonMeasureColor = buttonMeasure.style.color;
      buttonMeasure.style.background = "#C00";
      buttonMeasure.style.color = "#EEE";
      map.addInteraction(interactionMeasure);
      }
    else
      {
      elemHide(popupSmall, true);
      interactionMeasure.removeLastPoint();
      interactionMeasure.removeLastPoint();
      map.removeInteraction(interactionMeasure);
      posMeasureStart = undefined;
      buttonMeasure.style.background = buttonMeasureBackground;
      buttonMeasure.style.color = buttonMeasureColor;
      };
    }

  function rangeRingToggle()
    {
    if (posMeasureStart == undefined)
      {
      buttonMeasureBackground = buttonMeasure.style.background;
      buttonMeasureColor = buttonMeasure.style.color;
      buttonMeasure.style.background = "#C00";
      buttonMeasure.style.color = "#EEE";
      map.addInteraction(interactionRangeRing);
      }
    else
      {
      elemHide(popupSmall, true);
      interactionMeasure.removeLastPoint();
      interactionMeasure.removeLastPoint();
      map.removeInteraction(interactionRangeRing);
      posMeasureStart = undefined;
      buttonMeasure.style.background = buttonMeasureBackground;
      buttonMeasure.style.color = buttonMeasureColor;
      };
    }

  function loadLayer(layer)
    {
    var currtime;

    currtime = (new Date()).getTime() / 1000;

    if (layer.OLtype != "Heatmap" && layer.OLtype != "Vector")
      {
      layer.trytime = currtime;
      return;
      };

    var url;
    var extent;
    var latN;
    var latS;
    var lonE;
    var lonW;
    var zoomp;
    var filterName="";
    var cmd;
    var index;
    var found=false;

    // view bounding box
    extent = ol.proj.transformExtent(mapExtent, 'EPSG:3857', 'EPSG:4326');
    latN = extent[3];
    latS = extent[1];
    lonE = extent[2];
    lonW = extent[0];

    if (settings["advanced"]["showFeaturesAll"] === true)
      zoomp = 100;
    else
      zoomp = settings["center"]["zoomp"];

    if (filterToggle)
      filterName = document.getElementById("filterName").value;
    else
      filterName = "";

    cmd =
      {
      debug: settings["advanced"]["showDebug"],
      baseURL: baseURL,
      layerUUID: layer.uuid,
      layerName: layer.name,
      layerGroup: layer.group,
      url: layer.url,
      lat: settings["center"]["latitude"],
      lon: settings["center"]["longitude"],
      latN: latN,
      latS: latS,
      lonE: lonE,
      lonW: lonW,
      visible: layer.layer.getVisible(),
      zoomp: zoomp,
      filterName: filterName
      };

    if (layer.dataFunction != undefined)
      {
      if (settings["advanced"]["showDebug"] === true)
        console.log("Data function: Start: " + layer.name);

      layer.dataFunction(cmd);
      if (layer.url == undefined)
        {
        layer.trytime = currtime;
        return;
        };
      };

    // find a suitable worker and use it, otherwise just bail
    for (index=0; index < workers.length; ++index)
      {
      if (workers[index].running === false)
        {
        workers[index].running = true;

        if (settings["advanced"]["showDebug"] === true)
          console.log('Start worker: "' + layer.name + '" URL:' + layer.url);

        workers[index].postMessage(cmd);

        layer.trytime = currtime;
        found = true;

        break;
        };
      };

    // oops, all workers are busy, so try again very soon
    if (found === false)
      {
      if (settings["advanced"]["showDebug"])
        console.log("All workers busy: " + layer.name);

      layer.trytime = 0;
      };
    }

  function addLayers(layers, menuId)
    {
    var index;
    var currtime;
    var groups=[];
    var html;

    currtime = (new Date()).getTime() / 1000;

    html = "";
    for (index=0; index < layers.length; ++index)
      {
      var group;

      group = layers[index].group;
      if (group == undefined || group.length == 0)
        group = "Miscellaneous";
      if (!groups.includes(group))
        groups.push(group);
      };
    groups.sort(function(a, b){return(a-b);});

    for (index=0; index < groups.length; ++index)
      {
      html += "<tbody id='" + menuId + "-" + groups[index] + "'><tr><th>" +
        groups[index] + "</th><th></th><th></th>" +
        "<th id='headerVisible'></th><th id='headerTotal'></th></tr></tbody>";
      };

    document.getElementById(menuId).innerHTML = html;

    for (index=0; index < layers.length; ++index)
      {
      // initialize layer and add it to the map
      // (if it hasn't already been initialized and added)
      if (layers[index].layer == undefined)
        {
        var type;
        var id;

        if (layers[index].name == undefined)
          {
          console.log("Error: No layer name");
          continue;
          };
        if (layers[index].url == undefined &&
            layers[index].dataFunction == undefined &&
            layers[index].OLtype != "OSM")
            {
            console.log("Warning: " + layers[index].name + 
                        ": No layer URL or dataFunction");
            continue;
          };
        if (layers[index].OLtype == undefined)
          {
            console.log("Warning: " + layers[index].name + ": No layer type");
            continue;
          };
        if (layers[index].group == undefined)
          layers[index].group = "Miscellaneous";
        if (layers[index].params == undefined)
          layers[index].params = "";
        if (layers[index].interval == undefined)
          layers[index].interval = 120;
        if (layers[index].input == undefined)
          layers[index].input = "checkbox";
        if (layers[index].visible == undefined)
          layers[index].visible = 1;
        if (layers[index].loadtime == undefined)
          layers[index].loadtime = 0;
        if (layers[index].trytime == undefined)
          layers[index].trytime = 0;
        if (layers[index].uuid == undefined)
          layers[index].uuid = uuid();

        if (layers[index].OLtype == "Vector")
          {
          var source;

          source = new ol.source.Vector(
            {
            wrapX: false,
            noWrap: true
            });
          // DO NOT use a style function because it causes OpenLayers to
          // flicker the features badly.  set style directly instead.
          layers[index].layer = new ol.layer.Vector(
            {
            source: source
            });
          }
        else if (layers[index].OLtype == "Heatmap")
          {
          var source;

          source = new ol.source.Vector(
            {
            wrapX: false,
            noWrap: true
            });
          // no style for heatmap layers
          layers[index].layer = new ol.layer.Heatmap(
            {
            source: source,
            blur: 25,
            radius: 20
            });
          }
        else if (layers[index].OLtype == "Image")
          {
          var source;

          source = new ol.source.ImageStatic(
            {
            url: layers[index].url,
            projection: map.getView().getProjection(),
            imageExtent: ol.extent.applyTransform(
              layers[index].extent,
              ol.proj.getTransform("EPSG:4326", "EPSG:3857"))
            });
          layers[index].layer = new ol.layer.Image(
            {
            source: source,
            opacity: layers[index].visible
            });
          }
        else if (layers[index].OLtype == "OSM")
          {
          var source;

          source = new ol.source.OSM(
            {
            wrapX: false,
            noWrap: true
            });
          layers[index].layer = new ol.layer.Tile(
            {
            preload: Infinity,
            source: source
            });
          }
        else if (layers[index].OLtype == "TileWMS")
          {
          var source;

          source = new ol.source.TileWMS(
            {
            url: layers[index].url,
            params: layers[index].params,
            //crossOrigin: "anonymous",
            wrapX: false,
            noWrap: true
            });
          layers[index].layer = new ol.layer.Tile(
            {
            preload: Infinity,
            source: source
            });
          }
        else if (layers[index].OLtype == "TopoJSON")
          {
          var source;

          source = new ol.source.Vector(
            {
            url: layers[index].url,
            crossOrigin: "Anonymous",
            format: new ol.format.TopoJSON(),
            overlaps: false,
            wrapX: false,
            noWrap: true
            });
          layers[index].layer = new ol.layer.Vector(
            {
            source: source
            });
          }
        else if (layers[index].OLtype == "XYZ")
          {
          var source;

          source = new ol.source.XYZ(
            {
            url: layers[index].url,
            //crossOrigin: "Anonymous",
            projection: ol.proj.get("EPSG:4326"),
            tileSize: 512,
            tileUrlFunction: function(tileCoord)
              {
              return(this.getUrls()[0]
                     .replace("{z}", (tileCoord[0]-1).toString())
                     .replace("{x}", tileCoord[1].toString())
                     .replace("{y}", (-tileCoord[2]-1).toString()));
              },
            wrapX: false,
            noWrap: true
            });
          layers[index].layer = new ol.layer.Tile(
            {
            preload: Infinity,
            source: source
            });
          }
        else
          {
          console.log("Error: " + layers[index].name + ": Invalid type: " +
                      layers[index].OLtype);
          continue;
          };

        if (layers[index].visible > 0)
          layers[index].layer.setVisible(true);
        else
          layers[index].layer.setVisible(false);
        map.addLayer(layers[index].layer);

        // add menu option

        if (layers[index].input == "slider")
          {
          type = "class='uk-range' type='range' min=0 max=100 ";
          if (layers[index].visible > 0)
            type += "value='" + layers[index].visible * 100 + "'";
          else
            type += "value='0'";
          }
        else
          {
          type = "type='checkbox' class='uk-checkbox'";
          if (layers[index].visible > 0)
            type += " checked='checked'";
          };

        //html = "<tr><td style='width:2em'></td>" +
        html = "<tr><td style='text-align:right'><input " + type + " id='" +
          layers[index].uuid + "' oninput='layersAllSetVisible()' /></td>" +
          "<td><label for='" + layers[index].uuid + "'>" +
          layers[index].name + "</label></td>" +
          "<td id='" + layers[index].uuid + "-visible'></td>" +
          "<td id='" + layers[index].uuid + "-total'></td></tr>";

        document.getElementById(menuId + "-" +
          layers[index].group).innerHTML += html;
        };
      };
    }

  function layersSetVisible(formId, layers)
    {
    var index;

    for (index=0; index < layers.length; ++index)
      {
      var element;

      element = document.getElementById(layers[index].uuid);
      if (element != null)
        {
        if (layers[index].input == "slider")
          {
          var val;

          val = element.value;
          if (val < 0)
            val = 0;
          if (val > 100)
            val = 100;
          if (val > 0)
            layers[index].layer.setVisible(true);
          else
            layers[index].layer.setVisible(false);
          layers[index].layer.setOpacity(val/100);
          }
        else if (layers[index].input == "checkbox")
          {
          layers[index].layer.setVisible(element.checked);
          };
        };
      };
    }

  function layersLoad(force)
    {
    if (layersLoadMutex === true)
      return;

    var index;
    var currtime;

    currtime = (new Date()).getTime() / 1000;

    for (index=0; index < layersData.length; ++index)
      {
      if (layersData[index].interval > 0 && (force === true ||
          currtime - layersData[index].trytime > layersData[index].interval))
        {
        loadLayer(layersData[index]);
        };
      };

    for (index=0; index < layersBase.length; ++index)
      {
      if (force === true ||
          currtime - layersBase[index].trytime > layersBase[index].interval)
        {
        loadLayer(layersBase[index]);
        };
      };
    }

  function layersAllSetVisible()
    {
    layersSetVisible("formDataLayers", layersData);
    layersSetVisible("formMapsLayers", layersBase);
    layersLoad(true);
    }

  function layersDataSetVisible()
    {
    var index;

    for (index=0; index < layersData.length; ++index)
      {
      var element;

      element = document.getElementById(layersData[index].uuid);
      if (element != null)
        {
        if (layersData[index].input == "slider")
          {
          }
        else if (layersData[index].input == "checkbox")
          {
          element.checked = true;
          };
        };
      };

    layersSetVisible("formDataLayers", layersData);
    layersLoad(true);
    }

  function layersDataSetInvisible()
    {
    var index;

    for (index=0; index < layersData.length; ++index)
      {
      var element;

      element = document.getElementById(layersData[index].uuid);
      if (element != null)
        {
        if (layersData[index].input == "slider")
          {
          }
        else if (layersData[index].input == "checkbox")
          {
          element.checked = false;
          };
        };
      };

    layersSetVisible("formDataLayers", layersData);
    layersLoad(true);
    }

  function featuresFilter()
    {
    layersLoad(true);
    }

  function setFilterToggle()
    {
    if (filterToggle)
      {
      filterToggle.style.background = filterToggleBackground;
      filterToggle.style.color = filterToggleColor;
      filterToggle = false;
      }
    else
      {
      filterToggle = true;
      filterToggleBackground = filterToggle.style.background;
      filterToggleColor = filterToggle.style.color;
      filterToggle.style.background = "#C00";
      filterToggle.style.color = "#EEE";
      };
    }

  // filter out polygons
  function featuresFilterOutPolygons(features)
    {
    if (features === undefined)
      return;

    features = features.filter(function(elem, index, self)
      {
      var geometries;

      if (typeof elem.getGeometry().getGeometries === "function")
        {
        var jndex;

        geometries = elem.getGeometry().getGeometries();
        for (jndex=0; jndex < geometries.length; ++jndex)
          {
          if (geometries[jndex].getType() === "Polygon")
            return(false);
          };
        }
      else if (typeof elem.getGeometry().getType === "function")
        {
        if (elem.getGeometry().getType() === "Polygon")
          return(false);
        };

      return(true);
      });

    return(features);
    }

  function popupFeatureShow(features, coord, showx)
    {
    dropdownsClose();

    features = featuresFilterOutPolygons(features);

    if (features.length == 0)
      return(false);

    var index;
    var text="";

    popupFeatureContent.innerHTML = "";
    for (index=0; index < features.length; ++index)
      {
      var _text;

      // XXX remove duplicates
      _text = getFeatureText(features[index], true);
      if (_text.length > 0)
        {
        if (showx === true)
          {
          var position;

          text += "<div id='buttonBar-" + getUUID(features[index]) +
                  "' class='btn-group btn-group-horiz' " +
                  "style='position:relative;float:right;'>";

          text += "<button class='uk-button uk-button-primary' " +
                  "title='Follow " + getName(features[index]) + "' " +
                  "onclick='followOn(\"" + getName(features[index]) +
                  "\", \"" + getUUID(features[index]) + "\")'>" +
                  "<i class='fas fa-thumbtack fa-fw'></i></button>";

          if (features[index].get("deletable") === "true")
            {
            text += "<button class='uk-button uk-button-primary' " +
                    "title='Delete " + getName(features[index]) + "' " +
                    "onclick='markerDelete(\"" + getUUID(features[index]) +
                    "\")'><i class='fas fa-trash fa-fw'></i>" +
                    "</button>";
            };

          position = features[index].get("position");
          if (position != undefined)
            {
            var aimpoint;

            aimpoint = position["aimpoint"];
            if (aimpoint != undefined)
              {
              var ip;

              ip = getIP(features[index]);
              if (ip != undefined)
                {
                text += "<button class='uk-button uk-button-primary' " +
                  "title='Set aimpoint " + getName(features[index]) +
                  "' onclick='aimOn(\"" + getName(features[index]) +
                  "\", \"" + getUUID(features[index]) + "\", \"" +
                  ip + "\")'>" +
                  "<i class='fas fa-map-pin fa-fw'></i>" +
                  "</button>";
                };
              };
            };

          text += "</div>";
          };

        text += "<div>" + _text + "</div>"
        };
      };

    if (text.length == 0)
      {
      document.body.style.cursor = "default";
      popupFeatureClose();
      return(false);
      };

    popupFeatureContent.scrollTop = 0;
    popupFeatureContent.scrollLeft = 0;
    elementPosition(overlayPopupFeature, coord);
    popupFeatureContent.innerHTML = text;

    if (showx === true)
      {
      elemShow(popupFeatureHeader);
      setTimeout(function(){popupFeatureClose();}, 15000);
      }
    else
      {
      elemHide(popupFeatureHeader);
      };

    elemShow(popupFeature);

    return(true);
    }

  function popupFeatureClose()
    {
    elemHide(popupFeatureHeader);
    elemHide(popupFeature);
    }

  function zoomIn()
    {
    map.getView().setZoom(map.getView().getZoom()+1);
    }

  function zoomOut()
    {
    map.getView().setZoom(map.getView().getZoom()-1);
    }

  function onKeyDown(evt)
    {
    var key = evt.which || evt.keyCode || 0;

    popupFeatureClose();

    keysStatus[key] = true;

    if (settings["advanced"]["showDebug"])
      console.log("key Down", key);
    }

  function onKeyUp(evt)
    {
    var key = evt.which || evt.keyCode || 0;

    keysStatus[key] = false;

    if (settings["advanced"]["showDebug"])
      console.log("key Up  ", key);

    switch (key)
      {
      case 16:   // shift
        break;
      case 27:   // Esc
        break;
      case 61:   // '=' (simulate being the '+' key
        zoomIn();
        break;
      case 82:   // 'r'
        mapRotate(0);
        break;
      };
    }

  function onMoveEnd()
    {
    layersLoadMutex = false;

    updateCenter();
    updateCompass();
    updateSunMoon();
    setTimeout(function(){layersLoad(true);}, 2000);
    }

  function onMouseDown(evt)
    {
    document.body.style.cursor = "move";
    }

  function onMouseUp(evt)
    {
    document.body.style.cursor = "default";
    }

  function onMouseMove(evt)
    {
    mousePixelCurrent = evt.pixel;
    mouseCoordinateCurrent = evt.coordinate;

    // notify everyone that pointer has stopped moving
    if (mouseMoveTimeout !== undefined)
      window.clearTimeout(mouseMoveTimeout);
    mouseMoveTimeout = window.setTimeout(function()
      {
      var evt;

      evt = new CustomEvent("pointerstop", {"detail": "stopped"});
      document.dispatchEvent(evt);
      }, 250);

    updateCursor(mouseCoordinateCurrent);

    if (evt.dragging)
      {
      document.body.style.cursor = "move";
      updateCenter();
      updateCompass();
      layersLoadMutex = true;

      return;
      };

    layersLoadMutex = false;

    if (posMeasureStart != undefined)
      {
      var distance;
      var azimuth;

      distance = posMeasureStart.distanceTo(posCursor);
      azimuth = posMeasureStart.initialBearingTo(posCursor);
      popupSmallContent.innerHTML = distanceString(distance) +
        "&nbsp;" + azimuthString(azimuth);
      elementPosition(overlayPopupSmall, mouseCoordinateCurrent);
      }
    else if (aimName != undefined)
      {
      popupSmallContent.innerHTML =
        "<i class='fas fa-map-pin fa-fw'></i> " +
        "Set aimpoint for " + aimName;
      elementPosition(overlayPopupSmall, mouseCoordinateCurrent);
      };

/*
    show pointer if over a feature XXX need to ensure stuff like
    sun terminator don't cause this to change pointer
    {
    var features;

    features = map.getFeaturesAtPixel(mousePixelCurrent);
    if (features != null && features != undefined && features.length > 0)
      document.body.style.cursor = "pointer";
    else
      document.body.style.cursor = "default";
    };
*/
    }

  function onMouseStop()
    {

    if (mouseMoveTimeout !== undefined)
      window.clearTimeout(mouseMoveTimeout);

    // if popupFeature was clicked, then keep it up
    if (popupFeatureHeader.style.display == "block")
      return;

    var features=[];
    var index;
    var show=false;

    features = map.getFeaturesAtPixel(mousePixelCurrent);
    if (features != null && features != undefined)
      {
      // delete duplicates
      features = features.filter(function(elem, index, self)
        {
        return(index == self.indexOf(elem));
        });

      features = featuresFilterOutPolygons(features);

      if (features.length > 0)
        show = true;
      }

    if (show)
      {
      if (popupFeatureShow(features, mouseCoordinateCurrent))
        document.body.style.cursor = "pointer";
      }
    else
      {
      popupFeatureClose();
      document.body.style.cursor = "default";
      };
    }

  function onZoom()
    {
    dropdownsClose();
    popupFeatureClose();
    }

  function onPointerLeave()
    {
    updateCursor();
    }

  function updateDiagnostics()
    {
    var index;

    hudDiagnostics.innerHTML = "";

    featuresCount = 0;
    for (index=0; index < layersData.length; ++index)
      featuresCount += layersData[index].layer.getSource().getFeatures().length;

    if (settings["advanced"]["showDiagnostics"])
      {
      hudDiagnostics.innerHTML = "<font class='small uk-text-muted'>Zoom: " +
         settings["center"]["zoomp"] + "% (" + settings["center"]["zoom"] +
         ")<br>Features visible: " + featuresCount + "</font>";
      };

    if (settings["other"]["showFeaturesCount"])
      {
      chartsUpdate();
      };

    if (featuresCount >= 10000)
      {
      UIkit.notification({status: "warning", message:
        "<div class='ch-center'>" +
        "<i class='fas exclamation-circle fa-fw'></i> " +
        "Too many visible features may result in poor performance.<br>" +
        "<b><i>Zoom in</i></b>, <b><i>Filter Features</i></b>, or " +
        "<b><i>Hide Layers</i></b><br>" +
        "to reduce the number of visible features.</div>"});
      };
    }

  function updateDatetime()
    {
    document.getElementById("datetime").innerHTML =
      "<i class='fas fa-clock fa-fw'></i> " +
      datetimeString(new Date());
    }

  function updateCursor(coord)
    {
    if (coord == undefined)
      {
      hudPositionCursor.innerHTML = "<div></div>";
      hudPositionDistAz.innerHTML = "<div></div>";
      }
    else
      {
      var lonlat
      var distance;
      var azimuth;
      var posCenter;

      lonlat = ol.proj.transform(coord, "EPSG:3857", "EPSG:4326");
      lonlat[1] = roundf(lonlat[1], 5); // latitude
      lonlat[0] = roundf(lonlat[0], 5); // longitude

      posCenter = new LatLon(settings["center"]["latitude"],
                             settings["center"]["longitude"]);
      posCursor = new LatLon(lonlat[1], lonlat[0]);
      distance = posCenter.distanceTo(posCursor);
      azimuth = posCenter.initialBearingTo(posCursor);

      hudPositionCursor.innerHTML =
        "<i class='fas fa-mouse-pointer fa-fw'></i>" +
        latlonString(lonlat[1], lonlat[0]);

      hudPositionDistAz.innerHTML =
        "<i class='fas fa-arrow-right fa-fw'></i>" +
        distanceString(distance) + "&nbsp;" + azimuthString(azimuth);
      };
    }

  function updateCenter()
    {
    var coord;
    var lonlat;

    mapExtent = map.getView().calculateExtent(map.getSize());
    coord = map.getView().getCenter();

    lonlat = ol.proj.transform(coord, "EPSG:3857", "EPSG:4326");

    settings["center"]["latitude"] = roundf(lonlat[1], 5);
    settings["center"]["longitude"] = roundf(lonlat[0], 5);
    settings["center"]["zoom"] = roundf(map.getView().getZoom(), 1);

    // zoom as a percentage (20 is zoomed in, 2 is zoomed out)
    settings["center"]["zoomp"] =
      Math.round((settings["center"]["zoom"] - 2) / 18 * 100);

    hudPositionCenter.innerHTML =
      "<i class='fas fa-plus fa-fw'></i>" +
      latlonString(lonlat[1], lonlat[0]);

    if (workerMQTT != undefined)
      {
      var layer;
      var extent;
      var cmd;

      layer = getLayerByName("MQTT");
      extent = ol.proj.transformExtent(mapExtent, 'EPSG:3857', 'EPSG:4326');

      cmd =
        {
        "cmd": "bbox",
        debug: settings["advanced"]["showDebug"],
        layerUUID: layer.uuid,
        layerName: layer.name,
        layerGroup: layer.group,
        "latN": extent[3],
        "latS": extent[1],
        "lonE": extent[2],
        "lonW": extent[0]
        };

      workerMQTT.postMessage(cmd);
      };
    }

  function updateCompass()
    {
    var viewAz;
    var northRot;

    viewAz = roundf((map.getView().getRotation() * 180 / Math.PI), 0) % 360;
    if (viewAz < 0)
      viewAz += 360;
    viewAz = 360 - viewAz;
    if (viewAz == 360)
      viewAz = 0;

    if (viewAz === 0)
      {
      elemHide(compassRose);
      return;
      };

    elemShow(compassRose);

    northRot = 360 - viewAz;

    if (viewAz < 10)
      viewAz = "00" + viewAz;
    else if (viewAz < 100)
      viewAz = "0" + viewAz;

    compassRoseAzimuth.textContent = viewAz + "°T";

    compassRoseCard.setAttribute("transform",
      "rotate(" + northRot + " 250 250)");
    }

  function layersFilter(feature, layer)
    {
    var node;

    // only show if feature actually is a proper libmote GeoJSON feature
    node = feature.get("node");
    if (node != undefined)
      {
      var name;

      name = node["name"];
      if (name != undefined && name.length > 0)
        return(true);
      };

    return(false);
    }

function aimOn(name, uuid, ip)
  {
  aimName = name;
  aimUUID = uuid;
  aimIP = ip;
  popupFeatureClose();
  elemShow(popupSmall, true);
  map.addInteraction(interactionAim);
  // XXX how to change name of cursor text
  }

function aimSet(name, uuid, ip, latitude, longitude)
  {
  var evt;
  var obj;

  obj = {};
  obj.name = name;
  obj.uuid = uuid;
  obj.latitude = latitude;
  obj.longitude = longitude;
  obj.ip = ip;

  evt = new CustomEvent("aimSet", {"detail": JSON.stringify(obj)});
  document.dispatchEvent(evt);
  }

function aimOff()
  {
  aimName = undefined;
  aimUUID = undefined;
  elemHide(popupSmall, true);
  map.removeInteraction(interactionAim);
  }

function workerCB(evt)
  {
  var index;
  var features;
  var obj;
  var layer;

  evt.target.running = false; // turn off web worker mutex

  obj = JSON.parse(evt.data);
  if (settings["advanced"]["showDebug"] === true)
    {
    var msg;

    msg = 'workerCB(): "' + obj.layerName + '": Total:' + obj.total +
      ' Visible:' + obj.visible;

    if (obj.timeLoad != undefined && obj.timeLoad > 0)
      msg += " Load:" + obj.timeLoad + "s";

    if (obj.url != undefined && obj.url.length > 0)
      msg += " URL:" + obj.url;

    console.log(msg);
    };

  if (obj.error != undefined)
    {
    UIkit.notification({status: "danger", message:
      "<div class='ch-center'>" +
      "<i class='fas fa-exclamation-triangle fa-fw'></i> " +
      obj.error + "</div>"});

    return;
    };

  if (obj.notice != undefined)
    {
    UIkit.notification({status: "success", message:
      "<div class='ch-center'><i class='fas fa-info-circle fa-fw'></i> " +
      obj.notice + "</div>"});

    return;
    };

  if (obj.warning != undefined)
    {
    UIkit.notification({status: "warning", message:
      "<div class='ch-center'>" +
      "<i class='fas fa-exclamation-circle fa-fw'></i> " +
      obj.warning + "</div>"});

    return;
    };

  layer = getLayerByUUID(obj.layerUUID);
  if (layer == undefined)
    {
    console.log('workerCB(): "' + obj.layerName +
                '" getLayerByUUID(' + obj.layerUUID + "): Not found");
    return;
    };

  layer.total = -1;
  obj.visible = -1;
  layer.loadtime = (new Date()).getTime() / 1000;

  // if error, clear out features
  if (obj.xhr.status != 200)
    {
    if (settings["advanced"]["showDebug"] === true &&
        obj.xhr.status != 404)
      {
      console.log('workerCB(): "' + layer.name + '": ' + obj.xhr.status);
      };

    layerClear(layer.layer);
    }
  else
    {
    if (obj.geojson != undefined && obj.geojson.features != undefined &&
        obj.geojson.features.length > 0)
      {
      try
        {
        features = formatGeoJSON.readFeatures(obj.geojson,
          {featureProjection: "EPSG:3857"});
        }
      catch(err)
        {
        console.log('workerCB(): GeoJSON: "' + layer.name + '": '+err.message);
        console.log("workerCB(): GeoJSON:", obj.geojson.features.length,
                    obj.geojson);
        };
      }
    else if (obj.kml != undefined)
      {
      try
        {
        features = formatKML.readFeatures(obj.kml,
          {featureProjection: "EPSG:3857"});
        }
      catch(err)
        {
        console.log('workerCB(): KML: "'+layer.name+'": ' + err.message);
        console.log("workerCB(): KML:", obj.kml.length, obj.kml);
        };
      }
    else
      {
      if (settings["advanced"]["showDebug"])
        console.log('workerCB(): "'+layer.name+'": No GeoJSON or KML present');
      };

    if (features != undefined)
      {
      layer.total = features.length;

      // only create styles for GeoJSON (use styles in KML data for KML)
      // and no styles for Heatmap
      if (features.length > 0 &&
          obj.geojson != undefined && layer.OLtype != "Heatmap")
        {
        setStyles(features);
        };
      };

    layerClear(layer.layer);
    if (features != undefined && features.length > 0)
      {
      //console.log('workerCB():"' + layer.name + '"XXX: ' + features.length);
      //console.dir(features[0]);
      layer.layer.getSource().addFeatures(features);
      };
    };

  // update feature counts
  if (settings["other"]["showFeaturesCount"])
    {
    var elemTotal;
    var elemVisible;
    var elemHeaderTotal;
    var elemHeaderVisible;
    var html;

    elemTotal = document.getElementById(obj.layerUUID + "-total");
    elemTotal.innerHTML = "";

    elemVisible = document.getElementById(obj.layerUUID + "-visible");
    elemVisible.innerHTML = "";

    elemHeaderVisible = document.getElementById("headerVisible");
    elemHeaderVisible.innerHTML =
      "<font class='small uk-text-muted'>View</font>";

    elemHeaderTotal = document.getElementById("headerTotal");
    elemHeaderTotal.innerHTML = "<font class='small uk-text-muted'>" +
      "Global</font>";

    if (layer.total >= 0)
      {
      html = "<font class='small ";
      if (layer.total == 0)
        html += "red-fore";
      else
        html += "darkgreen-fore";
      html += "'>" + layer.total + "</font>";
      elemTotal.innerHTML = html;
      }
    else
      {
      // error message
      //if (obj.xhr.status == 404)
        {
        elemTotal.innerHTML =
          "<font class='small red-fore'>" +
          "no data</font>";
        };
      };

    if (obj.visible >= 0)
      {
      html = "<font class='small ";
      if (obj.visible == 0)
        html += "red-fore";
      else
        html += "darkgreen-fore";
      html += "'>" + obj.visible + "</font>";
      elemVisible.innerHTML = html;
      }
    else
      {
      // XXX error message?
      };
    };

  evt = new CustomEvent("layerLoaded", {"detail": layer.uuid});
  document.dispatchEvent(evt);
  }

function workerCBcall(features, url, layerUUID, layerName, layerGroup)
  {
  var geojson;
  var data;
  var evt;

  // build geojson
  geojson = {};
  geojson.type = "FeatureCollection";
  geojson.features = features;

  // build data to send to workerCB()
  data = {};
  data.url = url;
  data.layerUUID = layerUUID;
  data.layerName = layerName;
  data.layerGroup = layerGroup;
  data.total = geojson.features.length;
  data.visible = geojson.features.length;
  data.xhr = {};
  data.xhr.status = 200;
  data.geojson = geojson;

  // build a fake message to send to workerCB()
  evt = {};
  evt.target = {};
  evt.target.running = false;
  evt.data = JSON.stringify(data);

  workerCB(evt);
  }

function viewLink()
  {
  var view;
  var center;
  var permalink;
  var url;

  view = map.getView();
  center = view.getCenter();
  permalink = window.location.href.replace(/#map=*/i, "");
  permalink = permalink + "#map=" +
    view.getZoom() + "/" +
    Math.round(center[0]*100) / 100 + "/" +
    Math.round(center[1]*100) / 100 + "/" +
    view.getRotation();

  return(permalink);
  }

function viewLinkEmail()
  {
  var permalink;
  var url;

  permalink = viewLink();

  url = "mailto:?subject=[" + siteTitle + "] Map View" +
        "&body=\n\nTake a look at this map view:\n\n" +
        permalink + "\n\n";

  url = encodeURI(url);
  window.location.href = url;
  }

function setHeader(text, colorFG, colorBG)
  {
  var headerHeight;
  var elem;

  headerHeight = 1.5;

  elem = document.getElementById("header");
  elem.innerHTML = text;
  elem.style.backgroundColor = colorBG;
  elem.style.color = colorFG;
  elem.style.display = "block";

  // adjust components
  elem = document.getElementById("navbarMain");
  elem.style.top = headerHeight + "em";

  elem = document.getElementById("hud");
  elem.style.top = (headerHeight + 3) + "em";
  }

function setFooter(text, colorFG, colorBG)
  {
  var footerHeight;
  var elem;
  var elems;
  var index;

  footerHeight = 1.5;

  elem = document.getElementById("footer");
  elem.innerHTML = text;
  elem.style.backgroundColor = colorBG;
  elem.style.color = colorFG;
  elem.style.maxHeight = footerHeight + "em";
  elem.style.display = "block";

  elems = document.getElementsByClassName("ol-scale-line");
  footerHeight = (footerHeight + 1) + "em";
  for (index=0; index < elems.length; ++index)
    {
    elems[index].style.bottom = footerHeight;
    };
  }

function charthouse()
  {
  var charthouseURL;
  var elem;

  if (siteTitle != undefined)
    document.title = siteTitle;

  if (baseURL == undefined)
    baseURL = "/";
  else if (!baseURL.match(/\/$/))
    baseURL += "/";
  if (charthouseURL == undefined)
    charthouseURL = baseURL + "charthouse.html";

  document.addEventListener("loadSite", function (evt)
    {
    main();
    }, false);

  xhttpGet(charthouseURL, true, false, function(xhr)
    {
    if (xhr.readyState != 4 || xhr.status != 200)
      return;

    var evt;

    document.getElementById("charthouse").innerHTML = xhr.responseText;

    if (siteHUDtitle != undefined)
      document.getElementById("title").innerHTML = siteHUDtitle;

    if (aboutURL == undefined)
      aboutURL = baseURL + "/about.html";

    xhttpGet(aboutURL, false, false, function(xhr)
      {
      if (xhr.readyState != 4 || xhr.status != 200)
        return;

      document.getElementById("aboutContent").innerHTML = xhr.responseText;

      // set version
      var vers;

      vers = document.getElementById("CharthouseVersion");
      if (vers != undefined)
        vers.innerHTML = CHARTHOUSE_VERSION;
      });

    if (helpURL == undefined)
      helpURL = baseURL + "/help.html";

    xhttpGet(helpURL, false, false, function(xhr)
      {
      if (xhr.readyState != 4 || xhr.status != 200)
        return;

      document.getElementById("helpContent").innerHTML = xhr.responseText;
      });

    evt = new CustomEvent("loadSite");
    document.dispatchEvent(evt);
    });
  }

function main()
  {
  settingsInit();

  // reset background so if layers don't load, we don't see background image
  setTimeout(function(){
    document.getElementById("charthouse").style.background = "#000";}, 2000);

  chartsCreate();

  // compass rose
  compassRose = document.getElementById("compassRose");
  elemHide(compassRose);
  compassRoseAzimuth = compassRose.getElementById("azimuth");
  compassRoseCard = compassRose.getElementById("card");

  popupFeatureHeader = document.getElementById("popupFeatureHeader");
  popupFeatureContent = document.getElementById("popupFeatureContent");
  popupFeature = document.getElementById("popupFeature");
  overlayPopupFeature = new ol.Overlay(
    {
    element: popupFeature,
    multi: true
    });

  popupSmall = document.getElementById("popupSmall");
  popupSmallContent = document.getElementById("popupSmallContent");
  overlayPopupSmall = new ol.Overlay(
    {
    element: popupSmall,
    multi: true
    });

  hudPositionCenter = document.getElementById("positionCenter");
  hudPositionCursor = document.getElementById("positionCursor");
  hudPositionDistAz = document.getElementById("positionDistAz");
  hudDiagnostics = document.getElementById("diagnostics");

  formatGeoJSON = new ol.format.GeoJSON();
  formatKML = new ol.format.KML();

  crosshair = document.getElementById("crosshair");

  map = new ol.Map(
    {
    controls: ol.control.defaults({attribution: false}),
    overlays:
      [
      overlayPopupFeature,
      overlayPopupSmall
      ],
    target: "charthouse",
    view: new ol.View(
      {
      center: [0, 0],
      zoom: 2,
      minZoom: 2,
      maxZoom: 20
      })
    });

/*
  {
  var map3d;
  var scene;
  var terrainProvider;

  map3d = new olcs.OLCesium({map: map});
  map3d.setEnabled(true);
  scene = map3d.getCesiumScene();
  terrainProvider = new Cesium.CesiumTerrainProvider(
    {
    url: "//assets.agi.com/stk-terrain/world"
    });
  scene.terrainProvider = terrainProvider;
  };
*/

  // XXX this isn't working with UIkit modals!
  //map.addControl(new ol.control.FullScreen());

/*
  map.addControl(new ol.control.OverviewMap(
    {
    layers: [new ol.layer.Tile(
        {
        preload: Infinity,
        source: new ol.source.OSM(
          {
          wrapX: false,
          noWrap: true
          })
        })]
    }));
*/

  scaleLine = new ol.control.ScaleLine();
  map.addControl(scaleLine);

  buttonSearch = document.getElementById("buttonSearch");

  buttonFilter = document.getElementById("buttonFilter");

  buttonFollow = document.getElementById("buttonFollow");
  buttonFollow.onclick = followOff;

  badgeFollow = document.getElementById("badgeFollow");

  buttonMeasure = document.getElementById("buttonMeasure");
  //buttonMeasure.onclick = measureModeToggle;
  buttonMeasure.onclick = rangeRingToggle;

  buttonMarkerAdd = document.getElementById("buttonMarkerAdd");
  buttonMarkerAdd.onclick = markerAddToggle;

  map.on("moveend", onMoveEnd);
  map.on("pointermove", onMouseMove);
  map.getView().on("change:resolution", onZoom);
  document.getElementById("charthouse").addEventListener("mouseleave",
                                                         onPointerLeave);
  document.getElementById("charthouse").addEventListener("mousedown",
                                                         onMouseDown);
  document.getElementById("charthouse").addEventListener("mouseup",
                                                         onMouseUp);
  document.addEventListener("pointerstop", onMouseStop);

  interactionSelect = new ol.interaction.Select(
    {
    multi: true
    //filter: layersFilter
    });
  interactionSelect.setHitTolerance(5);
  map.addInteraction(interactionSelect);
  interactionSelect.on("select", function(evt)
    {
    if (evt.selected.length > 0)
      {
      popupFeatureShow(evt.selected, evt.mapBrowserEvent.coordinate, true);
      }
    else
      {
      popupFeatureClose();
      };
    });

  map.addInteraction(new ol.interaction.DragRotateAndZoom());

  interactionMeasure = new ol.interaction.Draw(
    {
    type: "LineString",
    maxPoints: 1,
    source: new ol.source.Vector(
      {
      wrapX: false,
      noWrap: true
      }),
    style: new ol.style.Style(
      {
      fill: new ol.style.Fill(
        {
        color: "rgba(255, 255, 255, 0.2)"
        }),
      stroke: new ol.style.Stroke(
        {
        color: "rgba(255, 80, 0, 0.7)", // #F50 Int'l Orange
        lineDash: [10, 10],
        width: 2
        }),
      image: new ol.style.Circle(
        {
        radius: 5,
        stroke: new ol.style.Stroke(
          {
          color: "rgba(0, 0, 0, 0.7)"
          }),
        fill: new ol.style.Fill(
          {
          //color: "rgba(255, 255, 255, 0.2)"
          color:"rgba(255, 80, 0, 0.7)" // #F50 Int'l Orange
          })
        })
      })
    });

  interactionMeasure.on("drawstart", function(evt)
    {
    var geom;

    geom = evt.feature.getGeometry();
    if (geom instanceof ol.geom.LineString)
      {
      var lonlat;

      lonlat = ol.proj.transform(geom.getFirstCoordinate(),
                                 "EPSG:3857", "EPSG:4326");
      posMeasureStart = new LatLon(lonlat[1], lonlat[0]);
      popupSmallContent.innerHTML = distanceString(0) + "&nbsp;" +
        azimuthString(0);
      overlayPopupSmall.setPosition(geom.getFirstCoordinate());
      elemShow(popupSmall, true);
      };
    });

  interactionMeasure.on("drawend", function(evt)
    {
    measureModeToggle();
    });

  interactionRangeRing = new ol.interaction.Draw(
    {
    type: "Circle",
    source: new ol.source.Vector(
      {
      wrapX: false,
      noWrap: true
      }),
    style: new ol.style.Style(
      {
      fill: new ol.style.Fill(
        {
        color: "rgba(255, 255, 255, 0.2)"
        }),
      stroke: new ol.style.Stroke(
        {
        color: "rgba(255, 80, 0, 0.7)", // #F50 Int'l Orange
        lineDash: [10, 10],
        width: 2
        }),
      image: new ol.style.Circle(
        {
        radius: 5,
        stroke: new ol.style.Stroke(
          {
          color: "rgba(0, 0, 0, 0.7)"
          }),
        fill: new ol.style.Fill(
          {
          color:"rgba(255, 80, 0, 0.7)" // #F50 Int'l Orange
          })
        })
      })
    });

  interactionRangeRing.on("drawstart", function(evt)
    {
    var geom;

    popupFeatureClose();
    geom = evt.feature.getGeometry();
    if (geom instanceof ol.geom.Circle)
      {
      var lonlat;

      lonlat = ol.proj.transform(geom.getFirstCoordinate(),
                                 "EPSG:3857", "EPSG:4326");
      posMeasureStart = new LatLon(lonlat[1], lonlat[0]);
      popupSmallContent.innerHTML = distanceString(0) + "&nbsp;" +
        azimuthString(0);
      overlayPopupSmall.setPosition(geom.getFirstCoordinate());
      elemShow(popupSmall, true);
      };
    });

  interactionRangeRing.on("drawend", function(evt)
    {
    rangeRingToggle();
    });

  interactionAim = new ol.interaction.Draw(
    {
    type: "Point",
    maxPoints: 1,
    source: new ol.source.Vector(
      {
      wrapX: false,
      noWrap: true
      }),
    style: new ol.style.Style(
      {
      text: new ol.style.Text(
        {
        text: "\uF276",  //fa-map-pin
        textAlign: "right",
        textBaseline: "bottom",
        font: "Normal 18pt FontAwesome",
        // #F50 Int'l Orange
        fill: new ol.style.Fill({color:"rgba(255, 80, 0, 0.7)"}),
        stroke: new ol.style.Stroke({color: "rgba(0, 0, 0, 0.7)"}),
        offsetX: 10,
        offsetY: 10
        })
      })
    });

  interactionAim.on("drawend", function(evt)
    {
    var coord;
    var lonlat;

    coord = evt.feature.getGeometry().getCoordinates();
    lonlat = ol.proj.transform(coord, "EPSG:3857", "EPSG:4326");
    lonlat[1] = roundf(lonlat[1], 5); // latitude
    lonlat[0] = roundf(lonlat[0], 5); // longitude

    aimSet(aimName, aimUUID, aimIP, lonlat[1], lonlat[0]);
    aimOff();
    });

  interactionMarkerAdd = new ol.interaction.Draw(
    {
    type: "Point",
    maxPoints: 1,
    source: new ol.source.Vector(
      {
      wrapX: false,
      noWrap: true
      }),
    style: new ol.style.Style(
      {
      text: new ol.style.Text(
        {
        text: "\uF041",  // map-marker
        textAlign: "right",
        textBaseline: "bottom",
        font: "Normal 18pt FontAwesome",
        // #F50 Int'l Orange
        fill: new ol.style.Fill({color:"rgba(255, 80, 0, 0.7)"}),
        stroke: new ol.style.Stroke({color: "rgba(0, 0, 0, 0.7)"}),
        offsetX: 5,
        offsetY: 0
        })
      })
    });

  interactionMarkerAdd.on("drawend", function(evt)
    {
    var coord;
    var lonlat;

    coord = evt.feature.getGeometry().getCoordinates();
    lonlat = ol.proj.transform(coord, "EPSG:3857", "EPSG:4326");
    lonlat[1] = roundf(lonlat[1], 5); // latitude
    lonlat[0] = roundf(lonlat[0], 5); // longitude

    markerAddSet(lonlat[1], lonlat[0]);
    markerAddOff();
    });

  settingsLoad();

  // order matters when adding layers!
  if (siteLayersBase != undefined)
    layersBase = siteLayersBase.concat(layersBase);
  if (siteLayersData != undefined)
    layersData = siteLayersData.concat(layersData);
  addLayers(layersBase, "formMapsLayersTable");
  addLayers(layersData, "formDataLayersTable");
  // so if sliders are > 0 and < 1, the opacity is set properly
  layersAllSetVisible();

  document.onkeydown = function(evt) { onKeyDown(evt);};
  document.onkeyup = function(evt) { onKeyUp(evt);};

  document.addEventListener("click", function(evt)
    {
/*
    if (settings["advanced"]["showDiagnostics"])
      {
      console.log("click:", evt.target.className,
                  evt.target.id, evt.target.class);
      };
*/

    // if click on map itself, then close stuff
    if (evt.target.className == "ol-unselectable")
      {
      dropdownsClose();
      };
    });

  // set up web worker pool
  {
  var url;
  var index;

  url = baseURL + "workerXHTTP.min.js";
  for (index=0; index < 5; ++index)
    {
    var worker;

    worker = new Worker(url);
    worker.addEventListener("message", workerCB);
    worker.running = false;
    workers.push(worker);
    };
  };

  updateSunMoon();

  setInterval(layersLoad, 2000);
  setInterval(settingsSave, 30000);
  setInterval(updateDatetime, 1000);
  setInterval(updateDiagnostics, 30000);
  setInterval(followUpdate, 5000);
  setInterval(updateSunMoon, 60000);

  /* example of how to listen for a layer load event
  document.addEventListener("layerLoaded", function (evt)
    {
    console.log("layerLoaded: " + evt.detail);
    }, false);
  */

  if (initFunction != undefined)
    {
    initFunction();
    };

/*
  {
  var layerSun;
  var layerMoon;

  layerSun = getLayerByName("Sun & Solar Illumination");
  layerMoon = getLayerByName("Moon");

  layerSun.layer.on("precompose", function(evt)
    {
    evt.context.globalCompositeOperation = "luminosity";
    });
  layerSun.layer.on("postcompose", function(evt)
    {
    evt.context.globalCompositeOperation = "source-over";
    });
  layerMoon.layer.on("precompose", function(evt)
    {
    evt.context.globalCompositeOperation = "luminosity";
    });
  layerMoon.layer.on("postcompose", function(evt)
    {
    evt.context.globalCompositeOperation = "source-over";
    });
  };
*/

  //setHeader("THIS IS A BLUE HEADER", "#FFF", "#00F");
  //setFooter("THIS IS A TRANSPARENT FOOTER", "#00F", "transparent");
  //setFooter("THIS IS A PURPLE FOOTER", "#00F", "#F0F");

  // MQTT
  if (mqttWS !== undefined && mqttTopics !== undefined &&
      mqttTopics.length > 0)
    {
    var cmd;
    var layer;

    workerMQTT = new Worker(baseURL + "workerMQTT.min.js");
    workerMQTT.addEventListener("message", workerCB);

    layer = getLayerByName("MQTT");

    cmd =
      {
      cmd: "start",
      debug: settings["advanced"]["showDebug"],
      baseURL: baseURL,
      mqttWS: mqttWS,
      mqttTopics: mqttTopics,
      layerUUID: layer.uuid,
      layerName: layer.name,
      layerGroup: layer.group,
      clientId: "charthouse_" + Math.random().toString(16).substr(2, 8)
      };

    workerMQTT.postMessage(cmd);
    };

  // restore center, zoom-level, and rotation based on URL
  {
  if (window.location.hash !== "")
    {
    var hash;
    var parts;

    hash = window.location.hash.replace(/#map=*/i, "");
    parts = hash.split("/");
    if (parts.length === 4)
      {
      var zoom;
      var center;
      var rotation;
      var view;

      zoom = parseInt(parts[0], 10);
      center =
        [
        parseFloat(parts[1]),
        parseFloat(parts[2])
        ];
      rotation = parseFloat(parts[3]);
      view = map.getView();
      view.setCenter(center);
      view.setZoom(zoom);
      view.setRotation(rotation);

      if (window.history.replaceState !== undefined)
        {
        var pathname

        pathname = window.location.pathname.replace(/#map=*/i, "");
        window.history.replaceState({}, window.location.host, pathname);
        };
      };
    };
  };

  // framebuster
  if (self == top)
    document.documentElement.style.display = "block";
  else
    top.location = self.location;
  }

