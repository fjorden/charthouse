
# Charthouse
# https://gitlab.com/fjorden/charthouse
# Copyright 2017-2019 Fjorden

#VERSION = "$(git describe --tags --always --long --dirty)"
VERSION = "v1.2.19"

#UGLIFY_OPTS = "--verbose"

CHARTJS = support/Chart.bundle-2.7.2.min.js
GEODESY = support/geodesy-1.1.2
MQTTJS = support/mqtt-2.15.1.min.js
OL = support/OpenLayers/ol.js
#OL =  support/ol-cesium-v1.28/Cesium/Cesium.js \
#      support/ol-cesium-v1.28/olcesium.js
SUNCALC = support/suncalc-1.8.0
TURFJS = support/turf-5.0.4.min.js
UIKIT = support/uikit

INSTALL_DIR = "/home/www/html"

FILES = \
  polyfills.js \
  ${CHARTJS} \
  ${GEODESY}/vector3d.js \
  ${GEODESY}/latlon-ellipsoidal.js \
  ${GEODESY}/latlon-vincenty.js \
  ${GEODESY}/dms.js \
  ${GEODESY}/utm.js \
  ${GEODESY}/mgrs.js \
  ${OL} \
  ${SUNCALC}/suncalc.js \
  ${TURFJS} \
  ${UIKIT}/js/uikit.min.js \
  ${UIKIT}/js/uikit-icons.min.js \
  astronomy.js \
  dropdown.js \
  geolocation.js \
  search.js \
  icons.js \
  feature.js \
  featureDefaults.js \
  follow.js \
  marker.js \
  settings.js \
  stats.js \
  util.js \
  version.js \
  xhttpGet.js \
  main.js

FILES_WORKERS = \
  polyfillsTurf.js \
  ${TURFJS} \
  sortGeo.js \
  icons.js \
  featureDefaults.js

CSS = \
  charthouse.css

INSTALL_FILES = \
  charthouse.min.css \
  charthouse.min.js \
  workerMQTT.min.js \
  workerXHTTP.min.js \
  images/maps.jpg

SASSC := `which sassc 2> /dev/null`
PSCSS := `which pscss 2> /dev/null`

all:	uglify charthouse.min.css images
	cd python && cmake . && $(MAKE)
	chmod -R a+rX .

icons.js:	python/charthouse/feature.py
	./icons > icons.js

uglify:	${FILES} .FORCE
	uglifyjs -c ${UGLIFY_OPTS} -o charthouse.min.js -- ${FILES}
	# using awk ensures newline between each file
	awk '{print $0}' ${FILES} > charthouse-debug.js
	uglifyjs -b ${UGLIFY_OPTS} -o workerMQTT.min.js -- workerMQTT.js \
	  ${MQTTJS} ${FILES_WORKERS}
	uglifyjs -b ${UGLIFY_OPTS} -o workerXHTTP.min.js -- workerXHTTP.js \
      xhttpGet.js ${FILES_WORKERS}
	chmod a+r charthouse.min.js charthouse-debug.js workerMQTT.min.js \
	  workerXHTTP.min.js

charthouse.min.css:	${CSS} .FORCE
	@if `which sassc > /dev/null 2>&1`; then \
		echo "sassc ${@}"; \
		cat ${CSS} | sassc -s | cleancss -o charthouse.min.css; \
	elif `which pscss > /dev/null 2>&1`; then \
		echo "pscss ${@}"; \
		cat ${CSS} | pscss | cleancss -o charthouse.min.css; \
	else \
		echo "No sassc or pscss in path"; exit 1; \
	fi

version.js:	.FORCE
	@echo ${VERSION}
	@echo "var CHARTHOUSE_VERSION=\"${VERSION}\"" > version.js

images:	.FORCE
	convert images/unsplash.com/andrew-neel-133200-unsplash.jpg \
	  -scale 14% images/maps.jpg

clean:
	$(RM) -rf *.min.css *.min.js charthouse-debug.js icons.js version.js \
	  python/charthouse/__pycache__ python/CMakeCache.txt \
	  python/CMakeFiles python/Makefile python/build \
	  python/cmake_install.cmake python/setup.py \
	  images/*.jpg
	$(MAKE) -C utils clean

install:  all
	rsync -avp --exclude=.git --delete --chmod=D755,F644 \
	  . ${INSTALL_DIR}/charthouse

.FORCE:

